using System;
using System.IO;
using MathNet.Numerics.LinearAlgebra;
using SupportMathFunctions;
public class kMeansClusterClass{

static Matrix y;
static Matrix tempSize;
static Matrix p;
static Matrix temp;
static Matrix c;
static Matrix d;
static Matrix g;
static Matrix f;
static Matrix tempf;

public static Matrix kMeansCluster(Matrix m, Matrix k, Matrix isRand)
{
    if ((k<3))  {
isRand= (Matrix)(false);  }    int maxRow = m.RowCount;
int maxCol = m.ColumnCount;    if ((maxRow<=k))       {
y= (Matrix)(MatrixSupport.AddColumn((Matrix)m, MatrixSupport.CreateInterval((int)1, (int)maxRow)));  }  else      {
if ((isRand==1))  		{
tempSize = (Matrix)( MatrixSupport.Size(m, 1));          p= (Matrix)(RandomSupport.RandomPermutation((int)tempSize));               for (int i = 1- 1; i < k; i++)  			{
temp = (Matrix)( p[i, 0]);		              Matrix tempVectorc=MatrixSupport.GetMatrixByRow((Vector)temp, m);
MatrixSupport.CreateAndSetRow(ref c, (Vector)tempVectorc, i);        	}      }      else          {
for (int i = 1- 1; i < k; i++)             {
Matrix tempVectorc=MatrixSupport.GetMatrixByRow((Vector)i, m);
MatrixSupport.CreateAndSetRow(ref c, (Vector)tempVectorc, i);              	}      }        	temp= (Matrix)(new Matrix((int)maxRow, (int)1 ));   	while ((1==1))          {
d= (Matrix)(MatrixSupport.GetDistanceMatrix(m,c));            g=MatrixSupport.RetriveMinimumIndexesVector(d,2);            if ((g==temp))              {
break;          }          else              {
temp= (Matrix)(g);                   }          for (int i = 1- 1; i < k; i++)              {
f= (Matrix)(MatrixSupport.Find(g, i));              tempf = (Matrix)( MatrixSupport.GetMatrixByRow((Vector)f, m));              Matrix tempVectorc=MatrixSupport.MeanMatrix(tempf,1);
MatrixSupport.CreateAndSetRow(ref c, (Vector)tempVectorc, i);          }          }  	}  	y= (Matrix)(MatrixSupport.AddColumn((Matrix)m,(Vector)g));  return y;
}
}
  