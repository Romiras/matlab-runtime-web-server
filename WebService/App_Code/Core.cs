using System;
using System.Collections.Generic;
using System.Text;
using Path = System.IO.Path;
using Antlr.Runtime;
using Microsoft.CSharp;
using System.IO;
using System.CodeDom.Compiler;
using System.Globalization;
using System.Collections;
using System.Reflection;
using MathNet.Numerics.LinearAlgebra;


public class Core
{
    public static StringBuilder InsertIdentifiers(ITokenStream tokens, MatlabParser parser, string methodName)
    {
        string storedTypes = String.Empty;
        foreach (String storedType in parser.identifiers)
        {
            if (!parser.parameters.Contains(storedType))
            {
                storedTypes += "static Matrix " + storedType + ";\n";
            }
        }
        foreach (String storedType in parser.identifiers3d)
        {
            if (!parser.parameters.Contains(storedType))
            {
                storedTypes += "Matrix3d " + storedType + " = null;\n";
            }
        }

        StringBuilder finalOutput = new StringBuilder(tokens.ToString());
        finalOutput.Insert(119, "\n" + storedTypes + "\n");
        finalOutput.Replace("Wrapper", methodName + "Class");
        return finalOutput;
    }

    public static void Init(string inputFileName, out ITokenStream tokens, out MatlabParser parser)
    {
        ICharStream input = new ANTLRFileStream(inputFileName);
        MatlabLexer lex = new MatlabLexer(input);
        tokens = new TokenRewriteStream(lex);
        parser = new MatlabParser(tokens);
        parser.program();
    }

    public static void WriteToFile(string fileName, string output)
    {
        FindAndDelete(fileName);
        //string s = "function y=kMeansCluster(m, k, isRand)\r\n\r\nif (k<3),\r\nisRand=false;\r\nend\r\n\r\n[maxRow, maxCol]=size(m);\r\n\r\nif (maxRow<=k), \r\n    y=[m, 1:maxRow];\r\nend\r\nelse\r\n    if (isRand==1),\r\n\t\ttempSize = size(m,1);\r\n        p=randperm(tempSize);     \r\n        for i=1:k\r\n\t\t\ttemp = p(i);\t\t\r\n            c(i,:)=m(temp,:);  \r\n    \tend\r\n    end\r\n    else\r\n        for i=1:k\r\n           c(i,:)=m(i,:);        \r\n    \tend\r\n    end\r\n    \r\n\ttemp=zeros(maxRow,1); \r\n\twhile (1==1),\r\n        d=DistMatrix(m,c);  \r\n        [z,g]=min(d,[],2);  \r\n        if (g==temp),\r\n            break;\r\n        end\r\n        else\r\n            temp=g;         \r\n        end\r\n        for i=1:k\r\n            f=find(g==i);\r\n            tempf = m(f,:);\r\n            c(i,:)=mean(tempf,1);\r\n        end\r\n        end\r\n\tend\r\n\ty=[m,g];\r\nend\r\n";
        //output.
        //string newstring = output.Replace("\\r\\n", "//n");
        using (StreamWriter sw = File.CreateText(fileName))
        {
            //sw.NewLine = "\n";
            sw.Write(output);
            sw.Close();
        }
    }

    public static void FindAndDelete(string fileName)
    {
        if (File.Exists(fileName))
            File.Delete(fileName);
    }

    public static string CompileLibrary(String sourceName)
    {
        StringBuilder result = new StringBuilder();
        FileInfo sourceFile = new FileInfo(sourceName);
        CodeDomProvider provider = null;
        bool compileOk = false;

        // Select the code provider based on the input file extension.
        if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
            provider = new Microsoft.CSharp.CSharpCodeProvider();
        else
            result.Append("Source file must have a .cs extension");

        if (provider != null)
        {
            // Format the executable file name.
            // Build the output assembly path using the current directory
            // and <source>_cs.exe or <source>_vb.exe.

            String dllName = String.Format(@"{0}\{1}" + ".dll",
                @"Libraries\",
                sourceFile.Name.Replace(".cs", ""));

            CompilerParameters cp = new CompilerParameters();

            // Generate an executable instead of 
            // a class library.
            cp.GenerateExecutable = false;

            // Specify the assembly file name to generate.
            cp.OutputAssembly = dllName;

            // Save the assembly as a physical file.
            cp.GenerateInMemory = false;

            // Set whether to treat all warnings as errors.
            cp.TreatWarningsAsErrors = false;

            cp.ReferencedAssemblies.Add(@"Bin\MathNet.Iridium.dll");
            cp.ReferencedAssemblies.Add(@"Bin\SupportMathFunctions.dll");

            // Invoke compilation of the source file.
            CompilerResults cr = provider.CompileAssemblyFromFile(cp, sourceName);

            if (cr.Errors.Count > 0)
            {
                // Display compilation errors.
                result.Append("Errors building " + sourceName + " into " + cr.PathToAssembly);
                foreach (CompilerError ce in cr.Errors)
                {
                    result.Append(ce.ToString() + "\n");
                }
            }
            else
            {
                // Display a successful compilation message.
                result.Append("Source " + sourceName + " built into " + cr.PathToAssembly + " successfully.");
            }

            // Return the results of the compilation.
            if (cr.Errors.Count > 0)
            {
                compileOk = false;
            }
            else
            {
                compileOk = true;
            }
        }
        return result.ToString();
    }

    public static void Load(string libraryName)
    {
        if (Service.hardcoded.Contains(libraryName))
        {
            if (File.Exists(@"Libraries\Algorythms.dll"))
            {
                Service.assembly = Assembly.LoadFrom(@"Libraries\Algorythms.dll");
            }
        }
        else if (File.Exists(@"Libraries\" + libraryName + ".dll"))
        {
            Service.assembly = Assembly.LoadFrom(@"Libraries\" + libraryName + ".dll");
        }
    }

    //list[0] contains number of rows
    public static Matrix ToMatrix(ArrayList list)
    {
        double rowCount = (double)list[0];
        ArrayList listToConvert = list.GetRange(1, list.Count - 1);
        double[] array = (double[])listToConvert.ToArray(typeof(double));
        return new Matrix(array, (int)rowCount);
    }

    public static ArrayList ToList(Matrix m)
    {
        ArrayList list = new ArrayList();
        list.Add(m.RowCount);
        for (int i = 0; i < m.ColumnCount; i++)
        {
            for (int j = 0; j < m.RowCount; j++)
            {
                list.Add(m[j, i]);
            }
        }
        return list;
    }

}
