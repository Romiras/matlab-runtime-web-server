﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Linq;
using MathNet.Numerics.LinearAlgebra;
using System.Collections;
using System.Reflection;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;
using Antlr.Runtime;

[WebService(Namespace = "http://www.cama-project.ru/implementer")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

// [System.Web.Script.Services.ScriptService]
public class Service : System.Web.Services.WebService
{
    public Service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    const string ROOT = @"C:/Programming/SVN/trunk/KERNEL/WebSerivce/";

    [WebMethod]
    public ArrayList Implement(string methodName, ArrayList data, ArrayList paramsAdd)
    {
        Directory.SetCurrentDirectory(ROOT);
        parameters = new Object[paramsAdd.Count + 1];

        Core.Load(methodName);
        MethodInfo method = assembly.GetType(methodName + "Class").GetMethod(methodName);

        parameters[0] = Core.ToMatrix(data);
        for (int i = 0; i < paramsAdd.Count; i++)
        {
            parameters[i + 1] = (Matrix)((double)paramsAdd[i]);
        }

        Matrix result;
        Stopwatch sw = new Stopwatch();
        sw.Start();
        try
        {
            result = (Matrix)method.Invoke(null, parameters);
        }
        catch (Exception e)
        {
            throw new Exception("Some error occurs during method invocation.\n Exception:" + e.ToString());
        }
        sw.Stop();

        ArrayList arrayResult = Core.ToList(result);
        arrayResult.Add(sw.Elapsed.TotalSeconds);
        return arrayResult;
    }
    
    [WebMethod]
    public string Compile(string inputFileName, string inputFile)
    {
        Directory.SetCurrentDirectory(ROOT);
        matlabName = @"AlgorythmsM\" + inputFileName + ".txt";
        csName = @"AlgorythmsCS\" + inputFileName + ".cs";

        Core.FindAndDelete(matlabName);
        Core.FindAndDelete(csName);

        Core.WriteToFile(matlabName, inputFile);

        ITokenStream tokens;
        MatlabParser parser;
        //initialize parser and lexer
        Core.Init(matlabName, out tokens, out parser);
        //insert all necessary data inside the output stream
        StringBuilder finalOutput = Core.InsertIdentifiers(tokens, parser, inputFileName);

        //write to cs file
        Core.WriteToFile(csName, finalOutput.ToString());
        //compile to dll file
        return (finalOutput.ToString() + "\n" + Core.CompileLibrary(csName));
    }

    [WebMethod]
    public ArrayList FindAllMethods()
    {
        ArrayList results = new ArrayList();
        Directory.SetCurrentDirectory(ROOT);
        StringBuilder sb = new StringBuilder(hardcoded.ToString());
        string[] libs = Directory.GetFiles(@"Libraries\");
        foreach (string lib in libs)
        {
            StringBuilder methodName = new StringBuilder(lib.Remove(0, 10));
            methodName.Replace(".dll", "");
            Assembly assembly = Assembly.LoadFrom(lib);
            MethodInfo mi;
            Type[] types = null;
            if (methodName.ToString() != "Algorythms")
            {
                mi = assembly.GetType(methodName.ToString() + "Class").GetMethod(methodName.ToString());
                results.Add(mi.ToString());
            }
            else
                types = assembly.GetTypes();

            MethodInfo info;
            if (types != null)
            {
                foreach (Type type in types)
                {
                    info = type.GetMethod(type.Name.Replace("Class", ""));
                    results.Add(info.ToString());
                }
            }
        }
        return results;
    }

    static string matlabName;
    static string csName;
    static Object[] parameters;

    public static Assembly assembly;
    public static List<string> hardcoded = new List<string>() { "kMeansCluster", "FCM", "EMGM", "Dunn", "Hartigan" };

}
