﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Nwc.XmlRpc;
using System.Collections;
using InputConversionUtility;
using MathNet.Numerics.LinearAlgebra;

namespace Service
{
    class Server
    {
        const int PORT = 8080;

        public static void Main()
        {
            //ArrayList list1 = new ArrayList() { "4,0", "1,0", "2,0", "3,0", "4,0", "5,0", "6,0", "7,0", "8,0", "9,0", "10,0", "11,0", "12,0" }; ;
            ArrayList list2 = new ArrayList() { "3,0", "100,0", "0,0001" };
            Matrix X = Utility.FromFileToMatrix(@"c:\Programming\Iris_main_test.txt");


            //string s = Core.Compile("kMeans", "function c=kMeans(m, k, isRand)    if (k<3),  isRand=false;  end    [maxRow, maxCol]=size(m);    if (maxRow<=k),       y=[m, 1:maxRow];  end  else      if (isRand==1),  		tempSize = size(m,1);          p=randperm(tempSize);               for i=1:k  			temp = p(i);		              c(i,:)=m(temp,:);        	end      end      else          for i=1:k             c(i,:)=m(i,:);              	end      end        	temp=zeros(maxRow,1);   	while (1==1),          d=DistMatrix(m,c);            [z,g]=min(d,[],2);            if (g==temp),              break;          end          else              temp=g;                   end          for i=1:k              f=find(g==i);              tempf = m(f,:);              c(i,:)=mean(tempf,1);          end          end  	end  	y=[m,g];  end  ");// Implement("kMeansCluster", list1, list2);
            string res = Service.Implement("FCM", Service.ToList(X), list2);
            //ArrayList l = Service.FindAllMethods();
            //string inputFile = "function x=calc(a, b)\n    x = a + b;\nend";
            //Core.Compile("calc", inputFile);

            //XmlRpcServer server = new XmlRpcServer(PORT);
            //server.Add("Service", new Service());
            //server.Add("Core", new Core());
            //Console.WriteLine("Web Server Running on port {0} ... Press ^C to Stop...", PORT);
            //server.Start();
        }
    }
}
