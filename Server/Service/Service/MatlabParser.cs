/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// $ANTLR 3.1.2 C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g 2009-05-14 11:14:12

// The variable 'variable' is assigned but its value is never used.
#pragma warning disable 168, 219
// Unreachable code detected.
#pragma warning disable 162




using System;
using Antlr.Runtime;
using IList 		= System.Collections.IList;
using ArrayList 	= System.Collections.ArrayList;
using Stack 		= Antlr.Runtime.Collections.StackList;



using Antlr.Runtime.Tree;

/** Convert the simple input to be java code; wrap in a class,
 *  convert method with "public void", add decls.
 */
public partial class MatlabParser : Parser
{
    public static readonly string[] tokenNames = new string[] 
	{
        "<invalid>", 
		"<EOR>", 
		"<DOWN>", 
		"<UP>", 
		"ID", 
		"END", 
		"IF", 
		"CONDITION", 
		"ANDOR", 
		"INT", 
		"DOUBLE", 
		"NAME", 
		"ID3D", 
		"WHILE", 
		"FOR", 
		"TYPE", 
		"BEGIN", 
		"ELSE", 
		"SEMI", 
		"WS", 
		"'function'", 
		"'='", 
		"'('", 
		"')'", 
		"','", 
		"'+'", 
		"'-'", 
		"'^'", 
		"'*'", 
		"'/'", 
		"'['", 
		"']'", 
		"'validsumsqures'", 
		"'stepfcm'", 
		"':'", 
		"'min('", 
		"'[]'", 
		"'error'", 
		"'return'", 
		"'trace('", 
		"'max('", 
		"'kmeans('", 
		"'sum('", 
		"'exp('", 
		"'\\''", 
		"'ones('", 
		"'inv('", 
		"'sqrt('", 
		"'det('", 
		"'abs'", 
		"'initfcm'", 
		"'mean'", 
		"'find'", 
		"'DistMatrix'", 
		"'zeros('", 
		"'size'", 
		"'randperm('", 
		"'break;'", 
		"'pi'", 
		"'eps'"
    };

    public const int T__29 = 29;
    public const int T__28 = 28;
    public const int T__27 = 27;
    public const int WHILE = 13;
    public const int T__26 = 26;
    public const int T__25 = 25;
    public const int T__24 = 24;
    public const int T__23 = 23;
    public const int T__22 = 22;
    public const int T__21 = 21;
    public const int T__20 = 20;
    public const int FOR = 14;
    public const int CONDITION = 7;
    public const int ID = 4;
    public const int EOF = -1;
    public const int ANDOR = 8;
    public const int TYPE = 15;
    public const int IF = 6;
    public const int T__55 = 55;
    public const int T__56 = 56;
    public const int T__57 = 57;
    public const int NAME = 11;
    public const int T__58 = 58;
    public const int T__51 = 51;
    public const int T__52 = 52;
    public const int T__53 = 53;
    public const int T__54 = 54;
    public const int T__59 = 59;
    public const int BEGIN = 16;
    public const int DOUBLE = 10;
    public const int T__50 = 50;
    public const int T__42 = 42;
    public const int T__43 = 43;
    public const int T__40 = 40;
    public const int T__41 = 41;
    public const int T__46 = 46;
    public const int T__47 = 47;
    public const int T__44 = 44;
    public const int T__45 = 45;
    public const int T__48 = 48;
    public const int T__49 = 49;
    public const int ELSE = 17;
    public const int INT = 9;
    public const int SEMI = 18;
    public const int T__30 = 30;
    public const int T__31 = 31;
    public const int T__32 = 32;
    public const int WS = 19;
    public const int T__33 = 33;
    public const int T__34 = 34;
    public const int T__35 = 35;
    public const int T__36 = 36;
    public const int T__37 = 37;
    public const int T__38 = 38;
    public const int T__39 = 39;
    public const int ID3D = 12;
    public const int END = 5;

    // delegates
    // delegators



        public MatlabParser(ITokenStream input)
    		: this(input, new RecognizerSharedState()) {
        }

        public MatlabParser(ITokenStream input, RecognizerSharedState state)
    		: base(input, state) {
            InitializeCyclicDFAs();

             
        }
        
    protected ITreeAdaptor adaptor = new CommonTreeAdaptor();

    public ITreeAdaptor TreeAdaptor
    {
        get { return this.adaptor; }
        set {
    	this.adaptor = value;
    	}
    }

    override public string[] TokenNames {
		get { return MatlabParser.tokenNames; }
    }

    override public string GrammarFileName {
		get { return "C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g"; }
    }


    public TokenRewriteStream tokens;
    public System.Collections.Generic.List<String> identifiers = new System.Collections.Generic.List<string>();
    public System.Collections.Generic.List<String> identifiers3d = new System.Collections.Generic.List<string>();
    public System.Collections.Generic.List<String> parameters = new System.Collections.Generic.List<string>();
    public System.Collections.Generic.List<String> refParameters = new System.Collections.Generic.List<string>();
    public int haven;
    public string returnValue;


    public class program_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "program"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:26:1: program : ( method )+ ;
    public MatlabParser.program_return program() // throws RecognitionException [1]
    {   
        MatlabParser.program_return retval = new MatlabParser.program_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.method_return method1 = default(MatlabParser.method_return);




            tokens = (TokenRewriteStream)input; 
            IToken start = input.LT(1);

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:31:5: ( ( method )+ )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:31:9: ( method )+
            {
            	root_0 = (object)adaptor.GetNilNode();

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:31:9: ( method )+
            	int cnt1 = 0;
            	do 
            	{
            	    int alt1 = 2;
            	    int LA1_0 = input.LA(1);

            	    if ( (LA1_0 == 20) )
            	    {
            	        alt1 = 1;
            	    }


            	    switch (alt1) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:31:9: method
            			    {
            			    	PushFollow(FOLLOW_method_in_program50);
            			    	method1 = method();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, method1.Tree);

            			    }
            			    break;

            			default:
            			    if ( cnt1 >= 1 ) goto loop1;
            		            EarlyExitException eee1 =
            		                new EarlyExitException(1, input);
            		            throw eee1;
            	    }
            	    cnt1++;
            	} while (true);

            	loop1:
            		;	// Stops C# compiler whinging that label 'loop1' has no statements


            	        tokens.InsertBefore(start,"using System;\nusing System.IO;\nusing MathNet.Numerics.LinearAlgebra;\nusing SupportMathFunctions;\npublic class Wrapper{\n");      
            	        // note the reference to the last token matched for method:
            	        tokens.InsertAfter(((method1 != null) ? ((IToken)method1.Stop) : null), "\n}\n");
            	//        haven = (IToken)(method.stop).TokenIndex;
            	        

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "program"

    public class method_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "method"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:40:1: method : m= 'function' (rv= ID | refValues ) s= '=' ID br1= '(' vars lbl= ')' st= statements fin= END ;
    public MatlabParser.method_return method() // throws RecognitionException [1]
    {   
        MatlabParser.method_return retval = new MatlabParser.method_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken m = null;
        IToken rv = null;
        IToken s = null;
        IToken br1 = null;
        IToken lbl = null;
        IToken fin = null;
        IToken ID3 = null;
        MatlabParser.statements_return st = default(MatlabParser.statements_return);

        MatlabParser.refValues_return refValues2 = default(MatlabParser.refValues_return);

        MatlabParser.vars_return vars4 = default(MatlabParser.vars_return);


        object m_tree=null;
        object rv_tree=null;
        object s_tree=null;
        object br1_tree=null;
        object lbl_tree=null;
        object fin_tree=null;
        object ID3_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:41:5: (m= 'function' (rv= ID | refValues ) s= '=' ID br1= '(' vars lbl= ')' st= statements fin= END )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:41:9: m= 'function' (rv= ID | refValues ) s= '=' ID br1= '(' vars lbl= ')' st= statements fin= END
            {
            	root_0 = (object)adaptor.GetNilNode();

            	m=(IToken)Match(input,20,FOLLOW_20_in_method82); 
            		m_tree = (object)adaptor.Create(m);
            		adaptor.AddChild(root_0, m_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:41:22: (rv= ID | refValues )
            	int alt2 = 2;
            	int LA2_0 = input.LA(1);

            	if ( (LA2_0 == ID) )
            	{
            	    alt2 = 1;
            	}
            	else if ( (LA2_0 == 30) )
            	{
            	    alt2 = 2;
            	}
            	else 
            	{
            	    NoViableAltException nvae_d2s0 =
            	        new NoViableAltException("", 2, 0, input);

            	    throw nvae_d2s0;
            	}
            	switch (alt2) 
            	{
            	    case 1 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:41:23: rv= ID
            	        {
            	        	rv=(IToken)Match(input,ID,FOLLOW_ID_in_method87); 
            	        		rv_tree = (object)adaptor.Create(rv);
            	        		adaptor.AddChild(root_0, rv_tree);


            	        }
            	        break;
            	    case 2 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:41:31: refValues
            	        {
            	        	PushFollow(FOLLOW_refValues_in_method91);
            	        	refValues2 = refValues();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, refValues2.Tree);

            	        }
            	        break;

            	}

            	s=(IToken)Match(input,21,FOLLOW_21_in_method96); 
            		s_tree = (object)adaptor.Create(s);
            		adaptor.AddChild(root_0, s_tree);

            	ID3=(IToken)Match(input,ID,FOLLOW_ID_in_method98); 
            		ID3_tree = (object)adaptor.Create(ID3);
            		adaptor.AddChild(root_0, ID3_tree);

            	br1=(IToken)Match(input,22,FOLLOW_22_in_method102); 
            		br1_tree = (object)adaptor.Create(br1);
            		adaptor.AddChild(root_0, br1_tree);

            	PushFollow(FOLLOW_vars_in_method104);
            	vars4 = vars();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, vars4.Tree);
            	lbl=(IToken)Match(input,23,FOLLOW_23_in_method108); 
            		lbl_tree = (object)adaptor.Create(lbl);
            		adaptor.AddChild(root_0, lbl_tree);

            	PushFollow(FOLLOW_statements_in_method112);
            	st = statements();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, st.Tree);
            	fin=(IToken)Match(input,END,FOLLOW_END_in_method116); 
            		fin_tree = (object)adaptor.Create(fin);
            		adaptor.AddChild(root_0, fin_tree);


            	        	
            	        	tokens.Replace(m, s, "public static Matrix ");
            	        	tokens.Replace(lbl, ")\n{\n");
            	        	tokens.Replace(fin, "}");
            	        	if(rv != null)
            	        	{
            	        		returnValue=rv.Text;
            		        	tokens.InsertBefore(fin, "return " + returnValue + ";\n");
            	        	}
            	        	System.Text.StringBuilder sb = new System.Text.StringBuilder();
            	        	for(int j = 0; j < refParameters.Count; j++)
            	        	{
            				sb.Append("ref Matrix " + refParameters[j] + ", ");
            	        	}
            	        	tokens.Replace(br1, "(" + sb.ToString());
            	        

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "method"

    public class ifStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "ifStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:61:1: ifStatement : IF condExpression com= ',' ;
    public MatlabParser.ifStatement_return ifStatement() // throws RecognitionException [1]
    {   
        MatlabParser.ifStatement_return retval = new MatlabParser.ifStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken com = null;
        IToken IF5 = null;
        MatlabParser.condExpression_return condExpression6 = default(MatlabParser.condExpression_return);


        object com_tree=null;
        object IF5_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:62:5: ( IF condExpression com= ',' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:62:7: IF condExpression com= ','
            {
            	root_0 = (object)adaptor.GetNilNode();

            	IF5=(IToken)Match(input,IF,FOLLOW_IF_in_ifStatement148); 
            		IF5_tree = (object)adaptor.Create(IF5);
            		root_0 = (object)adaptor.BecomeRoot(IF5_tree, root_0);

            	PushFollow(FOLLOW_condExpression_in_ifStatement151);
            	condExpression6 = condExpression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, condExpression6.Tree);
            	com=(IToken)Match(input,24,FOLLOW_24_in_ifStatement155); 
            		com_tree = (object)adaptor.Create(com);
            		adaptor.AddChild(root_0, com_tree);


            	    	tokens.Replace(com, "");
            	    	tokens.InsertBefore(((condExpression6 != null) ? ((IToken)condExpression6.Start) : null), "(");
            	    	tokens.InsertAfter(((condExpression6 != null) ? ((IToken)condExpression6.Stop) : null), ")");
            	    

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "ifStatement"

    public class expression_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "expression"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:71:1: expression : mul ( ( '+' | '-' ) mul )* ;
    public MatlabParser.expression_return expression() // throws RecognitionException [1]
    {   
        MatlabParser.expression_return retval = new MatlabParser.expression_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken set8 = null;
        MatlabParser.mul_return mul7 = default(MatlabParser.mul_return);

        MatlabParser.mul_return mul9 = default(MatlabParser.mul_return);


        object set8_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:71:11: ( mul ( ( '+' | '-' ) mul )* )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:71:15: mul ( ( '+' | '-' ) mul )*
            {
            	root_0 = (object)adaptor.GetNilNode();

            	PushFollow(FOLLOW_mul_in_expression176);
            	mul7 = mul();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, mul7.Tree);
            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:71:19: ( ( '+' | '-' ) mul )*
            	do 
            	{
            	    int alt3 = 2;
            	    int LA3_0 = input.LA(1);

            	    if ( ((LA3_0 >= 25 && LA3_0 <= 26)) )
            	    {
            	        alt3 = 1;
            	    }


            	    switch (alt3) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:71:20: ( '+' | '-' ) mul
            			    {
            			    	set8 = (IToken)input.LT(1);
            			    	if ( (input.LA(1) >= 25 && input.LA(1) <= 26) ) 
            			    	{
            			    	    input.Consume();
            			    	    adaptor.AddChild(root_0, (object)adaptor.Create(set8));
            			    	    state.errorRecovery = false;
            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    throw mse;
            			    	}

            			    	PushFollow(FOLLOW_mul_in_expression185);
            			    	mul9 = mul();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, mul9.Tree);

            			    }
            			    break;

            			default:
            			    goto loop3;
            	    }
            	} while (true);

            	loop3:
            		;	// Stops C# compiler whining that label 'loop3' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "expression"

    public class power_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "power"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:73:1: power : id1= ID pow= '^' id2= ID ;
    public MatlabParser.power_return power() // throws RecognitionException [1]
    {   
        MatlabParser.power_return retval = new MatlabParser.power_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken id1 = null;
        IToken pow = null;
        IToken id2 = null;

        object id1_tree=null;
        object pow_tree=null;
        object id2_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:73:7: (id1= ID pow= '^' id2= ID )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:73:9: id1= ID pow= '^' id2= ID
            {
            	root_0 = (object)adaptor.GetNilNode();

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_power198); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	pow=(IToken)Match(input,27,FOLLOW_27_in_power202); 
            		pow_tree = (object)adaptor.Create(pow);
            		adaptor.AddChild(root_0, pow_tree);

            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_power206); 
            		id2_tree = (object)adaptor.Create(id2);
            		adaptor.AddChild(root_0, id2_tree);


            			tokens.Replace(id1, "Math.Pow(" + id1.Text + ", " + id2.Text);
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "power"

    public class condExpression_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "condExpression"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:79:1: condExpression : '(' value CONDITION value ')' ( ANDOR '(' value CONDITION value ')' )* ;
    public MatlabParser.condExpression_return condExpression() // throws RecognitionException [1]
    {   
        MatlabParser.condExpression_return retval = new MatlabParser.condExpression_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken char_literal10 = null;
        IToken CONDITION12 = null;
        IToken char_literal14 = null;
        IToken ANDOR15 = null;
        IToken char_literal16 = null;
        IToken CONDITION18 = null;
        IToken char_literal20 = null;
        MatlabParser.value_return value11 = default(MatlabParser.value_return);

        MatlabParser.value_return value13 = default(MatlabParser.value_return);

        MatlabParser.value_return value17 = default(MatlabParser.value_return);

        MatlabParser.value_return value19 = default(MatlabParser.value_return);


        object char_literal10_tree=null;
        object CONDITION12_tree=null;
        object char_literal14_tree=null;
        object ANDOR15_tree=null;
        object char_literal16_tree=null;
        object CONDITION18_tree=null;
        object char_literal20_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:79:16: ( '(' value CONDITION value ')' ( ANDOR '(' value CONDITION value ')' )* )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:79:19: '(' value CONDITION value ')' ( ANDOR '(' value CONDITION value ')' )*
            {
            	root_0 = (object)adaptor.GetNilNode();

            	char_literal10=(IToken)Match(input,22,FOLLOW_22_in_condExpression220); 
            		char_literal10_tree = (object)adaptor.Create(char_literal10);
            		adaptor.AddChild(root_0, char_literal10_tree);

            	PushFollow(FOLLOW_value_in_condExpression222);
            	value11 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value11.Tree);
            	CONDITION12=(IToken)Match(input,CONDITION,FOLLOW_CONDITION_in_condExpression224); 
            		CONDITION12_tree = (object)adaptor.Create(CONDITION12);
            		adaptor.AddChild(root_0, CONDITION12_tree);

            	PushFollow(FOLLOW_value_in_condExpression226);
            	value13 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value13.Tree);
            	char_literal14=(IToken)Match(input,23,FOLLOW_23_in_condExpression228); 
            		char_literal14_tree = (object)adaptor.Create(char_literal14);
            		adaptor.AddChild(root_0, char_literal14_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:79:49: ( ANDOR '(' value CONDITION value ')' )*
            	do 
            	{
            	    int alt4 = 2;
            	    int LA4_0 = input.LA(1);

            	    if ( (LA4_0 == ANDOR) )
            	    {
            	        alt4 = 1;
            	    }


            	    switch (alt4) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:79:50: ANDOR '(' value CONDITION value ')'
            			    {
            			    	ANDOR15=(IToken)Match(input,ANDOR,FOLLOW_ANDOR_in_condExpression231); 
            			    		ANDOR15_tree = (object)adaptor.Create(ANDOR15);
            			    		adaptor.AddChild(root_0, ANDOR15_tree);

            			    	char_literal16=(IToken)Match(input,22,FOLLOW_22_in_condExpression233); 
            			    		char_literal16_tree = (object)adaptor.Create(char_literal16);
            			    		adaptor.AddChild(root_0, char_literal16_tree);

            			    	PushFollow(FOLLOW_value_in_condExpression234);
            			    	value17 = value();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, value17.Tree);
            			    	CONDITION18=(IToken)Match(input,CONDITION,FOLLOW_CONDITION_in_condExpression236); 
            			    		CONDITION18_tree = (object)adaptor.Create(CONDITION18);
            			    		adaptor.AddChild(root_0, CONDITION18_tree);

            			    	PushFollow(FOLLOW_value_in_condExpression238);
            			    	value19 = value();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, value19.Tree);
            			    	char_literal20=(IToken)Match(input,23,FOLLOW_23_in_condExpression240); 
            			    		char_literal20_tree = (object)adaptor.Create(char_literal20);
            			    		adaptor.AddChild(root_0, char_literal20_tree);


            			    }
            			    break;

            			default:
            			    goto loop4;
            	    }
            	} while (true);

            	loop4:
            		;	// Stops C# compiler whining that label 'loop4' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "condExpression"

    public class mul_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "mul"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:83:1: mul : value ( ( '*' | '/' ) value )* ;
    public MatlabParser.mul_return mul() // throws RecognitionException [1]
    {   
        MatlabParser.mul_return retval = new MatlabParser.mul_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken set22 = null;
        MatlabParser.value_return value21 = default(MatlabParser.value_return);

        MatlabParser.value_return value23 = default(MatlabParser.value_return);


        object set22_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:83:5: ( value ( ( '*' | '/' ) value )* )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:83:9: value ( ( '*' | '/' ) value )*
            {
            	root_0 = (object)adaptor.GetNilNode();

            	PushFollow(FOLLOW_value_in_mul262);
            	value21 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value21.Tree);
            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:83:15: ( ( '*' | '/' ) value )*
            	do 
            	{
            	    int alt5 = 2;
            	    int LA5_0 = input.LA(1);

            	    if ( ((LA5_0 >= 28 && LA5_0 <= 29)) )
            	    {
            	        alt5 = 1;
            	    }


            	    switch (alt5) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:83:16: ( '*' | '/' ) value
            			    {
            			    	set22 = (IToken)input.LT(1);
            			    	if ( (input.LA(1) >= 28 && input.LA(1) <= 29) ) 
            			    	{
            			    	    input.Consume();
            			    	    adaptor.AddChild(root_0, (object)adaptor.Create(set22));
            			    	    state.errorRecovery = false;
            			    	}
            			    	else 
            			    	{
            			    	    MismatchedSetException mse = new MismatchedSetException(null,input);
            			    	    throw mse;
            			    	}

            			    	PushFollow(FOLLOW_value_in_mul271);
            			    	value23 = value();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, value23.Tree);

            			    }
            			    break;

            			default:
            			    goto loop5;
            	    }
            	} while (true);

            	loop5:
            		;	// Stops C# compiler whining that label 'loop5' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "mul"

    public class atom_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "atom"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:86:1: atom : ( ID | INT | DOUBLE );
    public MatlabParser.atom_return atom() // throws RecognitionException [1]
    {   
        MatlabParser.atom_return retval = new MatlabParser.atom_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken set24 = null;

        object set24_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:86:5: ( ID | INT | DOUBLE )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:
            {
            	root_0 = (object)adaptor.GetNilNode();

            	set24 = (IToken)input.LT(1);
            	if ( input.LA(1) == ID || (input.LA(1) >= INT && input.LA(1) <= DOUBLE) ) 
            	{
            	    input.Consume();
            	    adaptor.AddChild(root_0, (object)adaptor.Create(set24));
            	    state.errorRecovery = false;
            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "atom"

    public class vars_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "vars"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:91:1: vars : ( param ',' )* param ;
    public MatlabParser.vars_return vars() // throws RecognitionException [1]
    {   
        MatlabParser.vars_return retval = new MatlabParser.vars_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken char_literal26 = null;
        MatlabParser.param_return param25 = default(MatlabParser.param_return);

        MatlabParser.param_return param27 = default(MatlabParser.param_return);


        object char_literal26_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:92:5: ( ( param ',' )* param )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:92:7: ( param ',' )* param
            {
            	root_0 = (object)adaptor.GetNilNode();

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:92:7: ( param ',' )*
            	do 
            	{
            	    int alt6 = 2;
            	    int LA6_0 = input.LA(1);

            	    if ( (LA6_0 == ID) )
            	    {
            	        int LA6_1 = input.LA(2);

            	        if ( (LA6_1 == 24) )
            	        {
            	            alt6 = 1;
            	        }


            	    }


            	    switch (alt6) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:92:8: param ','
            			    {
            			    	PushFollow(FOLLOW_param_in_vars330);
            			    	param25 = param();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, param25.Tree);
            			    	char_literal26=(IToken)Match(input,24,FOLLOW_24_in_vars332); 
            			    		char_literal26_tree = (object)adaptor.Create(char_literal26);
            			    		adaptor.AddChild(root_0, char_literal26_tree);


            			    }
            			    break;

            			default:
            			    goto loop6;
            	    }
            	} while (true);

            	loop6:
            		;	// Stops C# compiler whining that label 'loop6' has no statements

            	PushFollow(FOLLOW_param_in_vars336);
            	param27 = param();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, param27.Tree);

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "vars"

    public class param_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "param"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:95:1: param : id1= ID ;
    public MatlabParser.param_return param() // throws RecognitionException [1]
    {   
        MatlabParser.param_return retval = new MatlabParser.param_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken id1 = null;

        object id1_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:95:7: (id1= ID )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:95:9: id1= ID
            {
            	root_0 = (object)adaptor.GetNilNode();

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_param351); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);


            			tokens.InsertBefore(id1, "Matrix ");
            			parameters.Add(id1.Text);
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "param"

    public class statement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "statement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:102:1: statement : ( assignmentStatement | structuredStatement );
    public MatlabParser.statement_return statement() // throws RecognitionException [1]
    {   
        MatlabParser.statement_return retval = new MatlabParser.statement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.assignmentStatement_return assignmentStatement28 = default(MatlabParser.assignmentStatement_return);

        MatlabParser.structuredStatement_return structuredStatement29 = default(MatlabParser.structuredStatement_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:103:5: ( assignmentStatement | structuredStatement )
            int alt7 = 2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0 == ID || LA7_0 == ID3D || LA7_0 == 30 || LA7_0 == 38 || LA7_0 == 57) )
            {
                alt7 = 1;
            }
            else if ( (LA7_0 == IF || (LA7_0 >= WHILE && LA7_0 <= FOR) || LA7_0 == ELSE) )
            {
                alt7 = 2;
            }
            else 
            {
                NoViableAltException nvae_d7s0 =
                    new NoViableAltException("", 7, 0, input);

                throw nvae_d7s0;
            }
            switch (alt7) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:103:7: assignmentStatement
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_assignmentStatement_in_statement368);
                    	assignmentStatement28 = assignmentStatement();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, assignmentStatement28.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:103:29: structuredStatement
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_structuredStatement_in_statement372);
                    	structuredStatement29 = structuredStatement();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, structuredStatement29.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "statement"

    public class simpleStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "simpleStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:106:1: simpleStatement : assignmentStatement ;
    public MatlabParser.simpleStatement_return simpleStatement() // throws RecognitionException [1]
    {   
        MatlabParser.simpleStatement_return retval = new MatlabParser.simpleStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.assignmentStatement_return assignmentStatement30 = default(MatlabParser.assignmentStatement_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:107:5: ( assignmentStatement )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:107:7: assignmentStatement
            {
            	root_0 = (object)adaptor.GetNilNode();

            	PushFollow(FOLLOW_assignmentStatement_in_simpleStatement390);
            	assignmentStatement30 = assignmentStatement();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, assignmentStatement30.Tree);

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "simpleStatement"

    public class assignmentStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "assignmentStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:111:1: assignmentStatement : ( assign | vectorAssign | returnKeyword | complexFunction | statement3d );
    public MatlabParser.assignmentStatement_return assignmentStatement() // throws RecognitionException [1]
    {   
        MatlabParser.assignmentStatement_return retval = new MatlabParser.assignmentStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.assign_return assign31 = default(MatlabParser.assign_return);

        MatlabParser.vectorAssign_return vectorAssign32 = default(MatlabParser.vectorAssign_return);

        MatlabParser.returnKeyword_return returnKeyword33 = default(MatlabParser.returnKeyword_return);

        MatlabParser.complexFunction_return complexFunction34 = default(MatlabParser.complexFunction_return);

        MatlabParser.statement3d_return statement3d35 = default(MatlabParser.statement3d_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:112:5: ( assign | vectorAssign | returnKeyword | complexFunction | statement3d )
            int alt8 = 5;
            switch ( input.LA(1) ) 
            {
            case ID:
            	{
                int LA8_1 = input.LA(2);

                if ( (LA8_1 == 21) )
                {
                    int LA8_6 = input.LA(3);

                    if ( (LA8_6 == ID3D) )
                    {
                        alt8 = 5;
                    }
                    else if ( (LA8_6 == ID || (LA8_6 >= INT && LA8_6 <= DOUBLE) || LA8_6 == 26 || LA8_6 == 30 || LA8_6 == 35 || (LA8_6 >= 39 && LA8_6 <= 43) || (LA8_6 >= 45 && LA8_6 <= 56) || (LA8_6 >= 58 && LA8_6 <= 59)) )
                    {
                        alt8 = 1;
                    }
                    else 
                    {
                        NoViableAltException nvae_d8s6 =
                            new NoViableAltException("", 8, 6, input);

                        throw nvae_d8s6;
                    }
                }
                else if ( (LA8_1 == 22) )
                {
                    alt8 = 2;
                }
                else 
                {
                    NoViableAltException nvae_d8s1 =
                        new NoViableAltException("", 8, 1, input);

                    throw nvae_d8s1;
                }
                }
                break;
            case 57:
            	{
                alt8 = 1;
                }
                break;
            case 38:
            	{
                alt8 = 3;
                }
                break;
            case 30:
            	{
                alt8 = 4;
                }
                break;
            case ID3D:
            	{
                alt8 = 5;
                }
                break;
            	default:
            	    NoViableAltException nvae_d8s0 =
            	        new NoViableAltException("", 8, 0, input);

            	    throw nvae_d8s0;
            }

            switch (alt8) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:112:7: assign
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_assign_in_assignmentStatement412);
                    	assign31 = assign();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, assign31.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:112:16: vectorAssign
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_vectorAssign_in_assignmentStatement416);
                    	vectorAssign32 = vectorAssign();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, vectorAssign32.Tree);

                    }
                    break;
                case 3 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:112:32: returnKeyword
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_returnKeyword_in_assignmentStatement421);
                    	returnKeyword33 = returnKeyword();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, returnKeyword33.Tree);

                    }
                    break;
                case 4 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:112:48: complexFunction
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_complexFunction_in_assignmentStatement425);
                    	complexFunction34 = complexFunction();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, complexFunction34.Tree);

                    }
                    break;
                case 5 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:112:66: statement3d
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_statement3d_in_assignmentStatement429);
                    	statement3d35 = statement3d();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, statement3d35.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "assignmentStatement"

    public class scalarAssign_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "scalarAssign"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:115:1: scalarAssign : mx1= ID curl1= '(' value ',' value curl2= ')' seq= '=' expression last= ';' ;
    public MatlabParser.scalarAssign_return scalarAssign() // throws RecognitionException [1]
    {   
        MatlabParser.scalarAssign_return retval = new MatlabParser.scalarAssign_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken mx1 = null;
        IToken curl1 = null;
        IToken curl2 = null;
        IToken seq = null;
        IToken last = null;
        IToken char_literal37 = null;
        MatlabParser.value_return value36 = default(MatlabParser.value_return);

        MatlabParser.value_return value38 = default(MatlabParser.value_return);

        MatlabParser.expression_return expression39 = default(MatlabParser.expression_return);


        object mx1_tree=null;
        object curl1_tree=null;
        object curl2_tree=null;
        object seq_tree=null;
        object last_tree=null;
        object char_literal37_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:116:2: (mx1= ID curl1= '(' value ',' value curl2= ')' seq= '=' expression last= ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:116:5: mx1= ID curl1= '(' value ',' value curl2= ')' seq= '=' expression last= ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	mx1=(IToken)Match(input,ID,FOLLOW_ID_in_scalarAssign448); 
            		mx1_tree = (object)adaptor.Create(mx1);
            		adaptor.AddChild(root_0, mx1_tree);

            	curl1=(IToken)Match(input,22,FOLLOW_22_in_scalarAssign452); 
            		curl1_tree = (object)adaptor.Create(curl1);
            		adaptor.AddChild(root_0, curl1_tree);

            	PushFollow(FOLLOW_value_in_scalarAssign454);
            	value36 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value36.Tree);
            	char_literal37=(IToken)Match(input,24,FOLLOW_24_in_scalarAssign456); 
            		char_literal37_tree = (object)adaptor.Create(char_literal37);
            		adaptor.AddChild(root_0, char_literal37_tree);

            	PushFollow(FOLLOW_value_in_scalarAssign458);
            	value38 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value38.Tree);
            	curl2=(IToken)Match(input,23,FOLLOW_23_in_scalarAssign462); 
            		curl2_tree = (object)adaptor.Create(curl2);
            		adaptor.AddChild(root_0, curl2_tree);

            	seq=(IToken)Match(input,21,FOLLOW_21_in_scalarAssign466); 
            		seq_tree = (object)adaptor.Create(seq);
            		adaptor.AddChild(root_0, seq_tree);

            	PushFollow(FOLLOW_expression_in_scalarAssign468);
            	expression39 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression39.Tree);
            	last=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_scalarAssign472); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Replace(curl1, "[");
            			tokens.Replace(curl2, "]");
            			tokens.Replace(seq, "= (int)");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "scalarAssign"

    public class complexFunction_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "complexFunction"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:124:1: complexFunction : ( minimumIndexVector | sizeEval | stepfcm | validsumsqures | innerComplexFunction );
    public MatlabParser.complexFunction_return complexFunction() // throws RecognitionException [1]
    {   
        MatlabParser.complexFunction_return retval = new MatlabParser.complexFunction_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.minimumIndexVector_return minimumIndexVector40 = default(MatlabParser.minimumIndexVector_return);

        MatlabParser.sizeEval_return sizeEval41 = default(MatlabParser.sizeEval_return);

        MatlabParser.stepfcm_return stepfcm42 = default(MatlabParser.stepfcm_return);

        MatlabParser.validsumsqures_return validsumsqures43 = default(MatlabParser.validsumsqures_return);

        MatlabParser.innerComplexFunction_return innerComplexFunction44 = default(MatlabParser.innerComplexFunction_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:125:2: ( minimumIndexVector | sizeEval | stepfcm | validsumsqures | innerComplexFunction )
            int alt9 = 5;
            alt9 = dfa9.Predict(input);
            switch (alt9) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:125:4: minimumIndexVector
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_minimumIndexVector_in_complexFunction486);
                    	minimumIndexVector40 = minimumIndexVector();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, minimumIndexVector40.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:125:25: sizeEval
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_sizeEval_in_complexFunction490);
                    	sizeEval41 = sizeEval();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, sizeEval41.Tree);

                    }
                    break;
                case 3 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:125:36: stepfcm
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_stepfcm_in_complexFunction494);
                    	stepfcm42 = stepfcm();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, stepfcm42.Tree);

                    }
                    break;
                case 4 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:125:46: validsumsqures
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_validsumsqures_in_complexFunction498);
                    	validsumsqures43 = validsumsqures();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, validsumsqures43.Tree);

                    }
                    break;
                case 5 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:125:63: innerComplexFunction
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_innerComplexFunction_in_complexFunction502);
                    	innerComplexFunction44 = innerComplexFunction();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, innerComplexFunction44.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "complexFunction"

    public class validsumsqures_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "validsumsqures"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:128:1: validsumsqures : first= '[' st= ID ',' sw= ID ',' sb= ID ',' cintra= ID ',' cinter= ID ']' prelast= '=' func= 'validsumsqures' cur1= '(' data= ID ',' labels= ID ',' k= ID ')' last= ';' ;
    public MatlabParser.validsumsqures_return validsumsqures() // throws RecognitionException [1]
    {   
        MatlabParser.validsumsqures_return retval = new MatlabParser.validsumsqures_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken st = null;
        IToken sw = null;
        IToken sb = null;
        IToken cintra = null;
        IToken cinter = null;
        IToken prelast = null;
        IToken func = null;
        IToken cur1 = null;
        IToken data = null;
        IToken labels = null;
        IToken k = null;
        IToken last = null;
        IToken char_literal45 = null;
        IToken char_literal46 = null;
        IToken char_literal47 = null;
        IToken char_literal48 = null;
        IToken char_literal49 = null;
        IToken char_literal50 = null;
        IToken char_literal51 = null;
        IToken char_literal52 = null;

        object first_tree=null;
        object st_tree=null;
        object sw_tree=null;
        object sb_tree=null;
        object cintra_tree=null;
        object cinter_tree=null;
        object prelast_tree=null;
        object func_tree=null;
        object cur1_tree=null;
        object data_tree=null;
        object labels_tree=null;
        object k_tree=null;
        object last_tree=null;
        object char_literal45_tree=null;
        object char_literal46_tree=null;
        object char_literal47_tree=null;
        object char_literal48_tree=null;
        object char_literal49_tree=null;
        object char_literal50_tree=null;
        object char_literal51_tree=null;
        object char_literal52_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:129:2: (first= '[' st= ID ',' sw= ID ',' sb= ID ',' cintra= ID ',' cinter= ID ']' prelast= '=' func= 'validsumsqures' cur1= '(' data= ID ',' labels= ID ',' k= ID ')' last= ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:129:4: first= '[' st= ID ',' sw= ID ',' sb= ID ',' cintra= ID ',' cinter= ID ']' prelast= '=' func= 'validsumsqures' cur1= '(' data= ID ',' labels= ID ',' k= ID ')' last= ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,30,FOLLOW_30_in_validsumsqures515); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	st=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures519); 
            		st_tree = (object)adaptor.Create(st);
            		adaptor.AddChild(root_0, st_tree);

            	char_literal45=(IToken)Match(input,24,FOLLOW_24_in_validsumsqures521); 
            		char_literal45_tree = (object)adaptor.Create(char_literal45);
            		adaptor.AddChild(root_0, char_literal45_tree);

            	sw=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures525); 
            		sw_tree = (object)adaptor.Create(sw);
            		adaptor.AddChild(root_0, sw_tree);

            	char_literal46=(IToken)Match(input,24,FOLLOW_24_in_validsumsqures527); 
            		char_literal46_tree = (object)adaptor.Create(char_literal46);
            		adaptor.AddChild(root_0, char_literal46_tree);

            	sb=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures531); 
            		sb_tree = (object)adaptor.Create(sb);
            		adaptor.AddChild(root_0, sb_tree);

            	char_literal47=(IToken)Match(input,24,FOLLOW_24_in_validsumsqures533); 
            		char_literal47_tree = (object)adaptor.Create(char_literal47);
            		adaptor.AddChild(root_0, char_literal47_tree);

            	cintra=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures537); 
            		cintra_tree = (object)adaptor.Create(cintra);
            		adaptor.AddChild(root_0, cintra_tree);

            	char_literal48=(IToken)Match(input,24,FOLLOW_24_in_validsumsqures539); 
            		char_literal48_tree = (object)adaptor.Create(char_literal48);
            		adaptor.AddChild(root_0, char_literal48_tree);

            	cinter=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures543); 
            		cinter_tree = (object)adaptor.Create(cinter);
            		adaptor.AddChild(root_0, cinter_tree);

            	char_literal49=(IToken)Match(input,31,FOLLOW_31_in_validsumsqures545); 
            		char_literal49_tree = (object)adaptor.Create(char_literal49);
            		adaptor.AddChild(root_0, char_literal49_tree);

            	prelast=(IToken)Match(input,21,FOLLOW_21_in_validsumsqures549); 
            		prelast_tree = (object)adaptor.Create(prelast);
            		adaptor.AddChild(root_0, prelast_tree);

            	func=(IToken)Match(input,32,FOLLOW_32_in_validsumsqures553); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	cur1=(IToken)Match(input,22,FOLLOW_22_in_validsumsqures557); 
            		cur1_tree = (object)adaptor.Create(cur1);
            		adaptor.AddChild(root_0, cur1_tree);

            	data=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures561); 
            		data_tree = (object)adaptor.Create(data);
            		adaptor.AddChild(root_0, data_tree);

            	char_literal50=(IToken)Match(input,24,FOLLOW_24_in_validsumsqures563); 
            		char_literal50_tree = (object)adaptor.Create(char_literal50);
            		adaptor.AddChild(root_0, char_literal50_tree);

            	labels=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures567); 
            		labels_tree = (object)adaptor.Create(labels);
            		adaptor.AddChild(root_0, labels_tree);

            	char_literal51=(IToken)Match(input,24,FOLLOW_24_in_validsumsqures569); 
            		char_literal51_tree = (object)adaptor.Create(char_literal51);
            		adaptor.AddChild(root_0, char_literal51_tree);

            	k=(IToken)Match(input,ID,FOLLOW_ID_in_validsumsqures573); 
            		k_tree = (object)adaptor.Create(k);
            		adaptor.AddChild(root_0, k_tree);

            	char_literal52=(IToken)Match(input,23,FOLLOW_23_in_validsumsqures575); 
            		char_literal52_tree = (object)adaptor.Create(char_literal52);
            		adaptor.AddChild(root_0, char_literal52_tree);

            	last=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_validsumsqures579); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Delete(first, prelast);
            			tokens.Replace(func, "MatrixSupport.ValidSumSqures");
            			tokens.InsertAfter(cur1, "ref " + st.Text + ",ref " + sw.Text + ", " + "ref " + sb.Text + ", "
            			+ "ref " + cintra.Text + ",ref " + cinter.Text + ", ");
            			if(!identifiers.Contains(st.Text))
            			{
            				identifiers.Add(st.Text);
            			}
            			if(!identifiers.Contains(sw.Text))
            			{
            				identifiers.Add(sw.Text);
            			}
            			if(!identifiers.Contains(sb.Text))
            			{
            				identifiers.Add(sb.Text);
            			}
            			if(!identifiers.Contains(cintra.Text))
            			{
            				identifiers.Add(cintra.Text);
            			}
            			if(!identifiers.Contains(cinter.Text))
            			{
            				identifiers.Add(cinter.Text);
            			}
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "validsumsqures"

    public class stepfcm_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "stepfcm"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:158:1: stepfcm : first= '[' u= ID ',' center= ID ',' objfcn= ID ']' prelast= '=' func= 'stepfcm' cur1= '(' mx= ID ',' ID ',' id1= ID ',' id2= ID ')' last= ';' ;
    public MatlabParser.stepfcm_return stepfcm() // throws RecognitionException [1]
    {   
        MatlabParser.stepfcm_return retval = new MatlabParser.stepfcm_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken u = null;
        IToken center = null;
        IToken objfcn = null;
        IToken prelast = null;
        IToken func = null;
        IToken cur1 = null;
        IToken mx = null;
        IToken id1 = null;
        IToken id2 = null;
        IToken last = null;
        IToken char_literal53 = null;
        IToken char_literal54 = null;
        IToken char_literal55 = null;
        IToken char_literal56 = null;
        IToken ID57 = null;
        IToken char_literal58 = null;
        IToken char_literal59 = null;
        IToken char_literal60 = null;

        object first_tree=null;
        object u_tree=null;
        object center_tree=null;
        object objfcn_tree=null;
        object prelast_tree=null;
        object func_tree=null;
        object cur1_tree=null;
        object mx_tree=null;
        object id1_tree=null;
        object id2_tree=null;
        object last_tree=null;
        object char_literal53_tree=null;
        object char_literal54_tree=null;
        object char_literal55_tree=null;
        object char_literal56_tree=null;
        object ID57_tree=null;
        object char_literal58_tree=null;
        object char_literal59_tree=null;
        object char_literal60_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:158:9: (first= '[' u= ID ',' center= ID ',' objfcn= ID ']' prelast= '=' func= 'stepfcm' cur1= '(' mx= ID ',' ID ',' id1= ID ',' id2= ID ')' last= ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:158:11: first= '[' u= ID ',' center= ID ',' objfcn= ID ']' prelast= '=' func= 'stepfcm' cur1= '(' mx= ID ',' ID ',' id1= ID ',' id2= ID ')' last= ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,30,FOLLOW_30_in_stepfcm594); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	u=(IToken)Match(input,ID,FOLLOW_ID_in_stepfcm598); 
            		u_tree = (object)adaptor.Create(u);
            		adaptor.AddChild(root_0, u_tree);

            	char_literal53=(IToken)Match(input,24,FOLLOW_24_in_stepfcm600); 
            		char_literal53_tree = (object)adaptor.Create(char_literal53);
            		adaptor.AddChild(root_0, char_literal53_tree);

            	center=(IToken)Match(input,ID,FOLLOW_ID_in_stepfcm604); 
            		center_tree = (object)adaptor.Create(center);
            		adaptor.AddChild(root_0, center_tree);

            	char_literal54=(IToken)Match(input,24,FOLLOW_24_in_stepfcm606); 
            		char_literal54_tree = (object)adaptor.Create(char_literal54);
            		adaptor.AddChild(root_0, char_literal54_tree);

            	objfcn=(IToken)Match(input,ID,FOLLOW_ID_in_stepfcm610); 
            		objfcn_tree = (object)adaptor.Create(objfcn);
            		adaptor.AddChild(root_0, objfcn_tree);

            	char_literal55=(IToken)Match(input,31,FOLLOW_31_in_stepfcm612); 
            		char_literal55_tree = (object)adaptor.Create(char_literal55);
            		adaptor.AddChild(root_0, char_literal55_tree);

            	prelast=(IToken)Match(input,21,FOLLOW_21_in_stepfcm616); 
            		prelast_tree = (object)adaptor.Create(prelast);
            		adaptor.AddChild(root_0, prelast_tree);

            	func=(IToken)Match(input,33,FOLLOW_33_in_stepfcm620); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	cur1=(IToken)Match(input,22,FOLLOW_22_in_stepfcm624); 
            		cur1_tree = (object)adaptor.Create(cur1);
            		adaptor.AddChild(root_0, cur1_tree);

            	mx=(IToken)Match(input,ID,FOLLOW_ID_in_stepfcm628); 
            		mx_tree = (object)adaptor.Create(mx);
            		adaptor.AddChild(root_0, mx_tree);

            	char_literal56=(IToken)Match(input,24,FOLLOW_24_in_stepfcm630); 
            		char_literal56_tree = (object)adaptor.Create(char_literal56);
            		adaptor.AddChild(root_0, char_literal56_tree);

            	ID57=(IToken)Match(input,ID,FOLLOW_ID_in_stepfcm632); 
            		ID57_tree = (object)adaptor.Create(ID57);
            		adaptor.AddChild(root_0, ID57_tree);

            	char_literal58=(IToken)Match(input,24,FOLLOW_24_in_stepfcm634); 
            		char_literal58_tree = (object)adaptor.Create(char_literal58);
            		adaptor.AddChild(root_0, char_literal58_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_stepfcm638); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal59=(IToken)Match(input,24,FOLLOW_24_in_stepfcm640); 
            		char_literal59_tree = (object)adaptor.Create(char_literal59);
            		adaptor.AddChild(root_0, char_literal59_tree);

            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_stepfcm644); 
            		id2_tree = (object)adaptor.Create(id2);
            		adaptor.AddChild(root_0, id2_tree);

            	char_literal60=(IToken)Match(input,23,FOLLOW_23_in_stepfcm646); 
            		char_literal60_tree = (object)adaptor.Create(char_literal60);
            		adaptor.AddChild(root_0, char_literal60_tree);

            	last=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_stepfcm650); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Delete(first, prelast);
            			tokens.Replace(func, "FCM.StepFCM");
            			tokens.InsertAfter(cur1, "ref " + u.Text + ",ref " + center.Text + ", " + "ref " + objfcn.Text + ", ");
            			tokens.InsertBefore(id1, "(int)");
            			tokens.InsertBefore(id2, "(double)");
            			if(!identifiers.Contains(center.Text))
            			{
            				identifiers.Add(center.Text);
            			}
            			
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "stepfcm"

    public class vectorAssign_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "vectorAssign"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:173:1: vectorAssign : mx1= ID curl1= '(' (column= ':' ',' )* id1= value (semi= ',' ':' )* ( ',' secondArg= ID )* curl2= ')' seq= '=' expression last= ';' ;
    public MatlabParser.vectorAssign_return vectorAssign() // throws RecognitionException [1]
    {   
        MatlabParser.vectorAssign_return retval = new MatlabParser.vectorAssign_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken mx1 = null;
        IToken curl1 = null;
        IToken column = null;
        IToken semi = null;
        IToken secondArg = null;
        IToken curl2 = null;
        IToken seq = null;
        IToken last = null;
        IToken char_literal61 = null;
        IToken char_literal62 = null;
        IToken char_literal63 = null;
        MatlabParser.value_return id1 = default(MatlabParser.value_return);

        MatlabParser.expression_return expression64 = default(MatlabParser.expression_return);


        object mx1_tree=null;
        object curl1_tree=null;
        object column_tree=null;
        object semi_tree=null;
        object secondArg_tree=null;
        object curl2_tree=null;
        object seq_tree=null;
        object last_tree=null;
        object char_literal61_tree=null;
        object char_literal62_tree=null;
        object char_literal63_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:2: (mx1= ID curl1= '(' (column= ':' ',' )* id1= value (semi= ',' ':' )* ( ',' secondArg= ID )* curl2= ')' seq= '=' expression last= ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:4: mx1= ID curl1= '(' (column= ':' ',' )* id1= value (semi= ',' ':' )* ( ',' secondArg= ID )* curl2= ')' seq= '=' expression last= ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	mx1=(IToken)Match(input,ID,FOLLOW_ID_in_vectorAssign666); 
            		mx1_tree = (object)adaptor.Create(mx1);
            		adaptor.AddChild(root_0, mx1_tree);

            	curl1=(IToken)Match(input,22,FOLLOW_22_in_vectorAssign670); 
            		curl1_tree = (object)adaptor.Create(curl1);
            		adaptor.AddChild(root_0, curl1_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:21: (column= ':' ',' )*
            	do 
            	{
            	    int alt10 = 2;
            	    int LA10_0 = input.LA(1);

            	    if ( (LA10_0 == 34) )
            	    {
            	        alt10 = 1;
            	    }


            	    switch (alt10) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:22: column= ':' ','
            			    {
            			    	column=(IToken)Match(input,34,FOLLOW_34_in_vectorAssign675); 
            			    		column_tree = (object)adaptor.Create(column);
            			    		adaptor.AddChild(root_0, column_tree);

            			    	char_literal61=(IToken)Match(input,24,FOLLOW_24_in_vectorAssign677); 
            			    		char_literal61_tree = (object)adaptor.Create(char_literal61);
            			    		adaptor.AddChild(root_0, char_literal61_tree);


            			    }
            			    break;

            			default:
            			    goto loop10;
            	    }
            	} while (true);

            	loop10:
            		;	// Stops C# compiler whining that label 'loop10' has no statements

            	PushFollow(FOLLOW_value_in_vectorAssign684);
            	id1 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, id1.Tree);
            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:50: (semi= ',' ':' )*
            	do 
            	{
            	    int alt11 = 2;
            	    int LA11_0 = input.LA(1);

            	    if ( (LA11_0 == 24) )
            	    {
            	        int LA11_1 = input.LA(2);

            	        if ( (LA11_1 == 34) )
            	        {
            	            alt11 = 1;
            	        }


            	    }


            	    switch (alt11) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:51: semi= ',' ':'
            			    {
            			    	semi=(IToken)Match(input,24,FOLLOW_24_in_vectorAssign689); 
            			    		semi_tree = (object)adaptor.Create(semi);
            			    		adaptor.AddChild(root_0, semi_tree);

            			    	char_literal62=(IToken)Match(input,34,FOLLOW_34_in_vectorAssign691); 
            			    		char_literal62_tree = (object)adaptor.Create(char_literal62);
            			    		adaptor.AddChild(root_0, char_literal62_tree);


            			    }
            			    break;

            			default:
            			    goto loop11;
            	    }
            	} while (true);

            	loop11:
            		;	// Stops C# compiler whining that label 'loop11' has no statements

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:66: ( ',' secondArg= ID )*
            	do 
            	{
            	    int alt12 = 2;
            	    int LA12_0 = input.LA(1);

            	    if ( (LA12_0 == 24) )
            	    {
            	        alt12 = 1;
            	    }


            	    switch (alt12) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:174:67: ',' secondArg= ID
            			    {
            			    	char_literal63=(IToken)Match(input,24,FOLLOW_24_in_vectorAssign696); 
            			    		char_literal63_tree = (object)adaptor.Create(char_literal63);
            			    		adaptor.AddChild(root_0, char_literal63_tree);

            			    	secondArg=(IToken)Match(input,ID,FOLLOW_ID_in_vectorAssign700); 
            			    		secondArg_tree = (object)adaptor.Create(secondArg);
            			    		adaptor.AddChild(root_0, secondArg_tree);


            			    }
            			    break;

            			default:
            			    goto loop12;
            	    }
            	} while (true);

            	loop12:
            		;	// Stops C# compiler whining that label 'loop12' has no statements

            	curl2=(IToken)Match(input,23,FOLLOW_23_in_vectorAssign706); 
            		curl2_tree = (object)adaptor.Create(curl2);
            		adaptor.AddChild(root_0, curl2_tree);

            	seq=(IToken)Match(input,21,FOLLOW_21_in_vectorAssign710); 
            		seq_tree = (object)adaptor.Create(seq);
            		adaptor.AddChild(root_0, seq_tree);

            	PushFollow(FOLLOW_expression_in_vectorAssign712);
            	expression64 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression64.Tree);
            	last=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_vectorAssign716); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			if(semi != null)
            			{
            				tokens.Delete(mx1, curl2);
            				tokens.InsertBefore(seq, "Matrix tempVector"+mx1.Text);
            				if(!identifiers.Contains(mx1.Text))
            				{
            					identifiers.Add(mx1.Text);
            				}
            				tokens.InsertAfter(last, "\nMatrixSupport.CreateAndSetRow(ref " + mx1.Text +", (Vector)" + "tempVector"+mx1.Text 
            					+ ", " 
            					+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
            					+ ");");
            			}
            			else if(secondArg != null)
            			{
            				tokens.Replace(curl1, "[");
            				tokens.Replace(curl2, "]");
            				tokens.Replace(seq, "= (double)");
            			}
            			else if(column != null)
            			{
            				
            				tokens.Delete(mx1, curl2);
            				tokens.InsertBefore(seq, "Matrix tempVector"+mx1.Text);
            				if(!identifiers.Contains(mx1.Text))
            				{
            					identifiers.Add(mx1.Text);
            				}
            				tokens.InsertAfter(last, "\nMatrixSupport.CreateAndSetColumn(ref " + mx1.Text +", (Vector)" + "tempVector"+mx1.Text 
            					+ ", " 
            					+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
            					+ ");");
            			}
            			else
            			{
            				tokens.Replace(curl1, "[");
            				tokens.Replace(curl2, ", 0]");
            				tokens.InsertAfter(seq, "(double)");
            			}
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "vectorAssign"

    public class innerComplexFunction_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "innerComplexFunction"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:218:1: innerComplexFunction : first= '[' id1= ID ',' id2= ID ',' id3= ID ']' eq= '=' ID br1= '(' ( ID ',' )* ID ')' ';' ;
    public MatlabParser.innerComplexFunction_return innerComplexFunction() // throws RecognitionException [1]
    {   
        MatlabParser.innerComplexFunction_return retval = new MatlabParser.innerComplexFunction_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken id1 = null;
        IToken id2 = null;
        IToken id3 = null;
        IToken eq = null;
        IToken br1 = null;
        IToken char_literal65 = null;
        IToken char_literal66 = null;
        IToken char_literal67 = null;
        IToken ID68 = null;
        IToken ID69 = null;
        IToken char_literal70 = null;
        IToken ID71 = null;
        IToken char_literal72 = null;
        IToken char_literal73 = null;

        object first_tree=null;
        object id1_tree=null;
        object id2_tree=null;
        object id3_tree=null;
        object eq_tree=null;
        object br1_tree=null;
        object char_literal65_tree=null;
        object char_literal66_tree=null;
        object char_literal67_tree=null;
        object ID68_tree=null;
        object ID69_tree=null;
        object char_literal70_tree=null;
        object ID71_tree=null;
        object char_literal72_tree=null;
        object char_literal73_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:219:2: (first= '[' id1= ID ',' id2= ID ',' id3= ID ']' eq= '=' ID br1= '(' ( ID ',' )* ID ')' ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:219:5: first= '[' id1= ID ',' id2= ID ',' id3= ID ']' eq= '=' ID br1= '(' ( ID ',' )* ID ')' ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,30,FOLLOW_30_in_innerComplexFunction734); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_innerComplexFunction738); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal65=(IToken)Match(input,24,FOLLOW_24_in_innerComplexFunction740); 
            		char_literal65_tree = (object)adaptor.Create(char_literal65);
            		adaptor.AddChild(root_0, char_literal65_tree);

            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_innerComplexFunction744); 
            		id2_tree = (object)adaptor.Create(id2);
            		adaptor.AddChild(root_0, id2_tree);

            	char_literal66=(IToken)Match(input,24,FOLLOW_24_in_innerComplexFunction746); 
            		char_literal66_tree = (object)adaptor.Create(char_literal66);
            		adaptor.AddChild(root_0, char_literal66_tree);

            	id3=(IToken)Match(input,ID,FOLLOW_ID_in_innerComplexFunction750); 
            		id3_tree = (object)adaptor.Create(id3);
            		adaptor.AddChild(root_0, id3_tree);

            	char_literal67=(IToken)Match(input,31,FOLLOW_31_in_innerComplexFunction752); 
            		char_literal67_tree = (object)adaptor.Create(char_literal67);
            		adaptor.AddChild(root_0, char_literal67_tree);

            	eq=(IToken)Match(input,21,FOLLOW_21_in_innerComplexFunction756); 
            		eq_tree = (object)adaptor.Create(eq);
            		adaptor.AddChild(root_0, eq_tree);

            	ID68=(IToken)Match(input,ID,FOLLOW_ID_in_innerComplexFunction758); 
            		ID68_tree = (object)adaptor.Create(ID68);
            		adaptor.AddChild(root_0, ID68_tree);

            	br1=(IToken)Match(input,22,FOLLOW_22_in_innerComplexFunction762); 
            		br1_tree = (object)adaptor.Create(br1);
            		adaptor.AddChild(root_0, br1_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:219:66: ( ID ',' )*
            	do 
            	{
            	    int alt13 = 2;
            	    int LA13_0 = input.LA(1);

            	    if ( (LA13_0 == ID) )
            	    {
            	        int LA13_1 = input.LA(2);

            	        if ( (LA13_1 == 24) )
            	        {
            	            alt13 = 1;
            	        }


            	    }


            	    switch (alt13) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:219:67: ID ','
            			    {
            			    	ID69=(IToken)Match(input,ID,FOLLOW_ID_in_innerComplexFunction765); 
            			    		ID69_tree = (object)adaptor.Create(ID69);
            			    		adaptor.AddChild(root_0, ID69_tree);

            			    	char_literal70=(IToken)Match(input,24,FOLLOW_24_in_innerComplexFunction767); 
            			    		char_literal70_tree = (object)adaptor.Create(char_literal70);
            			    		adaptor.AddChild(root_0, char_literal70_tree);


            			    }
            			    break;

            			default:
            			    goto loop13;
            	    }
            	} while (true);

            	loop13:
            		;	// Stops C# compiler whining that label 'loop13' has no statements

            	ID71=(IToken)Match(input,ID,FOLLOW_ID_in_innerComplexFunction771); 
            		ID71_tree = (object)adaptor.Create(ID71);
            		adaptor.AddChild(root_0, ID71_tree);

            	char_literal72=(IToken)Match(input,23,FOLLOW_23_in_innerComplexFunction773); 
            		char_literal72_tree = (object)adaptor.Create(char_literal72);
            		adaptor.AddChild(root_0, char_literal72_tree);

            	char_literal73=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_innerComplexFunction775); 
            		char_literal73_tree = (object)adaptor.Create(char_literal73);
            		adaptor.AddChild(root_0, char_literal73_tree);


            			tokens.Delete(first, eq);
            			tokens.Replace(br1, "(" + "ref " + id1.Text + ", "
            			+ "ref " + id2.Text + ", "
            			+ "ref " + id3.Text + ", ");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "innerComplexFunction"

    public class minimumIndexVector_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "minimumIndexVector"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:228:1: minimumIndexVector : first= '[' ID ',' vector= ID ']' '=' 'min(' mx= ID ',' '[]' ',' dim= INT ')' last= ';' ;
    public MatlabParser.minimumIndexVector_return minimumIndexVector() // throws RecognitionException [1]
    {   
        MatlabParser.minimumIndexVector_return retval = new MatlabParser.minimumIndexVector_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken vector = null;
        IToken mx = null;
        IToken dim = null;
        IToken last = null;
        IToken ID74 = null;
        IToken char_literal75 = null;
        IToken char_literal76 = null;
        IToken char_literal77 = null;
        IToken string_literal78 = null;
        IToken char_literal79 = null;
        IToken string_literal80 = null;
        IToken char_literal81 = null;
        IToken char_literal82 = null;

        object first_tree=null;
        object vector_tree=null;
        object mx_tree=null;
        object dim_tree=null;
        object last_tree=null;
        object ID74_tree=null;
        object char_literal75_tree=null;
        object char_literal76_tree=null;
        object char_literal77_tree=null;
        object string_literal78_tree=null;
        object char_literal79_tree=null;
        object string_literal80_tree=null;
        object char_literal81_tree=null;
        object char_literal82_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:229:2: (first= '[' ID ',' vector= ID ']' '=' 'min(' mx= ID ',' '[]' ',' dim= INT ')' last= ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:229:5: first= '[' ID ',' vector= ID ']' '=' 'min(' mx= ID ',' '[]' ',' dim= INT ')' last= ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,30,FOLLOW_30_in_minimumIndexVector793); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	ID74=(IToken)Match(input,ID,FOLLOW_ID_in_minimumIndexVector795); 
            		ID74_tree = (object)adaptor.Create(ID74);
            		adaptor.AddChild(root_0, ID74_tree);

            	char_literal75=(IToken)Match(input,24,FOLLOW_24_in_minimumIndexVector797); 
            		char_literal75_tree = (object)adaptor.Create(char_literal75);
            		adaptor.AddChild(root_0, char_literal75_tree);

            	vector=(IToken)Match(input,ID,FOLLOW_ID_in_minimumIndexVector801); 
            		vector_tree = (object)adaptor.Create(vector);
            		adaptor.AddChild(root_0, vector_tree);

            	char_literal76=(IToken)Match(input,31,FOLLOW_31_in_minimumIndexVector803); 
            		char_literal76_tree = (object)adaptor.Create(char_literal76);
            		adaptor.AddChild(root_0, char_literal76_tree);

            	char_literal77=(IToken)Match(input,21,FOLLOW_21_in_minimumIndexVector805); 
            		char_literal77_tree = (object)adaptor.Create(char_literal77);
            		adaptor.AddChild(root_0, char_literal77_tree);

            	string_literal78=(IToken)Match(input,35,FOLLOW_35_in_minimumIndexVector807); 
            		string_literal78_tree = (object)adaptor.Create(string_literal78);
            		adaptor.AddChild(root_0, string_literal78_tree);

            	mx=(IToken)Match(input,ID,FOLLOW_ID_in_minimumIndexVector811); 
            		mx_tree = (object)adaptor.Create(mx);
            		adaptor.AddChild(root_0, mx_tree);

            	char_literal79=(IToken)Match(input,24,FOLLOW_24_in_minimumIndexVector813); 
            		char_literal79_tree = (object)adaptor.Create(char_literal79);
            		adaptor.AddChild(root_0, char_literal79_tree);

            	string_literal80=(IToken)Match(input,36,FOLLOW_36_in_minimumIndexVector815); 
            		string_literal80_tree = (object)adaptor.Create(string_literal80);
            		adaptor.AddChild(root_0, string_literal80_tree);

            	char_literal81=(IToken)Match(input,24,FOLLOW_24_in_minimumIndexVector817); 
            		char_literal81_tree = (object)adaptor.Create(char_literal81);
            		adaptor.AddChild(root_0, char_literal81_tree);

            	dim=(IToken)Match(input,INT,FOLLOW_INT_in_minimumIndexVector821); 
            		dim_tree = (object)adaptor.Create(dim);
            		adaptor.AddChild(root_0, dim_tree);

            	char_literal82=(IToken)Match(input,23,FOLLOW_23_in_minimumIndexVector823); 
            		char_literal82_tree = (object)adaptor.Create(char_literal82);
            		adaptor.AddChild(root_0, char_literal82_tree);

            	last=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_minimumIndexVector827); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Delete(first, last);
            			if(!identifiers.Contains(vector.Text))
            			{
            				identifiers.Add(vector.Text);
            			}
            			tokens.InsertAfter(last, vector.Text + "=" + "MatrixSupport.RetriveMinimumIndexesVector(" + mx.Text + "," + dim.Text + ");");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "minimumIndexVector"

    public class error_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "error"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:240:1: error : 'error' '(' ( NAME )* ')' ';' ;
    public MatlabParser.error_return error() // throws RecognitionException [1]
    {   
        MatlabParser.error_return retval = new MatlabParser.error_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken string_literal83 = null;
        IToken char_literal84 = null;
        IToken NAME85 = null;
        IToken char_literal86 = null;
        IToken char_literal87 = null;

        object string_literal83_tree=null;
        object char_literal84_tree=null;
        object NAME85_tree=null;
        object char_literal86_tree=null;
        object char_literal87_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:241:2: ( 'error' '(' ( NAME )* ')' ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:241:5: 'error' '(' ( NAME )* ')' ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	string_literal83=(IToken)Match(input,37,FOLLOW_37_in_error842); 
            		string_literal83_tree = (object)adaptor.Create(string_literal83);
            		adaptor.AddChild(root_0, string_literal83_tree);

            	char_literal84=(IToken)Match(input,22,FOLLOW_22_in_error844); 
            		char_literal84_tree = (object)adaptor.Create(char_literal84);
            		adaptor.AddChild(root_0, char_literal84_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:241:18: ( NAME )*
            	do 
            	{
            	    int alt14 = 2;
            	    int LA14_0 = input.LA(1);

            	    if ( (LA14_0 == NAME) )
            	    {
            	        alt14 = 1;
            	    }


            	    switch (alt14) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:241:18: NAME
            			    {
            			    	NAME85=(IToken)Match(input,NAME,FOLLOW_NAME_in_error847); 
            			    		NAME85_tree = (object)adaptor.Create(NAME85);
            			    		adaptor.AddChild(root_0, NAME85_tree);


            			    }
            			    break;

            			default:
            			    goto loop14;
            	    }
            	} while (true);

            	loop14:
            		;	// Stops C# compiler whining that label 'loop14' has no statements

            	char_literal86=(IToken)Match(input,23,FOLLOW_23_in_error850); 
            		char_literal86_tree = (object)adaptor.Create(char_literal86);
            		adaptor.AddChild(root_0, char_literal86_tree);

            	char_literal87=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_error852); 
            		char_literal87_tree = (object)adaptor.Create(char_literal87);
            		adaptor.AddChild(root_0, char_literal87_tree);


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "error"

    public class returnKeyword_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "returnKeyword"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:244:1: returnKeyword : 'return' value ';' ;
    public MatlabParser.returnKeyword_return returnKeyword() // throws RecognitionException [1]
    {   
        MatlabParser.returnKeyword_return retval = new MatlabParser.returnKeyword_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken string_literal88 = null;
        IToken char_literal90 = null;
        MatlabParser.value_return value89 = default(MatlabParser.value_return);


        object string_literal88_tree=null;
        object char_literal90_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:244:15: ( 'return' value ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:244:17: 'return' value ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	string_literal88=(IToken)Match(input,38,FOLLOW_38_in_returnKeyword862); 
            		string_literal88_tree = (object)adaptor.Create(string_literal88);
            		adaptor.AddChild(root_0, string_literal88_tree);

            	PushFollow(FOLLOW_value_in_returnKeyword864);
            	value89 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value89.Tree);
            	char_literal90=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_returnKeyword866); 
            		char_literal90_tree = (object)adaptor.Create(char_literal90);
            		adaptor.AddChild(root_0, char_literal90_tree);


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "returnKeyword"

    public class funcAssign_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "funcAssign"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:247:1: funcAssign : num= ID eq= '=' function semi= ';' ;
    public MatlabParser.funcAssign_return funcAssign() // throws RecognitionException [1]
    {   
        MatlabParser.funcAssign_return retval = new MatlabParser.funcAssign_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken num = null;
        IToken eq = null;
        IToken semi = null;
        MatlabParser.function_return function91 = default(MatlabParser.function_return);


        object num_tree=null;
        object eq_tree=null;
        object semi_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:248:2: (num= ID eq= '=' function semi= ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:248:5: num= ID eq= '=' function semi= ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	num=(IToken)Match(input,ID,FOLLOW_ID_in_funcAssign880); 
            		num_tree = (object)adaptor.Create(num);
            		adaptor.AddChild(root_0, num_tree);

            	eq=(IToken)Match(input,21,FOLLOW_21_in_funcAssign884); 
            		eq_tree = (object)adaptor.Create(eq);
            		adaptor.AddChild(root_0, eq_tree);

            	PushFollow(FOLLOW_function_in_funcAssign886);
            	function91 = function();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, function91.Tree);
            	semi=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_funcAssign890); 
            		semi_tree = (object)adaptor.Create(semi);
            		adaptor.AddChild(root_0, semi_tree);


            			if (!identifiers.Contains(num.Text))
            			{
            				identifiers.Add(num.Text);
            				//tokens.InsertAfter(haven, "Matrix "+ num.Text + ";");
            				//tokens.InsertBefore(num, "Matrix ");
            				tokens.InsertAfter(eq, "(Matrix)");
            				//tokens.InsertBefore(semi, ")");
            			}
            			
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "funcAssign"

    public class function_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "function"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:262:1: function : ( addColumn | randPerm | size1 | index | minor | zeros | mulDistance | find | max | minimum | mean | vectorAsFunction1 | vectorAsFunction2 | initfcm | kmeans | abs | trace | ones | sum | sqrt | det | inv | transposed | exp | cell | innerFunction );
    public MatlabParser.function_return function() // throws RecognitionException [1]
    {   
        MatlabParser.function_return retval = new MatlabParser.function_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.addColumn_return addColumn92 = default(MatlabParser.addColumn_return);

        MatlabParser.randPerm_return randPerm93 = default(MatlabParser.randPerm_return);

        MatlabParser.size1_return size194 = default(MatlabParser.size1_return);

        MatlabParser.index_return index95 = default(MatlabParser.index_return);

        MatlabParser.minor_return minor96 = default(MatlabParser.minor_return);

        MatlabParser.zeros_return zeros97 = default(MatlabParser.zeros_return);

        MatlabParser.mulDistance_return mulDistance98 = default(MatlabParser.mulDistance_return);

        MatlabParser.find_return find99 = default(MatlabParser.find_return);

        MatlabParser.max_return max100 = default(MatlabParser.max_return);

        MatlabParser.minimum_return minimum101 = default(MatlabParser.minimum_return);

        MatlabParser.mean_return mean102 = default(MatlabParser.mean_return);

        MatlabParser.vectorAsFunction1_return vectorAsFunction1103 = default(MatlabParser.vectorAsFunction1_return);

        MatlabParser.vectorAsFunction2_return vectorAsFunction2104 = default(MatlabParser.vectorAsFunction2_return);

        MatlabParser.initfcm_return initfcm105 = default(MatlabParser.initfcm_return);

        MatlabParser.kmeans_return kmeans106 = default(MatlabParser.kmeans_return);

        MatlabParser.abs_return abs107 = default(MatlabParser.abs_return);

        MatlabParser.trace_return trace108 = default(MatlabParser.trace_return);

        MatlabParser.ones_return ones109 = default(MatlabParser.ones_return);

        MatlabParser.sum_return sum110 = default(MatlabParser.sum_return);

        MatlabParser.sqrt_return sqrt111 = default(MatlabParser.sqrt_return);

        MatlabParser.det_return det112 = default(MatlabParser.det_return);

        MatlabParser.inv_return inv113 = default(MatlabParser.inv_return);

        MatlabParser.transposed_return transposed114 = default(MatlabParser.transposed_return);

        MatlabParser.exp_return exp115 = default(MatlabParser.exp_return);

        MatlabParser.cell_return cell116 = default(MatlabParser.cell_return);

        MatlabParser.innerFunction_return innerFunction117 = default(MatlabParser.innerFunction_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:263:2: ( addColumn | randPerm | size1 | index | minor | zeros | mulDistance | find | max | minimum | mean | vectorAsFunction1 | vectorAsFunction2 | initfcm | kmeans | abs | trace | ones | sum | sqrt | det | inv | transposed | exp | cell | innerFunction )
            int alt15 = 26;
            alt15 = dfa15.Predict(input);
            switch (alt15) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:263:4: addColumn
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_addColumn_in_function904);
                    	addColumn92 = addColumn();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, addColumn92.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:264:4: randPerm
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_randPerm_in_function910);
                    	randPerm93 = randPerm();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, randPerm93.Tree);

                    }
                    break;
                case 3 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:265:4: size1
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_size1_in_function916);
                    	size194 = size1();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, size194.Tree);

                    }
                    break;
                case 4 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:266:4: index
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_index_in_function922);
                    	index95 = index();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, index95.Tree);

                    }
                    break;
                case 5 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:267:4: minor
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_minor_in_function927);
                    	minor96 = minor();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, minor96.Tree);

                    }
                    break;
                case 6 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:268:4: zeros
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_zeros_in_function932);
                    	zeros97 = zeros();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, zeros97.Tree);

                    }
                    break;
                case 7 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:269:4: mulDistance
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_mulDistance_in_function938);
                    	mulDistance98 = mulDistance();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, mulDistance98.Tree);

                    }
                    break;
                case 8 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:270:4: find
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_find_in_function944);
                    	find99 = find();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, find99.Tree);

                    }
                    break;
                case 9 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:271:4: max
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_max_in_function950);
                    	max100 = max();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, max100.Tree);

                    }
                    break;
                case 10 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:272:4: minimum
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_minimum_in_function955);
                    	minimum101 = minimum();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, minimum101.Tree);

                    }
                    break;
                case 11 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:273:4: mean
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_mean_in_function960);
                    	mean102 = mean();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, mean102.Tree);

                    }
                    break;
                case 12 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:274:4: vectorAsFunction1
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_vectorAsFunction1_in_function966);
                    	vectorAsFunction1103 = vectorAsFunction1();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, vectorAsFunction1103.Tree);

                    }
                    break;
                case 13 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:275:4: vectorAsFunction2
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_vectorAsFunction2_in_function971);
                    	vectorAsFunction2104 = vectorAsFunction2();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, vectorAsFunction2104.Tree);

                    }
                    break;
                case 14 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:276:4: initfcm
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_initfcm_in_function976);
                    	initfcm105 = initfcm();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, initfcm105.Tree);

                    }
                    break;
                case 15 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:277:4: kmeans
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_kmeans_in_function981);
                    	kmeans106 = kmeans();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, kmeans106.Tree);

                    }
                    break;
                case 16 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:278:4: abs
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_abs_in_function986);
                    	abs107 = abs();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, abs107.Tree);

                    }
                    break;
                case 17 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:279:4: trace
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_trace_in_function991);
                    	trace108 = trace();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, trace108.Tree);

                    }
                    break;
                case 18 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:280:4: ones
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_ones_in_function996);
                    	ones109 = ones();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, ones109.Tree);

                    }
                    break;
                case 19 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:281:4: sum
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_sum_in_function1001);
                    	sum110 = sum();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, sum110.Tree);

                    }
                    break;
                case 20 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:282:4: sqrt
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_sqrt_in_function1006);
                    	sqrt111 = sqrt();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, sqrt111.Tree);

                    }
                    break;
                case 21 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:283:4: det
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_det_in_function1011);
                    	det112 = det();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, det112.Tree);

                    }
                    break;
                case 22 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:284:4: inv
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_inv_in_function1016);
                    	inv113 = inv();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, inv113.Tree);

                    }
                    break;
                case 23 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:285:4: transposed
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_transposed_in_function1021);
                    	transposed114 = transposed();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, transposed114.Tree);

                    }
                    break;
                case 24 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:286:4: exp
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_exp_in_function1026);
                    	exp115 = exp();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, exp115.Tree);

                    }
                    break;
                case 25 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:287:4: cell
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_cell_in_function1031);
                    	cell116 = cell();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, cell116.Tree);

                    }
                    break;
                case 26 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:288:4: innerFunction
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_innerFunction_in_function1036);
                    	innerFunction117 = innerFunction();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, innerFunction117.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "function"

    public class trace_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "trace"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:292:1: trace : tr= 'trace(' ID last= ')' ;
    public MatlabParser.trace_return trace() // throws RecognitionException [1]
    {   
        MatlabParser.trace_return retval = new MatlabParser.trace_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken tr = null;
        IToken last = null;
        IToken ID118 = null;

        object tr_tree=null;
        object last_tree=null;
        object ID118_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:292:7: (tr= 'trace(' ID last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:292:9: tr= 'trace(' ID last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	tr=(IToken)Match(input,39,FOLLOW_39_in_trace1050); 
            		tr_tree = (object)adaptor.Create(tr);
            		adaptor.AddChild(root_0, tr_tree);

            	ID118=(IToken)Match(input,ID,FOLLOW_ID_in_trace1052); 
            		ID118_tree = (object)adaptor.Create(ID118);
            		adaptor.AddChild(root_0, ID118_tree);

            	last=(IToken)Match(input,23,FOLLOW_23_in_trace1056); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Delete(tr);
            			tokens.Replace(last, ".Trace()");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "trace"

    public class minimum_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "minimum"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:299:1: minimum : func= 'min(' ID ')' ;
    public MatlabParser.minimum_return minimum() // throws RecognitionException [1]
    {   
        MatlabParser.minimum_return retval = new MatlabParser.minimum_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken ID119 = null;
        IToken char_literal120 = null;

        object func_tree=null;
        object ID119_tree=null;
        object char_literal120_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:299:9: (func= 'min(' ID ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:299:11: func= 'min(' ID ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,35,FOLLOW_35_in_minimum1071); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	ID119=(IToken)Match(input,ID,FOLLOW_ID_in_minimum1073); 
            		ID119_tree = (object)adaptor.Create(ID119);
            		adaptor.AddChild(root_0, ID119_tree);

            	char_literal120=(IToken)Match(input,23,FOLLOW_23_in_minimum1075); 
            		char_literal120_tree = (object)adaptor.Create(char_literal120);
            		adaptor.AddChild(root_0, char_literal120_tree);


            			tokens.Replace(func, "MatrixSupport.Min(");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "minimum"

    public class minor_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "minor"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:305:1: minor : ID br= '(' row= ID ',' col1= ID ':' col2= ID br4= ')' ;
    public MatlabParser.minor_return minor() // throws RecognitionException [1]
    {   
        MatlabParser.minor_return retval = new MatlabParser.minor_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken br = null;
        IToken row = null;
        IToken col1 = null;
        IToken col2 = null;
        IToken br4 = null;
        IToken ID121 = null;
        IToken char_literal122 = null;
        IToken char_literal123 = null;

        object br_tree=null;
        object row_tree=null;
        object col1_tree=null;
        object col2_tree=null;
        object br4_tree=null;
        object ID121_tree=null;
        object char_literal122_tree=null;
        object char_literal123_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:305:7: ( ID br= '(' row= ID ',' col1= ID ':' col2= ID br4= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:305:9: ID br= '(' row= ID ',' col1= ID ':' col2= ID br4= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	ID121=(IToken)Match(input,ID,FOLLOW_ID_in_minor1088); 
            		ID121_tree = (object)adaptor.Create(ID121);
            		adaptor.AddChild(root_0, ID121_tree);

            	br=(IToken)Match(input,22,FOLLOW_22_in_minor1092); 
            		br_tree = (object)adaptor.Create(br);
            		adaptor.AddChild(root_0, br_tree);

            	row=(IToken)Match(input,ID,FOLLOW_ID_in_minor1096); 
            		row_tree = (object)adaptor.Create(row);
            		adaptor.AddChild(root_0, row_tree);

            	char_literal122=(IToken)Match(input,24,FOLLOW_24_in_minor1098); 
            		char_literal122_tree = (object)adaptor.Create(char_literal122);
            		adaptor.AddChild(root_0, char_literal122_tree);

            	col1=(IToken)Match(input,ID,FOLLOW_ID_in_minor1102); 
            		col1_tree = (object)adaptor.Create(col1);
            		adaptor.AddChild(root_0, col1_tree);

            	char_literal123=(IToken)Match(input,34,FOLLOW_34_in_minor1104); 
            		char_literal123_tree = (object)adaptor.Create(char_literal123);
            		adaptor.AddChild(root_0, char_literal123_tree);

            	col2=(IToken)Match(input,ID,FOLLOW_ID_in_minor1108); 
            		col2_tree = (object)adaptor.Create(col2);
            		adaptor.AddChild(root_0, col2_tree);

            	br4=(IToken)Match(input,23,FOLLOW_23_in_minor1112); 
            		br4_tree = (object)adaptor.Create(br4);
            		adaptor.AddChild(root_0, br4_tree);


            			tokens.Replace(br, br4, ".GetMatrix(" + row.Text + ", " + row.Text + ", " + col1.Text + ", " + col2.Text + ")");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "minor"

    public class max_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "max"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:311:1: max : func= 'max(' ID ')' ;
    public MatlabParser.max_return max() // throws RecognitionException [1]
    {   
        MatlabParser.max_return retval = new MatlabParser.max_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken ID124 = null;
        IToken char_literal125 = null;

        object func_tree=null;
        object ID124_tree=null;
        object char_literal125_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:311:5: (func= 'max(' ID ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:311:7: func= 'max(' ID ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,40,FOLLOW_40_in_max1127); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	ID124=(IToken)Match(input,ID,FOLLOW_ID_in_max1129); 
            		ID124_tree = (object)adaptor.Create(ID124);
            		adaptor.AddChild(root_0, ID124_tree);

            	char_literal125=(IToken)Match(input,23,FOLLOW_23_in_max1131); 
            		char_literal125_tree = (object)adaptor.Create(char_literal125);
            		adaptor.AddChild(root_0, char_literal125_tree);


            			tokens.Replace(func, "MatrixSupport.Max(");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "max"

    public class kmeans_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "kmeans"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:318:1: kmeans : func= 'kmeans(' ID ',' ID br= ')' ;
    public MatlabParser.kmeans_return kmeans() // throws RecognitionException [1]
    {   
        MatlabParser.kmeans_return retval = new MatlabParser.kmeans_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken br = null;
        IToken ID126 = null;
        IToken char_literal127 = null;
        IToken ID128 = null;

        object func_tree=null;
        object br_tree=null;
        object ID126_tree=null;
        object char_literal127_tree=null;
        object ID128_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:318:8: (func= 'kmeans(' ID ',' ID br= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:318:10: func= 'kmeans(' ID ',' ID br= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,41,FOLLOW_41_in_kmeans1147); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	ID126=(IToken)Match(input,ID,FOLLOW_ID_in_kmeans1149); 
            		ID126_tree = (object)adaptor.Create(ID126);
            		adaptor.AddChild(root_0, ID126_tree);

            	char_literal127=(IToken)Match(input,24,FOLLOW_24_in_kmeans1151); 
            		char_literal127_tree = (object)adaptor.Create(char_literal127);
            		adaptor.AddChild(root_0, char_literal127_tree);

            	ID128=(IToken)Match(input,ID,FOLLOW_ID_in_kmeans1153); 
            		ID128_tree = (object)adaptor.Create(ID128);
            		adaptor.AddChild(root_0, ID128_tree);

            	br=(IToken)Match(input,23,FOLLOW_23_in_kmeans1157); 
            		br_tree = (object)adaptor.Create(br);
            		adaptor.AddChild(root_0, br_tree);


            			tokens.Replace(func, "kMeans.kMeansCluster(ref c, ");
            			tokens.Replace(br, ", (Matrix)false)");
            			identifiers.Add("c");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "kmeans"

    public class cell_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "cell"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:327:1: cell : start= ID br1= '(' ID ',' ID br2= ')' ;
    public MatlabParser.cell_return cell() // throws RecognitionException [1]
    {   
        MatlabParser.cell_return retval = new MatlabParser.cell_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken start = null;
        IToken br1 = null;
        IToken br2 = null;
        IToken ID129 = null;
        IToken char_literal130 = null;
        IToken ID131 = null;

        object start_tree=null;
        object br1_tree=null;
        object br2_tree=null;
        object ID129_tree=null;
        object char_literal130_tree=null;
        object ID131_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:327:6: (start= ID br1= '(' ID ',' ID br2= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:327:8: start= ID br1= '(' ID ',' ID br2= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	start=(IToken)Match(input,ID,FOLLOW_ID_in_cell1173); 
            		start_tree = (object)adaptor.Create(start);
            		adaptor.AddChild(root_0, start_tree);

            	br1=(IToken)Match(input,22,FOLLOW_22_in_cell1177); 
            		br1_tree = (object)adaptor.Create(br1);
            		adaptor.AddChild(root_0, br1_tree);

            	ID129=(IToken)Match(input,ID,FOLLOW_ID_in_cell1179); 
            		ID129_tree = (object)adaptor.Create(ID129);
            		adaptor.AddChild(root_0, ID129_tree);

            	char_literal130=(IToken)Match(input,24,FOLLOW_24_in_cell1181); 
            		char_literal130_tree = (object)adaptor.Create(char_literal130);
            		adaptor.AddChild(root_0, char_literal130_tree);

            	ID131=(IToken)Match(input,ID,FOLLOW_ID_in_cell1183); 
            		ID131_tree = (object)adaptor.Create(ID131);
            		adaptor.AddChild(root_0, ID131_tree);

            	br2=(IToken)Match(input,23,FOLLOW_23_in_cell1187); 
            		br2_tree = (object)adaptor.Create(br2);
            		adaptor.AddChild(root_0, br2_tree);


            			tokens.Replace(br1, "[");
            			tokens.Replace(br2, "]");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "cell"

    public class sum_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "sum"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:334:1: sum : func= 'sum(' ID ')' ;
    public MatlabParser.sum_return sum() // throws RecognitionException [1]
    {   
        MatlabParser.sum_return retval = new MatlabParser.sum_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken ID132 = null;
        IToken char_literal133 = null;

        object func_tree=null;
        object ID132_tree=null;
        object char_literal133_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:334:5: (func= 'sum(' ID ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:334:7: func= 'sum(' ID ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,42,FOLLOW_42_in_sum1202); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	ID132=(IToken)Match(input,ID,FOLLOW_ID_in_sum1204); 
            		ID132_tree = (object)adaptor.Create(ID132);
            		adaptor.AddChild(root_0, ID132_tree);

            	char_literal133=(IToken)Match(input,23,FOLLOW_23_in_sum1206); 
            		char_literal133_tree = (object)adaptor.Create(char_literal133);
            		adaptor.AddChild(root_0, char_literal133_tree);


            			tokens.Replace(func, "MatrixSupport.Sum((Vector)");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "sum"

    public class exp_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "exp"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:340:1: exp : func= 'exp(' expression ')' ;
    public MatlabParser.exp_return exp() // throws RecognitionException [1]
    {   
        MatlabParser.exp_return retval = new MatlabParser.exp_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken char_literal135 = null;
        MatlabParser.expression_return expression134 = default(MatlabParser.expression_return);


        object func_tree=null;
        object char_literal135_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:340:5: (func= 'exp(' expression ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:340:7: func= 'exp(' expression ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,43,FOLLOW_43_in_exp1221); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	PushFollow(FOLLOW_expression_in_exp1223);
            	expression134 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression134.Tree);
            	char_literal135=(IToken)Match(input,23,FOLLOW_23_in_exp1225); 
            		char_literal135_tree = (object)adaptor.Create(char_literal135);
            		adaptor.AddChild(root_0, char_literal135_tree);


            			tokens.Replace(func, "Math.Exp(");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "exp"

    public class transposed_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "transposed"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:346:1: transposed : var= ID trans= '\\'' ;
    public MatlabParser.transposed_return transposed() // throws RecognitionException [1]
    {   
        MatlabParser.transposed_return retval = new MatlabParser.transposed_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken var = null;
        IToken trans = null;

        object var_tree=null;
        object trans_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:347:2: (var= ID trans= '\\'' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:347:4: var= ID trans= '\\''
            {
            	root_0 = (object)adaptor.GetNilNode();

            	var=(IToken)Match(input,ID,FOLLOW_ID_in_transposed1241); 
            		var_tree = (object)adaptor.Create(var);
            		adaptor.AddChild(root_0, var_tree);

            	trans=(IToken)Match(input,44,FOLLOW_44_in_transposed1245); 
            		trans_tree = (object)adaptor.Create(trans);
            		adaptor.AddChild(root_0, trans_tree);


            			tokens.Replace(trans, ".Transpose()");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "transposed"

    public class ones_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "ones"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:353:1: ones : func= 'ones(' value ',' value br= ')' ;
    public MatlabParser.ones_return ones() // throws RecognitionException [1]
    {   
        MatlabParser.ones_return retval = new MatlabParser.ones_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken br = null;
        IToken char_literal137 = null;
        MatlabParser.value_return value136 = default(MatlabParser.value_return);

        MatlabParser.value_return value138 = default(MatlabParser.value_return);


        object func_tree=null;
        object br_tree=null;
        object char_literal137_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:353:6: (func= 'ones(' value ',' value br= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:353:8: func= 'ones(' value ',' value br= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,45,FOLLOW_45_in_ones1260); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	PushFollow(FOLLOW_value_in_ones1262);
            	value136 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value136.Tree);
            	char_literal137=(IToken)Match(input,24,FOLLOW_24_in_ones1264); 
            		char_literal137_tree = (object)adaptor.Create(char_literal137);
            		adaptor.AddChild(root_0, char_literal137_tree);

            	PushFollow(FOLLOW_value_in_ones1266);
            	value138 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value138.Tree);
            	br=(IToken)Match(input,23,FOLLOW_23_in_ones1270); 
            		br_tree = (object)adaptor.Create(br);
            		adaptor.AddChild(root_0, br_tree);


            			tokens.Replace(func, "new Matrix(");
            			tokens.Replace(br, ", 1.0)");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "ones"

    public class inv_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "inv"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:360:1: inv : func= 'inv(' value br= ')' ;
    public MatlabParser.inv_return inv() // throws RecognitionException [1]
    {   
        MatlabParser.inv_return retval = new MatlabParser.inv_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken br = null;
        MatlabParser.value_return value139 = default(MatlabParser.value_return);


        object func_tree=null;
        object br_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:360:5: (func= 'inv(' value br= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:360:7: func= 'inv(' value br= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,46,FOLLOW_46_in_inv1285); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	PushFollow(FOLLOW_value_in_inv1287);
            	value139 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value139.Tree);
            	br=(IToken)Match(input,23,FOLLOW_23_in_inv1291); 
            		br_tree = (object)adaptor.Create(br);
            		adaptor.AddChild(root_0, br_tree);


            			tokens.Delete(func);
            			tokens.Replace(br, ".Inverse()");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "inv"

    public class sqrt_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "sqrt"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:367:1: sqrt : func= 'sqrt(' value ')' ;
    public MatlabParser.sqrt_return sqrt() // throws RecognitionException [1]
    {   
        MatlabParser.sqrt_return retval = new MatlabParser.sqrt_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken char_literal141 = null;
        MatlabParser.value_return value140 = default(MatlabParser.value_return);


        object func_tree=null;
        object char_literal141_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:367:6: (func= 'sqrt(' value ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:367:8: func= 'sqrt(' value ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,47,FOLLOW_47_in_sqrt1306); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	PushFollow(FOLLOW_value_in_sqrt1308);
            	value140 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value140.Tree);
            	char_literal141=(IToken)Match(input,23,FOLLOW_23_in_sqrt1310); 
            		char_literal141_tree = (object)adaptor.Create(char_literal141);
            		adaptor.AddChild(root_0, char_literal141_tree);


            			tokens.Replace(func, "Math.Sqrt(");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "sqrt"

    public class det_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "det"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:373:1: det : func= 'det(' value br= ')' ;
    public MatlabParser.det_return det() // throws RecognitionException [1]
    {   
        MatlabParser.det_return retval = new MatlabParser.det_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken br = null;
        MatlabParser.value_return value142 = default(MatlabParser.value_return);


        object func_tree=null;
        object br_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:373:5: (func= 'det(' value br= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:373:7: func= 'det(' value br= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,48,FOLLOW_48_in_det1325); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	PushFollow(FOLLOW_value_in_det1327);
            	value142 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value142.Tree);
            	br=(IToken)Match(input,23,FOLLOW_23_in_det1331); 
            		br_tree = (object)adaptor.Create(br);
            		adaptor.AddChild(root_0, br_tree);


            			tokens.Delete(func);
            			tokens.Replace(br, ".Determinant()");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "det"

    public class innerFunction_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "innerFunction"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:380:1: innerFunction : start= ID br1= '(' ( ID ',' )* ID ')' ;
    public MatlabParser.innerFunction_return innerFunction() // throws RecognitionException [1]
    {   
        MatlabParser.innerFunction_return retval = new MatlabParser.innerFunction_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken start = null;
        IToken br1 = null;
        IToken ID143 = null;
        IToken char_literal144 = null;
        IToken ID145 = null;
        IToken char_literal146 = null;

        object start_tree=null;
        object br1_tree=null;
        object ID143_tree=null;
        object char_literal144_tree=null;
        object ID145_tree=null;
        object char_literal146_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:381:2: (start= ID br1= '(' ( ID ',' )* ID ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:381:4: start= ID br1= '(' ( ID ',' )* ID ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	start=(IToken)Match(input,ID,FOLLOW_ID_in_innerFunction1347); 
            		start_tree = (object)adaptor.Create(start);
            		adaptor.AddChild(root_0, start_tree);

            	br1=(IToken)Match(input,22,FOLLOW_22_in_innerFunction1351); 
            		br1_tree = (object)adaptor.Create(br1);
            		adaptor.AddChild(root_0, br1_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:381:21: ( ID ',' )*
            	do 
            	{
            	    int alt16 = 2;
            	    int LA16_0 = input.LA(1);

            	    if ( (LA16_0 == ID) )
            	    {
            	        int LA16_1 = input.LA(2);

            	        if ( (LA16_1 == 24) )
            	        {
            	            alt16 = 1;
            	        }


            	    }


            	    switch (alt16) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:381:22: ID ','
            			    {
            			    	ID143=(IToken)Match(input,ID,FOLLOW_ID_in_innerFunction1354); 
            			    		ID143_tree = (object)adaptor.Create(ID143);
            			    		adaptor.AddChild(root_0, ID143_tree);

            			    	char_literal144=(IToken)Match(input,24,FOLLOW_24_in_innerFunction1356); 
            			    		char_literal144_tree = (object)adaptor.Create(char_literal144);
            			    		adaptor.AddChild(root_0, char_literal144_tree);


            			    }
            			    break;

            			default:
            			    goto loop16;
            	    }
            	} while (true);

            	loop16:
            		;	// Stops C# compiler whining that label 'loop16' has no statements

            	ID145=(IToken)Match(input,ID,FOLLOW_ID_in_innerFunction1360); 
            		ID145_tree = (object)adaptor.Create(ID145);
            		adaptor.AddChild(root_0, ID145_tree);

            	char_literal146=(IToken)Match(input,23,FOLLOW_23_in_innerFunction1362); 
            		char_literal146_tree = (object)adaptor.Create(char_literal146);
            		adaptor.AddChild(root_0, char_literal146_tree);


            			tokens.Replace(start, start.Text);
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "innerFunction"

    public class abs_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "abs"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:387:1: abs : start= 'abs' '(' expression ')' ;
    public MatlabParser.abs_return abs() // throws RecognitionException [1]
    {   
        MatlabParser.abs_return retval = new MatlabParser.abs_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken start = null;
        IToken char_literal147 = null;
        IToken char_literal149 = null;
        MatlabParser.expression_return expression148 = default(MatlabParser.expression_return);


        object start_tree=null;
        object char_literal147_tree=null;
        object char_literal149_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:387:5: (start= 'abs' '(' expression ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:387:7: start= 'abs' '(' expression ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	start=(IToken)Match(input,49,FOLLOW_49_in_abs1377); 
            		start_tree = (object)adaptor.Create(start);
            		adaptor.AddChild(root_0, start_tree);

            	char_literal147=(IToken)Match(input,22,FOLLOW_22_in_abs1379); 
            		char_literal147_tree = (object)adaptor.Create(char_literal147);
            		adaptor.AddChild(root_0, char_literal147_tree);

            	PushFollow(FOLLOW_expression_in_abs1381);
            	expression148 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression148.Tree);
            	char_literal149=(IToken)Match(input,23,FOLLOW_23_in_abs1383); 
            		char_literal149_tree = (object)adaptor.Create(char_literal149);
            		adaptor.AddChild(root_0, char_literal149_tree);


            			tokens.Replace(start, "(Matrix)Math.Abs");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "abs"

    public class initfcm_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "initfcm"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:393:1: initfcm : first= 'initfcm' '(' id1= ID ',' id2= ID last= ')' ;
    public MatlabParser.initfcm_return initfcm() // throws RecognitionException [1]
    {   
        MatlabParser.initfcm_return retval = new MatlabParser.initfcm_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken id1 = null;
        IToken id2 = null;
        IToken last = null;
        IToken char_literal150 = null;
        IToken char_literal151 = null;

        object first_tree=null;
        object id1_tree=null;
        object id2_tree=null;
        object last_tree=null;
        object char_literal150_tree=null;
        object char_literal151_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:393:9: (first= 'initfcm' '(' id1= ID ',' id2= ID last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:393:11: first= 'initfcm' '(' id1= ID ',' id2= ID last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,50,FOLLOW_50_in_initfcm1398); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	char_literal150=(IToken)Match(input,22,FOLLOW_22_in_initfcm1400); 
            		char_literal150_tree = (object)adaptor.Create(char_literal150);
            		adaptor.AddChild(root_0, char_literal150_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_initfcm1404); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal151=(IToken)Match(input,24,FOLLOW_24_in_initfcm1406); 
            		char_literal151_tree = (object)adaptor.Create(char_literal151);
            		adaptor.AddChild(root_0, char_literal151_tree);

            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_initfcm1410); 
            		id2_tree = (object)adaptor.Create(id2);
            		adaptor.AddChild(root_0, id2_tree);

            	last=(IToken)Match(input,23,FOLLOW_23_in_initfcm1414); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Replace(first, last, "FCM.InitFCM((int)" + id1.Text + ",(int) " + id2.Text + ")");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "initfcm"

    public class mean_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "mean"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:399:1: mean : first= 'mean' curl1= '(' id1= value com= ',' fil= INT last= ')' ;
    public MatlabParser.mean_return mean() // throws RecognitionException [1]
    {   
        MatlabParser.mean_return retval = new MatlabParser.mean_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken curl1 = null;
        IToken com = null;
        IToken fil = null;
        IToken last = null;
        MatlabParser.value_return id1 = default(MatlabParser.value_return);


        object first_tree=null;
        object curl1_tree=null;
        object com_tree=null;
        object fil_tree=null;
        object last_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:399:6: (first= 'mean' curl1= '(' id1= value com= ',' fil= INT last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:399:8: first= 'mean' curl1= '(' id1= value com= ',' fil= INT last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,51,FOLLOW_51_in_mean1429); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	curl1=(IToken)Match(input,22,FOLLOW_22_in_mean1433); 
            		curl1_tree = (object)adaptor.Create(curl1);
            		adaptor.AddChild(root_0, curl1_tree);

            	PushFollow(FOLLOW_value_in_mean1437);
            	id1 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, id1.Tree);
            	com=(IToken)Match(input,24,FOLLOW_24_in_mean1441); 
            		com_tree = (object)adaptor.Create(com);
            		adaptor.AddChild(root_0, com_tree);

            	fil=(IToken)Match(input,INT,FOLLOW_INT_in_mean1445); 
            		fil_tree = (object)adaptor.Create(fil);
            		adaptor.AddChild(root_0, fil_tree);

            	last=(IToken)Match(input,23,FOLLOW_23_in_mean1449); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Replace(first, curl1, "MatrixSupport.MeanMatrix(");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "mean"

    public class find_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "find"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:405:1: find : first= 'find' '(' mx= ID CONDITION filter= ID last= ')' ;
    public MatlabParser.find_return find() // throws RecognitionException [1]
    {   
        MatlabParser.find_return retval = new MatlabParser.find_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken mx = null;
        IToken filter = null;
        IToken last = null;
        IToken char_literal152 = null;
        IToken CONDITION153 = null;

        object first_tree=null;
        object mx_tree=null;
        object filter_tree=null;
        object last_tree=null;
        object char_literal152_tree=null;
        object CONDITION153_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:405:6: (first= 'find' '(' mx= ID CONDITION filter= ID last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:405:8: first= 'find' '(' mx= ID CONDITION filter= ID last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,52,FOLLOW_52_in_find1464); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	char_literal152=(IToken)Match(input,22,FOLLOW_22_in_find1466); 
            		char_literal152_tree = (object)adaptor.Create(char_literal152);
            		adaptor.AddChild(root_0, char_literal152_tree);

            	mx=(IToken)Match(input,ID,FOLLOW_ID_in_find1470); 
            		mx_tree = (object)adaptor.Create(mx);
            		adaptor.AddChild(root_0, mx_tree);

            	CONDITION153=(IToken)Match(input,CONDITION,FOLLOW_CONDITION_in_find1472); 
            		CONDITION153_tree = (object)adaptor.Create(CONDITION153);
            		adaptor.AddChild(root_0, CONDITION153_tree);

            	filter=(IToken)Match(input,ID,FOLLOW_ID_in_find1476); 
            		filter_tree = (object)adaptor.Create(filter);
            		adaptor.AddChild(root_0, filter_tree);

            	last=(IToken)Match(input,23,FOLLOW_23_in_find1480); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Replace(first, last, "MatrixSupport.Find(" + mx.Text + ", " + filter.Text + ")");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "find"

    public class vectorAsFunction1_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "vectorAsFunction1"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:411:1: vectorAsFunction1 : mx1= ID '(' id1= ID ',' ':' curl= ')' (trans= '\\'' )* ;
    public MatlabParser.vectorAsFunction1_return vectorAsFunction1() // throws RecognitionException [1]
    {   
        MatlabParser.vectorAsFunction1_return retval = new MatlabParser.vectorAsFunction1_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken mx1 = null;
        IToken id1 = null;
        IToken curl = null;
        IToken trans = null;
        IToken char_literal154 = null;
        IToken char_literal155 = null;
        IToken char_literal156 = null;

        object mx1_tree=null;
        object id1_tree=null;
        object curl_tree=null;
        object trans_tree=null;
        object char_literal154_tree=null;
        object char_literal155_tree=null;
        object char_literal156_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:412:2: (mx1= ID '(' id1= ID ',' ':' curl= ')' (trans= '\\'' )* )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:412:4: mx1= ID '(' id1= ID ',' ':' curl= ')' (trans= '\\'' )*
            {
            	root_0 = (object)adaptor.GetNilNode();

            	mx1=(IToken)Match(input,ID,FOLLOW_ID_in_vectorAsFunction11496); 
            		mx1_tree = (object)adaptor.Create(mx1);
            		adaptor.AddChild(root_0, mx1_tree);

            	char_literal154=(IToken)Match(input,22,FOLLOW_22_in_vectorAsFunction11498); 
            		char_literal154_tree = (object)adaptor.Create(char_literal154);
            		adaptor.AddChild(root_0, char_literal154_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_vectorAsFunction11502); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal155=(IToken)Match(input,24,FOLLOW_24_in_vectorAsFunction11504); 
            		char_literal155_tree = (object)adaptor.Create(char_literal155);
            		adaptor.AddChild(root_0, char_literal155_tree);

            	char_literal156=(IToken)Match(input,34,FOLLOW_34_in_vectorAsFunction11506); 
            		char_literal156_tree = (object)adaptor.Create(char_literal156);
            		adaptor.AddChild(root_0, char_literal156_tree);

            	curl=(IToken)Match(input,23,FOLLOW_23_in_vectorAsFunction11510); 
            		curl_tree = (object)adaptor.Create(curl);
            		adaptor.AddChild(root_0, curl_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:412:44: (trans= '\\'' )*
            	do 
            	{
            	    int alt17 = 2;
            	    int LA17_0 = input.LA(1);

            	    if ( (LA17_0 == 44) )
            	    {
            	        alt17 = 1;
            	    }


            	    switch (alt17) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:412:44: trans= '\\''
            			    {
            			    	trans=(IToken)Match(input,44,FOLLOW_44_in_vectorAsFunction11514); 
            			    		trans_tree = (object)adaptor.Create(trans);
            			    		adaptor.AddChild(root_0, trans_tree);


            			    }
            			    break;

            			default:
            			    goto loop17;
            	    }
            	} while (true);

            	loop17:
            		;	// Stops C# compiler whining that label 'loop17' has no statements


            			
            			if(trans != null)
            			{
            				tokens.Delete(trans);
            				tokens.Replace(mx1, curl, "Matrix.Transpose(MatrixSupport.GetMatrixByRow((Vector)" + id1.Text + ", " + mx1.Text + "))");
            			}
            			else
            			{
            				tokens.Replace(mx1, curl, "MatrixSupport.GetMatrixByRow((Vector)" + id1.Text + ", " + mx1.Text + ")");
            			}
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "vectorAsFunction1"

    public class vectorAsFunction2_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "vectorAsFunction2"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:428:1: vectorAsFunction2 : mx1= ID '(' ':' ',' id1= ID curl= ')' (trans= '\\'' )* ;
    public MatlabParser.vectorAsFunction2_return vectorAsFunction2() // throws RecognitionException [1]
    {   
        MatlabParser.vectorAsFunction2_return retval = new MatlabParser.vectorAsFunction2_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken mx1 = null;
        IToken id1 = null;
        IToken curl = null;
        IToken trans = null;
        IToken char_literal157 = null;
        IToken char_literal158 = null;
        IToken char_literal159 = null;

        object mx1_tree=null;
        object id1_tree=null;
        object curl_tree=null;
        object trans_tree=null;
        object char_literal157_tree=null;
        object char_literal158_tree=null;
        object char_literal159_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:429:2: (mx1= ID '(' ':' ',' id1= ID curl= ')' (trans= '\\'' )* )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:429:4: mx1= ID '(' ':' ',' id1= ID curl= ')' (trans= '\\'' )*
            {
            	root_0 = (object)adaptor.GetNilNode();

            	mx1=(IToken)Match(input,ID,FOLLOW_ID_in_vectorAsFunction21534); 
            		mx1_tree = (object)adaptor.Create(mx1);
            		adaptor.AddChild(root_0, mx1_tree);

            	char_literal157=(IToken)Match(input,22,FOLLOW_22_in_vectorAsFunction21536); 
            		char_literal157_tree = (object)adaptor.Create(char_literal157);
            		adaptor.AddChild(root_0, char_literal157_tree);

            	char_literal158=(IToken)Match(input,34,FOLLOW_34_in_vectorAsFunction21538); 
            		char_literal158_tree = (object)adaptor.Create(char_literal158);
            		adaptor.AddChild(root_0, char_literal158_tree);

            	char_literal159=(IToken)Match(input,24,FOLLOW_24_in_vectorAsFunction21540); 
            		char_literal159_tree = (object)adaptor.Create(char_literal159);
            		adaptor.AddChild(root_0, char_literal159_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_vectorAsFunction21544); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	curl=(IToken)Match(input,23,FOLLOW_23_in_vectorAsFunction21548); 
            		curl_tree = (object)adaptor.Create(curl);
            		adaptor.AddChild(root_0, curl_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:429:44: (trans= '\\'' )*
            	do 
            	{
            	    int alt18 = 2;
            	    int LA18_0 = input.LA(1);

            	    if ( (LA18_0 == 44) )
            	    {
            	        alt18 = 1;
            	    }


            	    switch (alt18) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:429:44: trans= '\\''
            			    {
            			    	trans=(IToken)Match(input,44,FOLLOW_44_in_vectorAsFunction21552); 
            			    		trans_tree = (object)adaptor.Create(trans);
            			    		adaptor.AddChild(root_0, trans_tree);


            			    }
            			    break;

            			default:
            			    goto loop18;
            	    }
            	} while (true);

            	loop18:
            		;	// Stops C# compiler whining that label 'loop18' has no statements


            			
            			if(trans != null)
            			{
            				tokens.Delete(trans);
            				tokens.Replace(mx1, curl, "Matrix.Transpose(MatrixSupport.GetMatrixByColumn((Vector)" + id1.Text + ", " + mx1.Text + "))");
            			}
            			else
            			{
            				tokens.Replace(mx1, curl, "MatrixSupport.GetMatrixByColumn((Vector)" + id1.Text + ", " + mx1.Text + ")");
            			}
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "vectorAsFunction2"

    public class mulDistance_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "mulDistance"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:444:1: mulDistance : func= 'DistMatrix' '(' id1= ID ',' id2= ID ')' ;
    public MatlabParser.mulDistance_return mulDistance() // throws RecognitionException [1]
    {   
        MatlabParser.mulDistance_return retval = new MatlabParser.mulDistance_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken id1 = null;
        IToken id2 = null;
        IToken char_literal160 = null;
        IToken char_literal161 = null;
        IToken char_literal162 = null;

        object func_tree=null;
        object id1_tree=null;
        object id2_tree=null;
        object char_literal160_tree=null;
        object char_literal161_tree=null;
        object char_literal162_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:445:2: (func= 'DistMatrix' '(' id1= ID ',' id2= ID ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:445:4: func= 'DistMatrix' '(' id1= ID ',' id2= ID ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,53,FOLLOW_53_in_mulDistance1569); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	char_literal160=(IToken)Match(input,22,FOLLOW_22_in_mulDistance1571); 
            		char_literal160_tree = (object)adaptor.Create(char_literal160);
            		adaptor.AddChild(root_0, char_literal160_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_mulDistance1575); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal161=(IToken)Match(input,24,FOLLOW_24_in_mulDistance1577); 
            		char_literal161_tree = (object)adaptor.Create(char_literal161);
            		adaptor.AddChild(root_0, char_literal161_tree);

            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_mulDistance1581); 
            		id2_tree = (object)adaptor.Create(id2);
            		adaptor.AddChild(root_0, id2_tree);

            	char_literal162=(IToken)Match(input,23,FOLLOW_23_in_mulDistance1583); 
            		char_literal162_tree = (object)adaptor.Create(char_literal162);
            		adaptor.AddChild(root_0, char_literal162_tree);


            			tokens.Replace(func, "MatrixSupport.GetDistanceMatrix");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "mulDistance"

    public class zeros_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "zeros"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:451:1: zeros : first= 'zeros(' id1= value ',' id2= expression last= ')' ;
    public MatlabParser.zeros_return zeros() // throws RecognitionException [1]
    {   
        MatlabParser.zeros_return retval = new MatlabParser.zeros_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken last = null;
        IToken char_literal163 = null;
        MatlabParser.value_return id1 = default(MatlabParser.value_return);

        MatlabParser.expression_return id2 = default(MatlabParser.expression_return);


        object first_tree=null;
        object last_tree=null;
        object char_literal163_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:451:7: (first= 'zeros(' id1= value ',' id2= expression last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:451:9: first= 'zeros(' id1= value ',' id2= expression last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,54,FOLLOW_54_in_zeros1598); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	PushFollow(FOLLOW_value_in_zeros1602);
            	id1 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, id1.Tree);
            	char_literal163=(IToken)Match(input,24,FOLLOW_24_in_zeros1604); 
            		char_literal163_tree = (object)adaptor.Create(char_literal163);
            		adaptor.AddChild(root_0, char_literal163_tree);

            	PushFollow(FOLLOW_expression_in_zeros1608);
            	id2 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, id2.Tree);
            	last=(IToken)Match(input,23,FOLLOW_23_in_zeros1612); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);

            		
            			tokens.Replace(first, last, "new Matrix((int)"
            			+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
            			+ ", (int)"
            			+ tokens.ToString( ((IToken)id2.Start).TokenIndex, ((IToken)id2.Stop).TokenIndex )
            			+ " )");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "zeros"

    public class index_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "index"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:461:1: index : ID first= '(' expression last= ')' ;
    public MatlabParser.index_return index() // throws RecognitionException [1]
    {   
        MatlabParser.index_return retval = new MatlabParser.index_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken last = null;
        IToken ID164 = null;
        MatlabParser.expression_return expression165 = default(MatlabParser.expression_return);


        object first_tree=null;
        object last_tree=null;
        object ID164_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:461:7: ( ID first= '(' expression last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:461:9: ID first= '(' expression last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	ID164=(IToken)Match(input,ID,FOLLOW_ID_in_index1625); 
            		ID164_tree = (object)adaptor.Create(ID164);
            		adaptor.AddChild(root_0, ID164_tree);

            	first=(IToken)Match(input,22,FOLLOW_22_in_index1629); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	PushFollow(FOLLOW_expression_in_index1631);
            	expression165 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression165.Tree);
            	last=(IToken)Match(input,23,FOLLOW_23_in_index1635); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Replace(first, "[");
            			tokens.Replace(last, ", 0]");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "index"

    public class size1_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "size1"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:468:1: size1 : first= 'size' '(' id1= ID ',' id2= ( ID | INT ) last= ')' ;
    public MatlabParser.size1_return size1() // throws RecognitionException [1]
    {   
        MatlabParser.size1_return retval = new MatlabParser.size1_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken id1 = null;
        IToken id2 = null;
        IToken last = null;
        IToken char_literal166 = null;
        IToken char_literal167 = null;

        object first_tree=null;
        object id1_tree=null;
        object id2_tree=null;
        object last_tree=null;
        object char_literal166_tree=null;
        object char_literal167_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:468:7: (first= 'size' '(' id1= ID ',' id2= ( ID | INT ) last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:468:9: first= 'size' '(' id1= ID ',' id2= ( ID | INT ) last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,55,FOLLOW_55_in_size11651); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	char_literal166=(IToken)Match(input,22,FOLLOW_22_in_size11653); 
            		char_literal166_tree = (object)adaptor.Create(char_literal166);
            		adaptor.AddChild(root_0, char_literal166_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_size11657); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal167=(IToken)Match(input,24,FOLLOW_24_in_size11659); 
            		char_literal167_tree = (object)adaptor.Create(char_literal167);
            		adaptor.AddChild(root_0, char_literal167_tree);

            	id2 = (IToken)input.LT(1);
            	if ( input.LA(1) == ID || input.LA(1) == INT ) 
            	{
            	    input.Consume();
            	    adaptor.AddChild(root_0, (object)adaptor.Create(id2));
            	    state.errorRecovery = false;
            	}
            	else 
            	{
            	    MismatchedSetException mse = new MismatchedSetException(null,input);
            	    throw mse;
            	}

            	last=(IToken)Match(input,23,FOLLOW_23_in_size11673); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Replace(first, last, "MatrixSupport.Size(" + id1.Text + ", " + id2.Text + ")");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "size1"

    public class randPerm_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "randPerm"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:474:1: randPerm : st= 'randperm(' arg= value fn= ')' ;
    public MatlabParser.randPerm_return randPerm() // throws RecognitionException [1]
    {   
        MatlabParser.randPerm_return retval = new MatlabParser.randPerm_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken st = null;
        IToken fn = null;
        MatlabParser.value_return arg = default(MatlabParser.value_return);


        object st_tree=null;
        object fn_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:475:2: (st= 'randperm(' arg= value fn= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:475:4: st= 'randperm(' arg= value fn= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	st=(IToken)Match(input,56,FOLLOW_56_in_randPerm1689); 
            		st_tree = (object)adaptor.Create(st);
            		adaptor.AddChild(root_0, st_tree);

            	PushFollow(FOLLOW_value_in_randPerm1693);
            	arg = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, arg.Tree);
            	fn=(IToken)Match(input,23,FOLLOW_23_in_randPerm1697); 
            		fn_tree = (object)adaptor.Create(fn);
            		adaptor.AddChild(root_0, fn_tree);


            			//tokens.Replace(st, "");
            			
            			//tokens.Replace(fn, "");
            			tokens.Replace(st, "RandomSupport.RandomPermutation((int)");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "randPerm"

    public class assignColumn_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "assignColumn"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:484:1: assignColumn : mx1= ID '(' id1= value ',' ':' ')' '=' mx2= ID '(' id2= value ',' ':' last= ')' ;
    public MatlabParser.assignColumn_return assignColumn() // throws RecognitionException [1]
    {   
        MatlabParser.assignColumn_return retval = new MatlabParser.assignColumn_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken mx1 = null;
        IToken mx2 = null;
        IToken last = null;
        IToken char_literal168 = null;
        IToken char_literal169 = null;
        IToken char_literal170 = null;
        IToken char_literal171 = null;
        IToken char_literal172 = null;
        IToken char_literal173 = null;
        IToken char_literal174 = null;
        IToken char_literal175 = null;
        MatlabParser.value_return id1 = default(MatlabParser.value_return);

        MatlabParser.value_return id2 = default(MatlabParser.value_return);


        object mx1_tree=null;
        object mx2_tree=null;
        object last_tree=null;
        object char_literal168_tree=null;
        object char_literal169_tree=null;
        object char_literal170_tree=null;
        object char_literal171_tree=null;
        object char_literal172_tree=null;
        object char_literal173_tree=null;
        object char_literal174_tree=null;
        object char_literal175_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:485:2: (mx1= ID '(' id1= value ',' ':' ')' '=' mx2= ID '(' id2= value ',' ':' last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:485:4: mx1= ID '(' id1= value ',' ':' ')' '=' mx2= ID '(' id2= value ',' ':' last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	mx1=(IToken)Match(input,ID,FOLLOW_ID_in_assignColumn1714); 
            		mx1_tree = (object)adaptor.Create(mx1);
            		adaptor.AddChild(root_0, mx1_tree);

            	char_literal168=(IToken)Match(input,22,FOLLOW_22_in_assignColumn1716); 
            		char_literal168_tree = (object)adaptor.Create(char_literal168);
            		adaptor.AddChild(root_0, char_literal168_tree);

            	PushFollow(FOLLOW_value_in_assignColumn1720);
            	id1 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, id1.Tree);
            	char_literal169=(IToken)Match(input,24,FOLLOW_24_in_assignColumn1722); 
            		char_literal169_tree = (object)adaptor.Create(char_literal169);
            		adaptor.AddChild(root_0, char_literal169_tree);

            	char_literal170=(IToken)Match(input,34,FOLLOW_34_in_assignColumn1724); 
            		char_literal170_tree = (object)adaptor.Create(char_literal170);
            		adaptor.AddChild(root_0, char_literal170_tree);

            	char_literal171=(IToken)Match(input,23,FOLLOW_23_in_assignColumn1726); 
            		char_literal171_tree = (object)adaptor.Create(char_literal171);
            		adaptor.AddChild(root_0, char_literal171_tree);

            	char_literal172=(IToken)Match(input,21,FOLLOW_21_in_assignColumn1728); 
            		char_literal172_tree = (object)adaptor.Create(char_literal172);
            		adaptor.AddChild(root_0, char_literal172_tree);

            	mx2=(IToken)Match(input,ID,FOLLOW_ID_in_assignColumn1733); 
            		mx2_tree = (object)adaptor.Create(mx2);
            		adaptor.AddChild(root_0, mx2_tree);

            	char_literal173=(IToken)Match(input,22,FOLLOW_22_in_assignColumn1735); 
            		char_literal173_tree = (object)adaptor.Create(char_literal173);
            		adaptor.AddChild(root_0, char_literal173_tree);

            	PushFollow(FOLLOW_value_in_assignColumn1739);
            	id2 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, id2.Tree);
            	char_literal174=(IToken)Match(input,24,FOLLOW_24_in_assignColumn1741); 
            		char_literal174_tree = (object)adaptor.Create(char_literal174);
            		adaptor.AddChild(root_0, char_literal174_tree);

            	char_literal175=(IToken)Match(input,34,FOLLOW_34_in_assignColumn1743); 
            		char_literal175_tree = (object)adaptor.Create(char_literal175);
            		adaptor.AddChild(root_0, char_literal175_tree);

            	last=(IToken)Match(input,23,FOLLOW_23_in_assignColumn1747); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            			tokens.Delete(mx1, last);
            			tokens.InsertAfter(last, "Matrix" + mx1.Text + ".SetColumnVector(" + mx2.Text + ".GetColumnVector((int)" 
            			+ tokens.ToString( ((IToken)id2.Start).TokenIndex, ((IToken)id2.Stop).TokenIndex )
            			+ "), "
            			+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
            			+ ");");
            			
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "assignColumn"

    public class value_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "value"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:1: value : ( ( '-' )* ID | function | INT | constant | ( '-' )* DOUBLE ) ;
    public MatlabParser.value_return value() // throws RecognitionException [1]
    {   
        MatlabParser.value_return retval = new MatlabParser.value_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken char_literal176 = null;
        IToken ID177 = null;
        IToken INT179 = null;
        IToken char_literal181 = null;
        IToken DOUBLE182 = null;
        MatlabParser.function_return function178 = default(MatlabParser.function_return);

        MatlabParser.constant_return constant180 = default(MatlabParser.constant_return);


        object char_literal176_tree=null;
        object ID177_tree=null;
        object INT179_tree=null;
        object char_literal181_tree=null;
        object DOUBLE182_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:7: ( ( ( '-' )* ID | function | INT | constant | ( '-' )* DOUBLE ) )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:10: ( ( '-' )* ID | function | INT | constant | ( '-' )* DOUBLE )
            {
            	root_0 = (object)adaptor.GetNilNode();

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:10: ( ( '-' )* ID | function | INT | constant | ( '-' )* DOUBLE )
            	int alt21 = 5;
            	alt21 = dfa21.Predict(input);
            	switch (alt21) 
            	{
            	    case 1 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:11: ( '-' )* ID
            	        {
            	        	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:11: ( '-' )*
            	        	do 
            	        	{
            	        	    int alt19 = 2;
            	        	    int LA19_0 = input.LA(1);

            	        	    if ( (LA19_0 == 26) )
            	        	    {
            	        	        alt19 = 1;
            	        	    }


            	        	    switch (alt19) 
            	        		{
            	        			case 1 :
            	        			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:12: '-'
            	        			    {
            	        			    	char_literal176=(IToken)Match(input,26,FOLLOW_26_in_value1765); 
            	        			    		char_literal176_tree = (object)adaptor.Create(char_literal176);
            	        			    		adaptor.AddChild(root_0, char_literal176_tree);


            	        			    }
            	        			    break;

            	        			default:
            	        			    goto loop19;
            	        	    }
            	        	} while (true);

            	        	loop19:
            	        		;	// Stops C# compiler whining that label 'loop19' has no statements

            	        	ID177=(IToken)Match(input,ID,FOLLOW_ID_in_value1769); 
            	        		ID177_tree = (object)adaptor.Create(ID177);
            	        		adaptor.AddChild(root_0, ID177_tree);


            	        }
            	        break;
            	    case 2 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:23: function
            	        {
            	        	PushFollow(FOLLOW_function_in_value1773);
            	        	function178 = function();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, function178.Tree);

            	        }
            	        break;
            	    case 3 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:34: INT
            	        {
            	        	INT179=(IToken)Match(input,INT,FOLLOW_INT_in_value1777); 
            	        		INT179_tree = (object)adaptor.Create(INT179);
            	        		adaptor.AddChild(root_0, INT179_tree);


            	        }
            	        break;
            	    case 4 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:40: constant
            	        {
            	        	PushFollow(FOLLOW_constant_in_value1781);
            	        	constant180 = constant();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, constant180.Tree);

            	        }
            	        break;
            	    case 5 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:51: ( '-' )* DOUBLE
            	        {
            	        	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:51: ( '-' )*
            	        	do 
            	        	{
            	        	    int alt20 = 2;
            	        	    int LA20_0 = input.LA(1);

            	        	    if ( (LA20_0 == 26) )
            	        	    {
            	        	        alt20 = 1;
            	        	    }


            	        	    switch (alt20) 
            	        		{
            	        			case 1 :
            	        			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:497:52: '-'
            	        			    {
            	        			    	char_literal181=(IToken)Match(input,26,FOLLOW_26_in_value1786); 
            	        			    		char_literal181_tree = (object)adaptor.Create(char_literal181);
            	        			    		adaptor.AddChild(root_0, char_literal181_tree);


            	        			    }
            	        			    break;

            	        			default:
            	        			    goto loop20;
            	        	    }
            	        	} while (true);

            	        	loop20:
            	        		;	// Stops C# compiler whining that label 'loop20' has no statements

            	        	DOUBLE182=(IToken)Match(input,DOUBLE,FOLLOW_DOUBLE_in_value1790); 
            	        		DOUBLE182_tree = (object)adaptor.Create(DOUBLE182);
            	        		adaptor.AddChild(root_0, DOUBLE182_tree);


            	        }
            	        break;

            	}


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "value"

    public class structuredStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "structuredStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:500:1: structuredStatement : ( ifStatement | repetetiveStatement | 'else' ) compoundStatement ;
    public MatlabParser.structuredStatement_return structuredStatement() // throws RecognitionException [1]
    {   
        MatlabParser.structuredStatement_return retval = new MatlabParser.structuredStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken string_literal185 = null;
        MatlabParser.ifStatement_return ifStatement183 = default(MatlabParser.ifStatement_return);

        MatlabParser.repetetiveStatement_return repetetiveStatement184 = default(MatlabParser.repetetiveStatement_return);

        MatlabParser.compoundStatement_return compoundStatement186 = default(MatlabParser.compoundStatement_return);


        object string_literal185_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:501:5: ( ( ifStatement | repetetiveStatement | 'else' ) compoundStatement )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:501:7: ( ifStatement | repetetiveStatement | 'else' ) compoundStatement
            {
            	root_0 = (object)adaptor.GetNilNode();

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:501:7: ( ifStatement | repetetiveStatement | 'else' )
            	int alt22 = 3;
            	switch ( input.LA(1) ) 
            	{
            	case IF:
            		{
            	    alt22 = 1;
            	    }
            	    break;
            	case WHILE:
            	case FOR:
            		{
            	    alt22 = 2;
            	    }
            	    break;
            	case ELSE:
            		{
            	    alt22 = 3;
            	    }
            	    break;
            		default:
            		    NoViableAltException nvae_d22s0 =
            		        new NoViableAltException("", 22, 0, input);

            		    throw nvae_d22s0;
            	}

            	switch (alt22) 
            	{
            	    case 1 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:501:8: ifStatement
            	        {
            	        	PushFollow(FOLLOW_ifStatement_in_structuredStatement1806);
            	        	ifStatement183 = ifStatement();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, ifStatement183.Tree);

            	        }
            	        break;
            	    case 2 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:501:22: repetetiveStatement
            	        {
            	        	PushFollow(FOLLOW_repetetiveStatement_in_structuredStatement1810);
            	        	repetetiveStatement184 = repetetiveStatement();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, repetetiveStatement184.Tree);

            	        }
            	        break;
            	    case 3 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:501:44: 'else'
            	        {
            	        	string_literal185=(IToken)Match(input,ELSE,FOLLOW_ELSE_in_structuredStatement1814); 
            	        		string_literal185_tree = (object)adaptor.Create(string_literal185);
            	        		adaptor.AddChild(root_0, string_literal185_tree);


            	        }
            	        break;

            	}

            	PushFollow(FOLLOW_compoundStatement_in_structuredStatement1817);
            	compoundStatement186 = compoundStatement();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, compoundStatement186.Tree);

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "structuredStatement"

    public class compoundStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "compoundStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:504:1: compoundStatement : st= statements cur2= END ;
    public MatlabParser.compoundStatement_return compoundStatement() // throws RecognitionException [1]
    {   
        MatlabParser.compoundStatement_return retval = new MatlabParser.compoundStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken cur2 = null;
        MatlabParser.statements_return st = default(MatlabParser.statements_return);


        object cur2_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:505:5: (st= statements cur2= END )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:506:5: st= statements cur2= END
            {
            	root_0 = (object)adaptor.GetNilNode();

            	PushFollow(FOLLOW_statements_in_compoundStatement1842);
            	st = statements();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, st.Tree);
            	cur2=(IToken)Match(input,END,FOLLOW_END_in_compoundStatement1850); 

            	    	tokens.InsertBefore(((IToken)st.Start).TokenIndex, "{\n");
            	      	tokens.Replace(cur2, '}');
            	    

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "compoundStatement"

    public class statements_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "statements"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:514:1: statements : ( statement )+ ;
    public MatlabParser.statements_return statements() // throws RecognitionException [1]
    {   
        MatlabParser.statements_return retval = new MatlabParser.statements_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.statement_return statement187 = default(MatlabParser.statement_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:515:5: ( ( statement )+ )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:515:7: ( statement )+
            {
            	root_0 = (object)adaptor.GetNilNode();

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:515:7: ( statement )+
            	int cnt23 = 0;
            	do 
            	{
            	    int alt23 = 2;
            	    int LA23_0 = input.LA(1);

            	    if ( (LA23_0 == ID || LA23_0 == IF || (LA23_0 >= ID3D && LA23_0 <= FOR) || LA23_0 == ELSE || LA23_0 == 30 || LA23_0 == 38 || LA23_0 == 57) )
            	    {
            	        alt23 = 1;
            	    }


            	    switch (alt23) 
            		{
            			case 1 :
            			    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:515:7: statement
            			    {
            			    	PushFollow(FOLLOW_statement_in_statements1874);
            			    	statement187 = statement();
            			    	state.followingStackPointer--;

            			    	adaptor.AddChild(root_0, statement187.Tree);

            			    }
            			    break;

            			default:
            			    if ( cnt23 >= 1 ) goto loop23;
            		            EarlyExitException eee23 =
            		                new EarlyExitException(23, input);
            		            throw eee23;
            	    }
            	    cnt23++;
            	} while (true);

            	loop23:
            		;	// Stops C# compiler whinging that label 'loop23' has no statements


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "statements"

    public class repetetiveStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "repetetiveStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:518:1: repetetiveStatement : ( whileStatement | forStatement );
    public MatlabParser.repetetiveStatement_return repetetiveStatement() // throws RecognitionException [1]
    {   
        MatlabParser.repetetiveStatement_return retval = new MatlabParser.repetetiveStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.whileStatement_return whileStatement188 = default(MatlabParser.whileStatement_return);

        MatlabParser.forStatement_return forStatement189 = default(MatlabParser.forStatement_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:519:5: ( whileStatement | forStatement )
            int alt24 = 2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0 == WHILE) )
            {
                alt24 = 1;
            }
            else if ( (LA24_0 == FOR) )
            {
                alt24 = 2;
            }
            else 
            {
                NoViableAltException nvae_d24s0 =
                    new NoViableAltException("", 24, 0, input);

                throw nvae_d24s0;
            }
            switch (alt24) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:519:7: whileStatement
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_whileStatement_in_repetetiveStatement1892);
                    	whileStatement188 = whileStatement();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, whileStatement188.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:520:7: forStatement
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_forStatement_in_repetetiveStatement1900);
                    	forStatement189 = forStatement();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, forStatement189.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "repetetiveStatement"

    public class assign_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "assign"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:1: assign : (num= ID eq= '=' ( expression | power ) semi= ';' | 'break;' ) ;
    public MatlabParser.assign_return assign() // throws RecognitionException [1]
    {   
        MatlabParser.assign_return retval = new MatlabParser.assign_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken num = null;
        IToken eq = null;
        IToken semi = null;
        IToken string_literal192 = null;
        MatlabParser.expression_return expression190 = default(MatlabParser.expression_return);

        MatlabParser.power_return power191 = default(MatlabParser.power_return);


        object num_tree=null;
        object eq_tree=null;
        object semi_tree=null;
        object string_literal192_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:7: ( (num= ID eq= '=' ( expression | power ) semi= ';' | 'break;' ) )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:11: (num= ID eq= '=' ( expression | power ) semi= ';' | 'break;' )
            {
            	root_0 = (object)adaptor.GetNilNode();

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:11: (num= ID eq= '=' ( expression | power ) semi= ';' | 'break;' )
            	int alt26 = 2;
            	int LA26_0 = input.LA(1);

            	if ( (LA26_0 == ID) )
            	{
            	    alt26 = 1;
            	}
            	else if ( (LA26_0 == 57) )
            	{
            	    alt26 = 2;
            	}
            	else 
            	{
            	    NoViableAltException nvae_d26s0 =
            	        new NoViableAltException("", 26, 0, input);

            	    throw nvae_d26s0;
            	}
            	switch (alt26) 
            	{
            	    case 1 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:12: num= ID eq= '=' ( expression | power ) semi= ';'
            	        {
            	        	num=(IToken)Match(input,ID,FOLLOW_ID_in_assign1917); 
            	        		num_tree = (object)adaptor.Create(num);
            	        		adaptor.AddChild(root_0, num_tree);

            	        	eq=(IToken)Match(input,21,FOLLOW_21_in_assign1921); 
            	        		eq_tree = (object)adaptor.Create(eq);
            	        		adaptor.AddChild(root_0, eq_tree);

            	        	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:26: ( expression | power )
            	        	int alt25 = 2;
            	        	int LA25_0 = input.LA(1);

            	        	if ( ((LA25_0 >= INT && LA25_0 <= DOUBLE) || LA25_0 == 26 || LA25_0 == 30 || LA25_0 == 35 || (LA25_0 >= 39 && LA25_0 <= 43) || (LA25_0 >= 45 && LA25_0 <= 56) || (LA25_0 >= 58 && LA25_0 <= 59)) )
            	        	{
            	        	    alt25 = 1;
            	        	}
            	        	else if ( (LA25_0 == ID) )
            	        	{
            	        	    int LA25_2 = input.LA(2);

            	        	    if ( (LA25_2 == SEMI || LA25_2 == 22 || (LA25_2 >= 25 && LA25_2 <= 26) || (LA25_2 >= 28 && LA25_2 <= 29) || LA25_2 == 44) )
            	        	    {
            	        	        alt25 = 1;
            	        	    }
            	        	    else if ( (LA25_2 == 27) )
            	        	    {
            	        	        alt25 = 2;
            	        	    }
            	        	    else 
            	        	    {
            	        	        NoViableAltException nvae_d25s2 =
            	        	            new NoViableAltException("", 25, 2, input);

            	        	        throw nvae_d25s2;
            	        	    }
            	        	}
            	        	else 
            	        	{
            	        	    NoViableAltException nvae_d25s0 =
            	        	        new NoViableAltException("", 25, 0, input);

            	        	    throw nvae_d25s0;
            	        	}
            	        	switch (alt25) 
            	        	{
            	        	    case 1 :
            	        	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:27: expression
            	        	        {
            	        	        	PushFollow(FOLLOW_expression_in_assign1924);
            	        	        	expression190 = expression();
            	        	        	state.followingStackPointer--;

            	        	        	adaptor.AddChild(root_0, expression190.Tree);

            	        	        }
            	        	        break;
            	        	    case 2 :
            	        	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:38: power
            	        	        {
            	        	        	PushFollow(FOLLOW_power_in_assign1926);
            	        	        	power191 = power();
            	        	        	state.followingStackPointer--;

            	        	        	adaptor.AddChild(root_0, power191.Tree);

            	        	        }
            	        	        break;

            	        	}

            	        	semi=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_assign1931); 
            	        		semi_tree = (object)adaptor.Create(semi);
            	        		adaptor.AddChild(root_0, semi_tree);


            	        }
            	        break;
            	    case 2 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:523:56: 'break;'
            	        {
            	        	string_literal192=(IToken)Match(input,57,FOLLOW_57_in_assign1935); 
            	        		string_literal192_tree = (object)adaptor.Create(string_literal192);
            	        		adaptor.AddChild(root_0, string_literal192_tree);


            	        }
            	        break;

            	}


            			if (num != null)
            			{
            				if (!identifiers.Contains(num.Text))
            				{
            					identifiers.Add(num.Text);
            				}
            				tokens.InsertAfter(eq, " (Matrix)(");
            				tokens.Replace(semi, ");");
            			}
            			
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "assign"

    public class statement3d_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "statement3d"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:538:1: statement3d : ( assign3dtomatrix | assignmatrixto3d | init3d );
    public MatlabParser.statement3d_return statement3d() // throws RecognitionException [1]
    {   
        MatlabParser.statement3d_return retval = new MatlabParser.statement3d_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.assign3dtomatrix_return assign3dtomatrix193 = default(MatlabParser.assign3dtomatrix_return);

        MatlabParser.assignmatrixto3d_return assignmatrixto3d194 = default(MatlabParser.assignmatrixto3d_return);

        MatlabParser.init3d_return init3d195 = default(MatlabParser.init3d_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:539:2: ( assign3dtomatrix | assignmatrixto3d | init3d )
            int alt27 = 3;
            int LA27_0 = input.LA(1);

            if ( (LA27_0 == ID3D) )
            {
                int LA27_1 = input.LA(2);

                if ( (LA27_1 == 22) )
                {
                    alt27 = 1;
                }
                else if ( (LA27_1 == 21) )
                {
                    alt27 = 3;
                }
                else 
                {
                    NoViableAltException nvae_d27s1 =
                        new NoViableAltException("", 27, 1, input);

                    throw nvae_d27s1;
                }
            }
            else if ( (LA27_0 == ID) )
            {
                alt27 = 2;
            }
            else 
            {
                NoViableAltException nvae_d27s0 =
                    new NoViableAltException("", 27, 0, input);

                throw nvae_d27s0;
            }
            switch (alt27) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:540:2: assign3dtomatrix
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_assign3dtomatrix_in_statement3d1955);
                    	assign3dtomatrix193 = assign3dtomatrix();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, assign3dtomatrix193.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:541:2: assignmatrixto3d
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_assignmatrixto3d_in_statement3d1959);
                    	assignmatrixto3d194 = assignmatrixto3d();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, assignmatrixto3d194.Tree);

                    }
                    break;
                case 3 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:542:2: init3d
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_init3d_in_statement3d1963);
                    	init3d195 = init3d();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, init3d195.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "statement3d"

    public class assign3dtomatrix_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "assign3dtomatrix"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:545:1: assign3dtomatrix : matrix3d '=' ID ';' ;
    public MatlabParser.assign3dtomatrix_return assign3dtomatrix() // throws RecognitionException [1]
    {   
        MatlabParser.assign3dtomatrix_return retval = new MatlabParser.assign3dtomatrix_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken char_literal197 = null;
        IToken ID198 = null;
        IToken char_literal199 = null;
        MatlabParser.matrix3d_return matrix3d196 = default(MatlabParser.matrix3d_return);


        object char_literal197_tree=null;
        object ID198_tree=null;
        object char_literal199_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:546:2: ( matrix3d '=' ID ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:546:4: matrix3d '=' ID ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	PushFollow(FOLLOW_matrix3d_in_assign3dtomatrix1975);
            	matrix3d196 = matrix3d();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, matrix3d196.Tree);
            	char_literal197=(IToken)Match(input,21,FOLLOW_21_in_assign3dtomatrix1977); 
            		char_literal197_tree = (object)adaptor.Create(char_literal197);
            		adaptor.AddChild(root_0, char_literal197_tree);

            	ID198=(IToken)Match(input,ID,FOLLOW_ID_in_assign3dtomatrix1979); 
            		ID198_tree = (object)adaptor.Create(ID198);
            		adaptor.AddChild(root_0, ID198_tree);

            	char_literal199=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_assign3dtomatrix1981); 
            		char_literal199_tree = (object)adaptor.Create(char_literal199);
            		adaptor.AddChild(root_0, char_literal199_tree);


            		
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "assign3dtomatrix"

    public class assignmatrixto3d_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "assignmatrixto3d"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:552:1: assignmatrixto3d : var= ID '=' matrix3d ';' ;
    public MatlabParser.assignmatrixto3d_return assignmatrixto3d() // throws RecognitionException [1]
    {   
        MatlabParser.assignmatrixto3d_return retval = new MatlabParser.assignmatrixto3d_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken var = null;
        IToken char_literal200 = null;
        IToken char_literal202 = null;
        MatlabParser.matrix3d_return matrix3d201 = default(MatlabParser.matrix3d_return);


        object var_tree=null;
        object char_literal200_tree=null;
        object char_literal202_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:553:2: (var= ID '=' matrix3d ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:553:4: var= ID '=' matrix3d ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	var=(IToken)Match(input,ID,FOLLOW_ID_in_assignmatrixto3d1998); 
            		var_tree = (object)adaptor.Create(var);
            		adaptor.AddChild(root_0, var_tree);

            	char_literal200=(IToken)Match(input,21,FOLLOW_21_in_assignmatrixto3d2000); 
            		char_literal200_tree = (object)adaptor.Create(char_literal200);
            		adaptor.AddChild(root_0, char_literal200_tree);

            	PushFollow(FOLLOW_matrix3d_in_assignmatrixto3d2002);
            	matrix3d201 = matrix3d();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, matrix3d201.Tree);
            	char_literal202=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_assignmatrixto3d2004); 
            		char_literal202_tree = (object)adaptor.Create(char_literal202);
            		adaptor.AddChild(root_0, char_literal202_tree);


            			identifiers.Add(var.Text);
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "assignmatrixto3d"

    public class init3d_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "init3d"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:559:1: init3d : id= ID3D '=' function3d ';' ;
    public MatlabParser.init3d_return init3d() // throws RecognitionException [1]
    {   
        MatlabParser.init3d_return retval = new MatlabParser.init3d_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken id = null;
        IToken char_literal203 = null;
        IToken char_literal205 = null;
        MatlabParser.function3d_return function3d204 = default(MatlabParser.function3d_return);


        object id_tree=null;
        object char_literal203_tree=null;
        object char_literal205_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:560:2: (id= ID3D '=' function3d ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:560:4: id= ID3D '=' function3d ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	id=(IToken)Match(input,ID3D,FOLLOW_ID3D_in_init3d2020); 
            		id_tree = (object)adaptor.Create(id);
            		adaptor.AddChild(root_0, id_tree);

            	char_literal203=(IToken)Match(input,21,FOLLOW_21_in_init3d2022); 
            		char_literal203_tree = (object)adaptor.Create(char_literal203);
            		adaptor.AddChild(root_0, char_literal203_tree);

            	PushFollow(FOLLOW_function3d_in_init3d2024);
            	function3d204 = function3d();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, function3d204.Tree);
            	char_literal205=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_init3d2026); 
            		char_literal205_tree = (object)adaptor.Create(char_literal205);
            		adaptor.AddChild(root_0, char_literal205_tree);


            				if (!identifiers3d.Contains(id.Text))
            				{
            					identifiers3d.Add(id.Text);
            				}
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "init3d"

    public class function3d_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "function3d"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:569:1: function3d : ( zeros3d | ones3d );
    public MatlabParser.function3d_return function3d() // throws RecognitionException [1]
    {   
        MatlabParser.function3d_return retval = new MatlabParser.function3d_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.zeros3d_return zeros3d206 = default(MatlabParser.zeros3d_return);

        MatlabParser.ones3d_return ones3d207 = default(MatlabParser.ones3d_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:570:2: ( zeros3d | ones3d )
            int alt28 = 2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0 == 54) )
            {
                alt28 = 1;
            }
            else if ( (LA28_0 == 45) )
            {
                alt28 = 2;
            }
            else 
            {
                NoViableAltException nvae_d28s0 =
                    new NoViableAltException("", 28, 0, input);

                throw nvae_d28s0;
            }
            switch (alt28) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:570:4: zeros3d
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_zeros3d_in_function3d2042);
                    	zeros3d206 = zeros3d();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, zeros3d206.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:570:14: ones3d
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_ones3d_in_function3d2046);
                    	ones3d207 = ones3d();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, ones3d207.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "function3d"

    public class whileStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "whileStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:574:1: whileStatement : WHILE condExpression com= ',' ;
    public MatlabParser.whileStatement_return whileStatement() // throws RecognitionException [1]
    {   
        MatlabParser.whileStatement_return retval = new MatlabParser.whileStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken com = null;
        IToken WHILE208 = null;
        MatlabParser.condExpression_return condExpression209 = default(MatlabParser.condExpression_return);


        object com_tree=null;
        object WHILE208_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:575:5: ( WHILE condExpression com= ',' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:575:7: WHILE condExpression com= ','
            {
            	root_0 = (object)adaptor.GetNilNode();

            	WHILE208=(IToken)Match(input,WHILE,FOLLOW_WHILE_in_whileStatement2065); 
            		WHILE208_tree = (object)adaptor.Create(WHILE208);
            		root_0 = (object)adaptor.BecomeRoot(WHILE208_tree, root_0);

            	PushFollow(FOLLOW_condExpression_in_whileStatement2068);
            	condExpression209 = condExpression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, condExpression209.Tree);
            	com=(IToken)Match(input,24,FOLLOW_24_in_whileStatement2072); 
            		com_tree = (object)adaptor.Create(com);
            		adaptor.AddChild(root_0, com_tree);


            	    		tokens.Replace(com, "");
            	    		tokens.InsertBefore(((condExpression209 != null) ? ((IToken)condExpression209.Start) : null), "(");
            	    		tokens.InsertAfter(((condExpression209 != null) ? ((IToken)condExpression209.Stop) : null), ")");
            	    

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "whileStatement"

    public class forStatement_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "forStatement"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:583:1: forStatement : start= FOR it= ID eq= '=' min= value dd= ':' atom ;
    public MatlabParser.forStatement_return forStatement() // throws RecognitionException [1]
    {   
        MatlabParser.forStatement_return retval = new MatlabParser.forStatement_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken start = null;
        IToken it = null;
        IToken eq = null;
        IToken dd = null;
        MatlabParser.value_return min = default(MatlabParser.value_return);

        MatlabParser.atom_return atom210 = default(MatlabParser.atom_return);


        object start_tree=null;
        object it_tree=null;
        object eq_tree=null;
        object dd_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:583:14: (start= FOR it= ID eq= '=' min= value dd= ':' atom )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:583:16: start= FOR it= ID eq= '=' min= value dd= ':' atom
            {
            	root_0 = (object)adaptor.GetNilNode();

            	start=(IToken)Match(input,FOR,FOLLOW_FOR_in_forStatement2094); 
            		start_tree = (object)adaptor.Create(start);
            		root_0 = (object)adaptor.BecomeRoot(start_tree, root_0);

            	it=(IToken)Match(input,ID,FOLLOW_ID_in_forStatement2099); 
            		it_tree = (object)adaptor.Create(it);
            		adaptor.AddChild(root_0, it_tree);

            	eq=(IToken)Match(input,21,FOLLOW_21_in_forStatement2103); 
            		eq_tree = (object)adaptor.Create(eq);
            		adaptor.AddChild(root_0, eq_tree);

            	PushFollow(FOLLOW_value_in_forStatement2107);
            	min = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, min.Tree);
            	dd=(IToken)Match(input,34,FOLLOW_34_in_forStatement2111); 
            		dd_tree = (object)adaptor.Create(dd);
            		adaptor.AddChild(root_0, dd_tree);

            	PushFollow(FOLLOW_atom_in_forStatement2113);
            	atom210 = atom();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, atom210.Tree);

            			tokens.Delete(it, dd);
            			tokens.Replace(((atom210 != null) ? ((IToken)atom210.Start) : null), ((atom210 != null) ? ((IToken)atom210.Stop) : null), "(" + "int "+ it.Text + " = " 
            			+ tokens.ToString( ((IToken)min.Start).TokenIndex, ((IToken)min.Stop).TokenIndex )
            			+ "- 1; " + it.Text + " < " + 
            			tokens.ToString( ((IToken)((atom210 != null) ? ((IToken)atom210.Start) : null)).TokenIndex, ((IToken)((atom210 != null) ? ((IToken)atom210.Stop) : null)).TokenIndex ) 
            			+ "; " + it.Text + "++)");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "forStatement"

    public class sizeEval_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "sizeEval"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:594:1: sizeEval : first= '[' id1= ID ',' id2= ID ']' '=' op= 'size' '(' mx= ID last= ')' ';' ;
    public MatlabParser.sizeEval_return sizeEval() // throws RecognitionException [1]
    {   
        MatlabParser.sizeEval_return retval = new MatlabParser.sizeEval_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken id1 = null;
        IToken id2 = null;
        IToken op = null;
        IToken mx = null;
        IToken last = null;
        IToken char_literal211 = null;
        IToken char_literal212 = null;
        IToken char_literal213 = null;
        IToken char_literal214 = null;
        IToken char_literal215 = null;

        object first_tree=null;
        object id1_tree=null;
        object id2_tree=null;
        object op_tree=null;
        object mx_tree=null;
        object last_tree=null;
        object char_literal211_tree=null;
        object char_literal212_tree=null;
        object char_literal213_tree=null;
        object char_literal214_tree=null;
        object char_literal215_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:595:2: (first= '[' id1= ID ',' id2= ID ']' '=' op= 'size' '(' mx= ID last= ')' ';' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:595:4: first= '[' id1= ID ',' id2= ID ']' '=' op= 'size' '(' mx= ID last= ')' ';'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,30,FOLLOW_30_in_sizeEval2129); 
            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_sizeEval2134); 
            	char_literal211=(IToken)Match(input,24,FOLLOW_24_in_sizeEval2137); 
            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_sizeEval2142); 
            	char_literal212=(IToken)Match(input,31,FOLLOW_31_in_sizeEval2145); 
            	char_literal213=(IToken)Match(input,21,FOLLOW_21_in_sizeEval2148); 
            	op=(IToken)Match(input,55,FOLLOW_55_in_sizeEval2153); 
            	char_literal214=(IToken)Match(input,22,FOLLOW_22_in_sizeEval2156); 
            	mx=(IToken)Match(input,ID,FOLLOW_ID_in_sizeEval2161); 
            	last=(IToken)Match(input,23,FOLLOW_23_in_sizeEval2166); 
            	char_literal215=(IToken)Match(input,SEMI,FOLLOW_SEMI_in_sizeEval2169); 
            		char_literal215_tree = (object)adaptor.Create(char_literal215);
            		adaptor.AddChild(root_0, char_literal215_tree);


            			tokens.Delete(first, last);
            			tokens.InsertAfter(last, "int " + id1.Text + " = " + mx.Text + ".RowCount;\n" + "int " + id2.Text + " = " + mx.Text + ".ColumnCount");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "sizeEval"

    public class matrix3d_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "matrix3d"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:602:1: matrix3d : first= ID3D br1= '(' ':' ',' ':' secondcomma= ',' expression last= ')' ;
    public MatlabParser.matrix3d_return matrix3d() // throws RecognitionException [1]
    {   
        MatlabParser.matrix3d_return retval = new MatlabParser.matrix3d_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken br1 = null;
        IToken secondcomma = null;
        IToken last = null;
        IToken char_literal216 = null;
        IToken char_literal217 = null;
        IToken char_literal218 = null;
        MatlabParser.expression_return expression219 = default(MatlabParser.expression_return);


        object first_tree=null;
        object br1_tree=null;
        object secondcomma_tree=null;
        object last_tree=null;
        object char_literal216_tree=null;
        object char_literal217_tree=null;
        object char_literal218_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:603:2: (first= ID3D br1= '(' ':' ',' ':' secondcomma= ',' expression last= ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:603:4: first= ID3D br1= '(' ':' ',' ':' secondcomma= ',' expression last= ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,ID3D,FOLLOW_ID3D_in_matrix3d2185); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	br1=(IToken)Match(input,22,FOLLOW_22_in_matrix3d2189); 
            		br1_tree = (object)adaptor.Create(br1);
            		adaptor.AddChild(root_0, br1_tree);

            	char_literal216=(IToken)Match(input,34,FOLLOW_34_in_matrix3d2191); 
            		char_literal216_tree = (object)adaptor.Create(char_literal216);
            		adaptor.AddChild(root_0, char_literal216_tree);

            	char_literal217=(IToken)Match(input,24,FOLLOW_24_in_matrix3d2193); 
            		char_literal217_tree = (object)adaptor.Create(char_literal217);
            		adaptor.AddChild(root_0, char_literal217_tree);

            	char_literal218=(IToken)Match(input,34,FOLLOW_34_in_matrix3d2195); 
            		char_literal218_tree = (object)adaptor.Create(char_literal218);
            		adaptor.AddChild(root_0, char_literal218_tree);

            	secondcomma=(IToken)Match(input,24,FOLLOW_24_in_matrix3d2199); 
            		secondcomma_tree = (object)adaptor.Create(secondcomma);
            		adaptor.AddChild(root_0, secondcomma_tree);

            	PushFollow(FOLLOW_expression_in_matrix3d2201);
            	expression219 = expression();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, expression219.Tree);
            	last=(IToken)Match(input,23,FOLLOW_23_in_matrix3d2205); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            				tokens.Replace(br1, secondcomma, "[");
            				tokens.Replace(last, "]");
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "matrix3d"

    public class zeros3d_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "zeros3d"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:610:1: zeros3d : func= 'zeros(' value first= ',' value second= ',' value ')' ;
    public MatlabParser.zeros3d_return zeros3d() // throws RecognitionException [1]
    {   
        MatlabParser.zeros3d_return retval = new MatlabParser.zeros3d_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken first = null;
        IToken second = null;
        IToken char_literal223 = null;
        MatlabParser.value_return value220 = default(MatlabParser.value_return);

        MatlabParser.value_return value221 = default(MatlabParser.value_return);

        MatlabParser.value_return value222 = default(MatlabParser.value_return);


        object func_tree=null;
        object first_tree=null;
        object second_tree=null;
        object char_literal223_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:610:9: (func= 'zeros(' value first= ',' value second= ',' value ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:610:11: func= 'zeros(' value first= ',' value second= ',' value ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,54,FOLLOW_54_in_zeros3d2221); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	PushFollow(FOLLOW_value_in_zeros3d2223);
            	value220 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value220.Tree);
            	first=(IToken)Match(input,24,FOLLOW_24_in_zeros3d2227); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	PushFollow(FOLLOW_value_in_zeros3d2229);
            	value221 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value221.Tree);
            	second=(IToken)Match(input,24,FOLLOW_24_in_zeros3d2233); 
            		second_tree = (object)adaptor.Create(second);
            		adaptor.AddChild(root_0, second_tree);

            	PushFollow(FOLLOW_value_in_zeros3d2235);
            	value222 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value222.Tree);
            	char_literal223=(IToken)Match(input,23,FOLLOW_23_in_zeros3d2237); 
            		char_literal223_tree = (object)adaptor.Create(char_literal223);
            		adaptor.AddChild(root_0, char_literal223_tree);


            				tokens.Replace(func, "new Matrix3d((int)");
            				tokens.Replace(first, ",(int) ");
            				tokens.Replace(second, ",(int) ");
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "zeros3d"

    public class ones3d_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "ones3d"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:618:1: ones3d : func= 'ones(' value first= ',' value second= ',' value ')' ;
    public MatlabParser.ones3d_return ones3d() // throws RecognitionException [1]
    {   
        MatlabParser.ones3d_return retval = new MatlabParser.ones3d_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken func = null;
        IToken first = null;
        IToken second = null;
        IToken char_literal227 = null;
        MatlabParser.value_return value224 = default(MatlabParser.value_return);

        MatlabParser.value_return value225 = default(MatlabParser.value_return);

        MatlabParser.value_return value226 = default(MatlabParser.value_return);


        object func_tree=null;
        object first_tree=null;
        object second_tree=null;
        object char_literal227_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:618:9: (func= 'ones(' value first= ',' value second= ',' value ')' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:618:11: func= 'ones(' value first= ',' value second= ',' value ')'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	func=(IToken)Match(input,45,FOLLOW_45_in_ones3d2255); 
            		func_tree = (object)adaptor.Create(func);
            		adaptor.AddChild(root_0, func_tree);

            	PushFollow(FOLLOW_value_in_ones3d2257);
            	value224 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value224.Tree);
            	first=(IToken)Match(input,24,FOLLOW_24_in_ones3d2261); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	PushFollow(FOLLOW_value_in_ones3d2263);
            	value225 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value225.Tree);
            	second=(IToken)Match(input,24,FOLLOW_24_in_ones3d2267); 
            		second_tree = (object)adaptor.Create(second);
            		adaptor.AddChild(root_0, second_tree);

            	PushFollow(FOLLOW_value_in_ones3d2269);
            	value226 = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, value226.Tree);
            	char_literal227=(IToken)Match(input,23,FOLLOW_23_in_ones3d2271); 
            		char_literal227_tree = (object)adaptor.Create(char_literal227);
            		adaptor.AddChild(root_0, char_literal227_tree);


            				tokens.Replace(func, "Matrix3d.Ones3d(");
            				tokens.Replace(first, ",(int) ");
            				tokens.Replace(second, ",(int) ");
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "ones3d"

    public class addColumn_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "addColumn"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:626:1: addColumn : first= '[' mx= ID ',' ( interval | vec= ID ) last= ']' ;
    public MatlabParser.addColumn_return addColumn() // throws RecognitionException [1]
    {   
        MatlabParser.addColumn_return retval = new MatlabParser.addColumn_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken first = null;
        IToken mx = null;
        IToken vec = null;
        IToken last = null;
        IToken char_literal228 = null;
        MatlabParser.interval_return interval229 = default(MatlabParser.interval_return);


        object first_tree=null;
        object mx_tree=null;
        object vec_tree=null;
        object last_tree=null;
        object char_literal228_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:627:2: (first= '[' mx= ID ',' ( interval | vec= ID ) last= ']' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:628:3: first= '[' mx= ID ',' ( interval | vec= ID ) last= ']'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	first=(IToken)Match(input,30,FOLLOW_30_in_addColumn2293); 
            		first_tree = (object)adaptor.Create(first);
            		adaptor.AddChild(root_0, first_tree);

            	mx=(IToken)Match(input,ID,FOLLOW_ID_in_addColumn2297); 
            		mx_tree = (object)adaptor.Create(mx);
            		adaptor.AddChild(root_0, mx_tree);

            	char_literal228=(IToken)Match(input,24,FOLLOW_24_in_addColumn2299); 
            		char_literal228_tree = (object)adaptor.Create(char_literal228);
            		adaptor.AddChild(root_0, char_literal228_tree);

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:628:23: ( interval | vec= ID )
            	int alt29 = 2;
            	int LA29_0 = input.LA(1);

            	if ( ((LA29_0 >= INT && LA29_0 <= DOUBLE) || LA29_0 == 26 || LA29_0 == 30 || LA29_0 == 35 || (LA29_0 >= 39 && LA29_0 <= 43) || (LA29_0 >= 45 && LA29_0 <= 56) || (LA29_0 >= 58 && LA29_0 <= 59)) )
            	{
            	    alt29 = 1;
            	}
            	else if ( (LA29_0 == ID) )
            	{
            	    int LA29_2 = input.LA(2);

            	    if ( (LA29_2 == 22 || LA29_2 == 34 || LA29_2 == 44) )
            	    {
            	        alt29 = 1;
            	    }
            	    else if ( (LA29_2 == 31) )
            	    {
            	        alt29 = 2;
            	    }
            	    else 
            	    {
            	        NoViableAltException nvae_d29s2 =
            	            new NoViableAltException("", 29, 2, input);

            	        throw nvae_d29s2;
            	    }
            	}
            	else 
            	{
            	    NoViableAltException nvae_d29s0 =
            	        new NoViableAltException("", 29, 0, input);

            	    throw nvae_d29s0;
            	}
            	switch (alt29) 
            	{
            	    case 1 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:628:24: interval
            	        {
            	        	PushFollow(FOLLOW_interval_in_addColumn2302);
            	        	interval229 = interval();
            	        	state.followingStackPointer--;

            	        	adaptor.AddChild(root_0, interval229.Tree);

            	        }
            	        break;
            	    case 2 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:628:33: vec= ID
            	        {
            	        	vec=(IToken)Match(input,ID,FOLLOW_ID_in_addColumn2306); 
            	        		vec_tree = (object)adaptor.Create(vec);
            	        		adaptor.AddChild(root_0, vec_tree);


            	        }
            	        break;

            	}

            	last=(IToken)Match(input,31,FOLLOW_31_in_addColumn2311); 
            		last_tree = (object)adaptor.Create(last);
            		adaptor.AddChild(root_0, last_tree);


            				tokens.Replace(first, "MatrixSupport.AddColumn((Matrix)");
            				if(vec != null)
            				{
            					tokens.InsertBefore(vec, "(Vector)");
            					tokens.Replace(last, ")");
            				}
            				else
            				{
            					tokens.Replace(last, "))");
            				}
            			

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "addColumn"

    public class interval_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "interval"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:643:1: interval : from= value dd= ':' to= value ;
    public MatlabParser.interval_return interval() // throws RecognitionException [1]
    {   
        MatlabParser.interval_return retval = new MatlabParser.interval_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken dd = null;
        MatlabParser.value_return from = default(MatlabParser.value_return);

        MatlabParser.value_return to = default(MatlabParser.value_return);


        object dd_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:644:2: (from= value dd= ':' to= value )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:644:4: from= value dd= ':' to= value
            {
            	root_0 = (object)adaptor.GetNilNode();

            	PushFollow(FOLLOW_value_in_interval2329);
            	from = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, from.Tree);
            	dd=(IToken)Match(input,34,FOLLOW_34_in_interval2333); 
            		dd_tree = (object)adaptor.Create(dd);
            		adaptor.AddChild(root_0, dd_tree);

            	PushFollow(FOLLOW_value_in_interval2337);
            	to = value();
            	state.followingStackPointer--;

            	adaptor.AddChild(root_0, to.Tree);

            			tokens.InsertBefore(((IToken)from.Start).TokenIndex, "MatrixSupport.CreateInterval((int)");
            			tokens.Replace(dd, ", ");
            			tokens.InsertBefore(((IToken)to.Start).TokenIndex, "(int)");
            			//tokens.InsertAfter(((IToken)to.Start).TokenIndex, ")");
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "interval"

    public class refValues_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "refValues"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:653:1: refValues : ( refValues2 | refValues3 );
    public MatlabParser.refValues_return refValues() // throws RecognitionException [1]
    {   
        MatlabParser.refValues_return retval = new MatlabParser.refValues_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        MatlabParser.refValues2_return refValues2230 = default(MatlabParser.refValues2_return);

        MatlabParser.refValues3_return refValues3231 = default(MatlabParser.refValues3_return);



        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:654:2: ( refValues2 | refValues3 )
            int alt30 = 2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0 == 30) )
            {
                int LA30_1 = input.LA(2);

                if ( (LA30_1 == ID) )
                {
                    int LA30_2 = input.LA(3);

                    if ( (LA30_2 == 24) )
                    {
                        int LA30_3 = input.LA(4);

                        if ( (LA30_3 == ID) )
                        {
                            int LA30_4 = input.LA(5);

                            if ( (LA30_4 == 31) )
                            {
                                alt30 = 1;
                            }
                            else if ( (LA30_4 == 24) )
                            {
                                alt30 = 2;
                            }
                            else 
                            {
                                NoViableAltException nvae_d30s4 =
                                    new NoViableAltException("", 30, 4, input);

                                throw nvae_d30s4;
                            }
                        }
                        else 
                        {
                            NoViableAltException nvae_d30s3 =
                                new NoViableAltException("", 30, 3, input);

                            throw nvae_d30s3;
                        }
                    }
                    else 
                    {
                        NoViableAltException nvae_d30s2 =
                            new NoViableAltException("", 30, 2, input);

                        throw nvae_d30s2;
                    }
                }
                else 
                {
                    NoViableAltException nvae_d30s1 =
                        new NoViableAltException("", 30, 1, input);

                    throw nvae_d30s1;
                }
            }
            else 
            {
                NoViableAltException nvae_d30s0 =
                    new NoViableAltException("", 30, 0, input);

                throw nvae_d30s0;
            }
            switch (alt30) 
            {
                case 1 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:654:4: refValues2
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_refValues2_in_refValues2352);
                    	refValues2230 = refValues2();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, refValues2230.Tree);

                    }
                    break;
                case 2 :
                    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:654:17: refValues3
                    {
                    	root_0 = (object)adaptor.GetNilNode();

                    	PushFollow(FOLLOW_refValues3_in_refValues2356);
                    	refValues3231 = refValues3();
                    	state.followingStackPointer--;

                    	adaptor.AddChild(root_0, refValues3231.Tree);

                    }
                    break;

            }
            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "refValues"

    public class refValues3_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "refValues3"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:657:1: refValues3 : '[' id1= ID ',' id2= ID ',' id3= ID ']' ;
    public MatlabParser.refValues3_return refValues3() // throws RecognitionException [1]
    {   
        MatlabParser.refValues3_return retval = new MatlabParser.refValues3_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken id1 = null;
        IToken id2 = null;
        IToken id3 = null;
        IToken char_literal232 = null;
        IToken char_literal233 = null;
        IToken char_literal234 = null;
        IToken char_literal235 = null;

        object id1_tree=null;
        object id2_tree=null;
        object id3_tree=null;
        object char_literal232_tree=null;
        object char_literal233_tree=null;
        object char_literal234_tree=null;
        object char_literal235_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:658:2: ( '[' id1= ID ',' id2= ID ',' id3= ID ']' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:658:4: '[' id1= ID ',' id2= ID ',' id3= ID ']'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	char_literal232=(IToken)Match(input,30,FOLLOW_30_in_refValues32367); 
            		char_literal232_tree = (object)adaptor.Create(char_literal232);
            		adaptor.AddChild(root_0, char_literal232_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_refValues32371); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal233=(IToken)Match(input,24,FOLLOW_24_in_refValues32373); 
            		char_literal233_tree = (object)adaptor.Create(char_literal233);
            		adaptor.AddChild(root_0, char_literal233_tree);

            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_refValues32377); 
            		id2_tree = (object)adaptor.Create(id2);
            		adaptor.AddChild(root_0, id2_tree);

            	char_literal234=(IToken)Match(input,24,FOLLOW_24_in_refValues32379); 
            		char_literal234_tree = (object)adaptor.Create(char_literal234);
            		adaptor.AddChild(root_0, char_literal234_tree);

            	id3=(IToken)Match(input,ID,FOLLOW_ID_in_refValues32383); 
            		id3_tree = (object)adaptor.Create(id3);
            		adaptor.AddChild(root_0, id3_tree);

            	char_literal235=(IToken)Match(input,31,FOLLOW_31_in_refValues32385); 
            		char_literal235_tree = (object)adaptor.Create(char_literal235);
            		adaptor.AddChild(root_0, char_literal235_tree);


            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "refValues3"

    public class refValues2_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "refValues2"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:661:1: refValues2 : '[' id1= ID ',' id2= ID ']' ;
    public MatlabParser.refValues2_return refValues2() // throws RecognitionException [1]
    {   
        MatlabParser.refValues2_return retval = new MatlabParser.refValues2_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken id1 = null;
        IToken id2 = null;
        IToken char_literal236 = null;
        IToken char_literal237 = null;
        IToken char_literal238 = null;

        object id1_tree=null;
        object id2_tree=null;
        object char_literal236_tree=null;
        object char_literal237_tree=null;
        object char_literal238_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:662:2: ( '[' id1= ID ',' id2= ID ']' )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:662:4: '[' id1= ID ',' id2= ID ']'
            {
            	root_0 = (object)adaptor.GetNilNode();

            	char_literal236=(IToken)Match(input,30,FOLLOW_30_in_refValues22396); 
            		char_literal236_tree = (object)adaptor.Create(char_literal236);
            		adaptor.AddChild(root_0, char_literal236_tree);

            	id1=(IToken)Match(input,ID,FOLLOW_ID_in_refValues22400); 
            		id1_tree = (object)adaptor.Create(id1);
            		adaptor.AddChild(root_0, id1_tree);

            	char_literal237=(IToken)Match(input,24,FOLLOW_24_in_refValues22402); 
            		char_literal237_tree = (object)adaptor.Create(char_literal237);
            		adaptor.AddChild(root_0, char_literal237_tree);

            	id2=(IToken)Match(input,ID,FOLLOW_ID_in_refValues22406); 
            		id2_tree = (object)adaptor.Create(id2);
            		adaptor.AddChild(root_0, id2_tree);

            	char_literal238=(IToken)Match(input,31,FOLLOW_31_in_refValues22408); 
            		char_literal238_tree = (object)adaptor.Create(char_literal238);
            		adaptor.AddChild(root_0, char_literal238_tree);


            			refParameters.Add(id1.Text);
            			refParameters.Add(id2.Text);	
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "refValues2"

    public class constant_return : ParserRuleReturnScope
    {
        private object tree;
        override public object Tree
        {
        	get { return tree; }
        	set { tree = (object) value; }
        }
    };

    // $ANTLR start "constant"
    // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:669:1: constant : (valpi= 'pi' | valeps= 'eps' ) ;
    public MatlabParser.constant_return constant() // throws RecognitionException [1]
    {   
        MatlabParser.constant_return retval = new MatlabParser.constant_return();
        retval.Start = input.LT(1);

        object root_0 = null;

        IToken valpi = null;
        IToken valeps = null;

        object valpi_tree=null;
        object valeps_tree=null;

        try 
    	{
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:669:10: ( (valpi= 'pi' | valeps= 'eps' ) )
            // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:669:12: (valpi= 'pi' | valeps= 'eps' )
            {
            	root_0 = (object)adaptor.GetNilNode();

            	// C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:669:12: (valpi= 'pi' | valeps= 'eps' )
            	int alt31 = 2;
            	int LA31_0 = input.LA(1);

            	if ( (LA31_0 == 58) )
            	{
            	    alt31 = 1;
            	}
            	else if ( (LA31_0 == 59) )
            	{
            	    alt31 = 2;
            	}
            	else 
            	{
            	    NoViableAltException nvae_d31s0 =
            	        new NoViableAltException("", 31, 0, input);

            	    throw nvae_d31s0;
            	}
            	switch (alt31) 
            	{
            	    case 1 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:669:13: valpi= 'pi'
            	        {
            	        	valpi=(IToken)Match(input,58,FOLLOW_58_in_constant2425); 
            	        		valpi_tree = (object)adaptor.Create(valpi);
            	        		adaptor.AddChild(root_0, valpi_tree);


            	        }
            	        break;
            	    case 2 :
            	        // C:\\Programming\\SVN\\trunk\\KERNEL\\Matlab.g:669:25: valeps= 'eps'
            	        {
            	        	valeps=(IToken)Match(input,59,FOLLOW_59_in_constant2430); 
            	        		valeps_tree = (object)adaptor.Create(valeps);
            	        		adaptor.AddChild(root_0, valeps_tree);


            	        }
            	        break;

            	}


            			if(valpi != null)
            			{
            				tokens.Replace(valpi, "Math.PI");
            			}
            			if(valeps != null)
            			{
            				tokens.Replace(valeps, "Math.Pow(2, -50)");
            			}
            		

            }

            retval.Stop = input.LT(-1);

            	retval.Tree = (object)adaptor.RulePostProcessing(root_0);
            	adaptor.SetTokenBoundaries(retval.Tree, (IToken) retval.Start, (IToken) retval.Stop);
        }
        catch (RecognitionException re) 
    	{
            ReportError(re);
            Recover(input,re);
    	// Conversion of the second argument necessary, but harmless
    	retval.Tree = (object)adaptor.ErrorNode(input, (IToken) retval.Start, input.LT(-1), re);

        }
        finally 
    	{
        }
        return retval;
    }
    // $ANTLR end "constant"

    // Delegated rules


   	protected DFA9 dfa9;
   	protected DFA15 dfa15;
   	protected DFA21 dfa21;
	private void InitializeCyclicDFAs()
	{
    	this.dfa9 = new DFA9(this);
    	this.dfa15 = new DFA15(this);
    	this.dfa21 = new DFA21(this);



	}

    const string DFA9_eotS =
        "\x10\uffff";
    const string DFA9_eofS =
        "\x10\uffff";
    const string DFA9_minS =
        "\x01\x1e\x01\x04\x01\x18\x01\x04\x01\x18\x01\x15\x01\x04\x01\x23"+
        "\x01\x18\x02\uffff\x01\x15\x01\uffff\x01\x04\x02\uffff";
    const string DFA9_maxS =
        "\x01\x1e\x01\x04\x01\x18\x01\x04\x01\x1f\x01\x15\x01\x04\x01\x37"+
        "\x01\x1f\x02\uffff\x01\x15\x01\uffff\x01\x21\x02\uffff";
    const string DFA9_acceptS =
        "\x09\uffff\x01\x01\x01\x02\x01\uffff\x01\x04\x01\uffff\x01\x03"+
        "\x01\x05";
    const string DFA9_specialS =
        "\x10\uffff}>";
    static readonly string[] DFA9_transitionS = {
            "\x01\x01",
            "\x01\x02",
            "\x01\x03",
            "\x01\x04",
            "\x01\x06\x06\uffff\x01\x05",
            "\x01\x07",
            "\x01\x08",
            "\x01\x09\x13\uffff\x01\x0a",
            "\x01\x0c\x06\uffff\x01\x0b",
            "",
            "",
            "\x01\x0d",
            "",
            "\x01\x0f\x1c\uffff\x01\x0e",
            "",
            ""
    };

    static readonly short[] DFA9_eot = DFA.UnpackEncodedString(DFA9_eotS);
    static readonly short[] DFA9_eof = DFA.UnpackEncodedString(DFA9_eofS);
    static readonly char[] DFA9_min = DFA.UnpackEncodedStringToUnsignedChars(DFA9_minS);
    static readonly char[] DFA9_max = DFA.UnpackEncodedStringToUnsignedChars(DFA9_maxS);
    static readonly short[] DFA9_accept = DFA.UnpackEncodedString(DFA9_acceptS);
    static readonly short[] DFA9_special = DFA.UnpackEncodedString(DFA9_specialS);
    static readonly short[][] DFA9_transition = DFA.UnpackEncodedStringArray(DFA9_transitionS);

    protected class DFA9 : DFA
    {
        public DFA9(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = DFA9_eot;
            this.eof = DFA9_eof;
            this.min = DFA9_min;
            this.max = DFA9_max;
            this.accept = DFA9_accept;
            this.special = DFA9_special;
            this.transition = DFA9_transition;

        }

        override public string Description
        {
            get { return "124:1: complexFunction : ( minimumIndexVector | sizeEval | stepfcm | validsumsqures | innerComplexFunction );"; }
        }

    }

    const string DFA15_eotS =
        "\x21\uffff";
    const string DFA15_eofS =
        "\x21\uffff";
    const string DFA15_minS =
        "\x01\x04\x03\uffff\x01\x16\x10\uffff\x01\x04\x01\uffff\x01\x16"+
        "\x02\uffff\x01\x04\x01\uffff\x01\x17\x04\uffff";
    const string DFA15_maxS =
        "\x01\x38\x03\uffff\x01\x2c\x10\uffff\x01\x3b\x01\uffff\x01\x2c"+
        "\x02\uffff\x01\x22\x01\uffff\x01\x22\x04\uffff";
    const string DFA15_acceptS =
        "\x01\uffff\x01\x01\x01\x02\x01\x03\x01\uffff\x01\x06\x01\x07\x01"+
        "\x08\x01\x09\x01\x0a\x01\x0b\x01\x0e\x01\x0f\x01\x10\x01\x11\x01"+
        "\x12\x01\x13\x01\x14\x01\x15\x01\x16\x01\x18\x01\uffff\x01\x17\x01"+
        "\uffff\x01\x0d\x01\x04\x01\uffff\x01\x04\x01\uffff\x01\x0c\x01\x05"+
        "\x01\x19\x01\x1a";
    const string DFA15_specialS =
        "\x21\uffff}>";
    static readonly string[] DFA15_transitionS = {
            "\x01\x04\x19\uffff\x01\x01\x04\uffff\x01\x09\x03\uffff\x01"+
            "\x0e\x01\x08\x01\x0c\x01\x10\x01\x14\x01\uffff\x01\x0f\x01\x13"+
            "\x01\x11\x01\x12\x01\x0d\x01\x0b\x01\x0a\x01\x07\x01\x06\x01"+
            "\x05\x01\x03\x01\x02",
            "",
            "",
            "",
            "\x01\x15\x15\uffff\x01\x16",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\x01\x17\x04\uffff\x02\x19\x0f\uffff\x01\x19\x03\uffff\x01"+
            "\x19\x03\uffff\x01\x18\x01\x19\x03\uffff\x05\x19\x01\uffff\x0c"+
            "\x19\x01\uffff\x02\x19",
            "",
            "\x02\x1b\x01\x1a\x02\x1b\x01\uffff\x02\x1b\x0e\uffff\x01\x1b",
            "",
            "",
            "\x01\x1c\x1d\uffff\x01\x1d",
            "",
            "\x01\x1f\x01\x20\x09\uffff\x01\x1e",
            "",
            "",
            "",
            ""
    };

    static readonly short[] DFA15_eot = DFA.UnpackEncodedString(DFA15_eotS);
    static readonly short[] DFA15_eof = DFA.UnpackEncodedString(DFA15_eofS);
    static readonly char[] DFA15_min = DFA.UnpackEncodedStringToUnsignedChars(DFA15_minS);
    static readonly char[] DFA15_max = DFA.UnpackEncodedStringToUnsignedChars(DFA15_maxS);
    static readonly short[] DFA15_accept = DFA.UnpackEncodedString(DFA15_acceptS);
    static readonly short[] DFA15_special = DFA.UnpackEncodedString(DFA15_specialS);
    static readonly short[][] DFA15_transition = DFA.UnpackEncodedStringArray(DFA15_transitionS);

    protected class DFA15 : DFA
    {
        public DFA15(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = DFA15_eot;
            this.eof = DFA15_eof;
            this.min = DFA15_min;
            this.max = DFA15_max;
            this.accept = DFA15_accept;
            this.special = DFA15_special;
            this.transition = DFA15_transition;

        }

        override public string Description
        {
            get { return "262:1: function : ( addColumn | randPerm | size1 | index | minor | zeros | mulDistance | find | max | minimum | mean | vectorAsFunction1 | vectorAsFunction2 | initfcm | kmeans | abs | trace | ones | sum | sqrt | det | inv | transposed | exp | cell | innerFunction );"; }
        }

    }

    const string DFA21_eotS =
        "\x08\uffff";
    const string DFA21_eofS =
        "\x08\uffff";
    const string DFA21_minS =
        "\x02\x04\x01\x07\x05\uffff";
    const string DFA21_maxS =
        "\x01\x3b\x01\x1a\x01\x2c\x05\uffff";
    const string DFA21_acceptS =
        "\x03\uffff\x01\x02\x01\x03\x01\x04\x01\x05\x01\x01";
    const string DFA21_specialS =
        "\x08\uffff}>";
    static readonly string[] DFA21_transitionS = {
            "\x01\x02\x04\uffff\x01\x04\x01\x06\x0f\uffff\x01\x01\x03\uffff"+
            "\x01\x03\x04\uffff\x01\x03\x03\uffff\x05\x03\x01\uffff\x0c\x03"+
            "\x01\uffff\x02\x05",
            "\x01\x07\x05\uffff\x01\x06\x0f\uffff\x01\x01",
            "\x01\x07\x0a\uffff\x01\x07\x03\uffff\x01\x03\x04\x07\x01\uffff"+
            "\x02\x07\x01\uffff\x01\x07\x02\uffff\x01\x07\x09\uffff\x01\x03",
            "",
            "",
            "",
            "",
            ""
    };

    static readonly short[] DFA21_eot = DFA.UnpackEncodedString(DFA21_eotS);
    static readonly short[] DFA21_eof = DFA.UnpackEncodedString(DFA21_eofS);
    static readonly char[] DFA21_min = DFA.UnpackEncodedStringToUnsignedChars(DFA21_minS);
    static readonly char[] DFA21_max = DFA.UnpackEncodedStringToUnsignedChars(DFA21_maxS);
    static readonly short[] DFA21_accept = DFA.UnpackEncodedString(DFA21_acceptS);
    static readonly short[] DFA21_special = DFA.UnpackEncodedString(DFA21_specialS);
    static readonly short[][] DFA21_transition = DFA.UnpackEncodedStringArray(DFA21_transitionS);

    protected class DFA21 : DFA
    {
        public DFA21(BaseRecognizer recognizer)
        {
            this.recognizer = recognizer;
            this.decisionNumber = 21;
            this.eot = DFA21_eot;
            this.eof = DFA21_eof;
            this.min = DFA21_min;
            this.max = DFA21_max;
            this.accept = DFA21_accept;
            this.special = DFA21_special;
            this.transition = DFA21_transition;

        }

        override public string Description
        {
            get { return "497:10: ( ( '-' )* ID | function | INT | constant | ( '-' )* DOUBLE )"; }
        }

    }

 

    public static readonly BitSet FOLLOW_method_in_program50 = new BitSet(new ulong[]{0x0000000000100002UL});
    public static readonly BitSet FOLLOW_20_in_method82 = new BitSet(new ulong[]{0x0000000040000010UL});
    public static readonly BitSet FOLLOW_ID_in_method87 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_refValues_in_method91 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_method96 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_method98 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_method102 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_vars_in_method104 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_method108 = new BitSet(new ulong[]{0x0200004040027050UL});
    public static readonly BitSet FOLLOW_statements_in_method112 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_END_in_method116 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_IF_in_ifStatement148 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_condExpression_in_ifStatement151 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_ifStatement155 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_mul_in_expression176 = new BitSet(new ulong[]{0x0000000006000002UL});
    public static readonly BitSet FOLLOW_set_in_expression179 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_mul_in_expression185 = new BitSet(new ulong[]{0x0000000006000002UL});
    public static readonly BitSet FOLLOW_ID_in_power198 = new BitSet(new ulong[]{0x0000000008000000UL});
    public static readonly BitSet FOLLOW_27_in_power202 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_power206 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_22_in_condExpression220 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_condExpression222 = new BitSet(new ulong[]{0x0000000000000080UL});
    public static readonly BitSet FOLLOW_CONDITION_in_condExpression224 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_condExpression226 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_condExpression228 = new BitSet(new ulong[]{0x0000000000000102UL});
    public static readonly BitSet FOLLOW_ANDOR_in_condExpression231 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_condExpression233 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_condExpression234 = new BitSet(new ulong[]{0x0000000000000080UL});
    public static readonly BitSet FOLLOW_CONDITION_in_condExpression236 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_condExpression238 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_condExpression240 = new BitSet(new ulong[]{0x0000000000000102UL});
    public static readonly BitSet FOLLOW_value_in_mul262 = new BitSet(new ulong[]{0x0000000030000002UL});
    public static readonly BitSet FOLLOW_set_in_mul265 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_mul271 = new BitSet(new ulong[]{0x0000000030000002UL});
    public static readonly BitSet FOLLOW_set_in_atom0 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_param_in_vars330 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_vars332 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_param_in_vars336 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_param351 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assignmentStatement_in_statement368 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_structuredStatement_in_statement372 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assignmentStatement_in_simpleStatement390 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assign_in_assignmentStatement412 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_vectorAssign_in_assignmentStatement416 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_returnKeyword_in_assignmentStatement421 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_complexFunction_in_assignmentStatement425 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_statement3d_in_assignmentStatement429 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_scalarAssign448 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_scalarAssign452 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_scalarAssign454 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_scalarAssign456 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_scalarAssign458 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_scalarAssign462 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_scalarAssign466 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_scalarAssign468 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_scalarAssign472 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_minimumIndexVector_in_complexFunction486 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_sizeEval_in_complexFunction490 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_stepfcm_in_complexFunction494 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_validsumsqures_in_complexFunction498 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_innerComplexFunction_in_complexFunction502 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_validsumsqures515 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures519 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_validsumsqures521 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures525 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_validsumsqures527 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures531 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_validsumsqures533 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures537 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_validsumsqures539 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures543 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_validsumsqures545 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_validsumsqures549 = new BitSet(new ulong[]{0x0000000100000000UL});
    public static readonly BitSet FOLLOW_32_in_validsumsqures553 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_validsumsqures557 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures561 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_validsumsqures563 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures567 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_validsumsqures569 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_validsumsqures573 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_validsumsqures575 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_validsumsqures579 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_stepfcm594 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_stepfcm598 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_stepfcm600 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_stepfcm604 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_stepfcm606 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_stepfcm610 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_stepfcm612 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_stepfcm616 = new BitSet(new ulong[]{0x0000000200000000UL});
    public static readonly BitSet FOLLOW_33_in_stepfcm620 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_stepfcm624 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_stepfcm628 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_stepfcm630 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_stepfcm632 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_stepfcm634 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_stepfcm638 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_stepfcm640 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_stepfcm644 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_stepfcm646 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_stepfcm650 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_vectorAssign666 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_vectorAssign670 = new BitSet(new ulong[]{0x0DFFEF8C44000610UL});
    public static readonly BitSet FOLLOW_34_in_vectorAssign675 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_vectorAssign677 = new BitSet(new ulong[]{0x0DFFEF8C44000610UL});
    public static readonly BitSet FOLLOW_value_in_vectorAssign684 = new BitSet(new ulong[]{0x0000000001800000UL});
    public static readonly BitSet FOLLOW_24_in_vectorAssign689 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_vectorAssign691 = new BitSet(new ulong[]{0x0000000001800000UL});
    public static readonly BitSet FOLLOW_24_in_vectorAssign696 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_vectorAssign700 = new BitSet(new ulong[]{0x0000000001800000UL});
    public static readonly BitSet FOLLOW_23_in_vectorAssign706 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_vectorAssign710 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_vectorAssign712 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_vectorAssign716 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_innerComplexFunction734 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerComplexFunction738 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_innerComplexFunction740 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerComplexFunction744 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_innerComplexFunction746 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerComplexFunction750 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_innerComplexFunction752 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_innerComplexFunction756 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerComplexFunction758 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_innerComplexFunction762 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerComplexFunction765 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_innerComplexFunction767 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerComplexFunction771 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_innerComplexFunction773 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_innerComplexFunction775 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_minimumIndexVector793 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_minimumIndexVector795 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_minimumIndexVector797 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_minimumIndexVector801 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_minimumIndexVector803 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_minimumIndexVector805 = new BitSet(new ulong[]{0x0000000800000000UL});
    public static readonly BitSet FOLLOW_35_in_minimumIndexVector807 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_minimumIndexVector811 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_minimumIndexVector813 = new BitSet(new ulong[]{0x0000001000000000UL});
    public static readonly BitSet FOLLOW_36_in_minimumIndexVector815 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_minimumIndexVector817 = new BitSet(new ulong[]{0x0000000000000200UL});
    public static readonly BitSet FOLLOW_INT_in_minimumIndexVector821 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_minimumIndexVector823 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_minimumIndexVector827 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_37_in_error842 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_error844 = new BitSet(new ulong[]{0x0000000000800800UL});
    public static readonly BitSet FOLLOW_NAME_in_error847 = new BitSet(new ulong[]{0x0000000000800800UL});
    public static readonly BitSet FOLLOW_23_in_error850 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_error852 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_38_in_returnKeyword862 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_returnKeyword864 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_returnKeyword866 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_funcAssign880 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_funcAssign884 = new BitSet(new ulong[]{0x01FFEF8840000010UL});
    public static readonly BitSet FOLLOW_function_in_funcAssign886 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_funcAssign890 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_addColumn_in_function904 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_randPerm_in_function910 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_size1_in_function916 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_index_in_function922 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_minor_in_function927 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_zeros_in_function932 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_mulDistance_in_function938 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_find_in_function944 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_max_in_function950 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_minimum_in_function955 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_mean_in_function960 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_vectorAsFunction1_in_function966 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_vectorAsFunction2_in_function971 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_initfcm_in_function976 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_kmeans_in_function981 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_abs_in_function986 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_trace_in_function991 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ones_in_function996 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_sum_in_function1001 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_sqrt_in_function1006 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_det_in_function1011 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_inv_in_function1016 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_transposed_in_function1021 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_exp_in_function1026 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_cell_in_function1031 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_innerFunction_in_function1036 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_39_in_trace1050 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_trace1052 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_trace1056 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_35_in_minimum1071 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_minimum1073 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_minimum1075 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_minor1088 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_minor1092 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_minor1096 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_minor1098 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_minor1102 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_minor1104 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_minor1108 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_minor1112 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_40_in_max1127 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_max1129 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_max1131 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_41_in_kmeans1147 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_kmeans1149 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_kmeans1151 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_kmeans1153 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_kmeans1157 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_cell1173 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_cell1177 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_cell1179 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_cell1181 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_cell1183 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_cell1187 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_42_in_sum1202 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_sum1204 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_sum1206 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_43_in_exp1221 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_exp1223 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_exp1225 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_transposed1241 = new BitSet(new ulong[]{0x0000100000000000UL});
    public static readonly BitSet FOLLOW_44_in_transposed1245 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_45_in_ones1260 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_ones1262 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_ones1264 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_ones1266 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_ones1270 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_46_in_inv1285 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_inv1287 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_inv1291 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_47_in_sqrt1306 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_sqrt1308 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_sqrt1310 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_48_in_det1325 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_det1327 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_det1331 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_innerFunction1347 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_innerFunction1351 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerFunction1354 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_innerFunction1356 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_innerFunction1360 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_innerFunction1362 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_49_in_abs1377 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_abs1379 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_abs1381 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_abs1383 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_50_in_initfcm1398 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_initfcm1400 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_initfcm1404 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_initfcm1406 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_initfcm1410 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_initfcm1414 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_51_in_mean1429 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_mean1433 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_mean1437 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_mean1441 = new BitSet(new ulong[]{0x0000000000000200UL});
    public static readonly BitSet FOLLOW_INT_in_mean1445 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_mean1449 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_52_in_find1464 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_find1466 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_find1470 = new BitSet(new ulong[]{0x0000000000000080UL});
    public static readonly BitSet FOLLOW_CONDITION_in_find1472 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_find1476 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_find1480 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_vectorAsFunction11496 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_vectorAsFunction11498 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_vectorAsFunction11502 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_vectorAsFunction11504 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_vectorAsFunction11506 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_vectorAsFunction11510 = new BitSet(new ulong[]{0x0000100000000002UL});
    public static readonly BitSet FOLLOW_44_in_vectorAsFunction11514 = new BitSet(new ulong[]{0x0000100000000002UL});
    public static readonly BitSet FOLLOW_ID_in_vectorAsFunction21534 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_vectorAsFunction21536 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_vectorAsFunction21538 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_vectorAsFunction21540 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_vectorAsFunction21544 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_vectorAsFunction21548 = new BitSet(new ulong[]{0x0000100000000002UL});
    public static readonly BitSet FOLLOW_44_in_vectorAsFunction21552 = new BitSet(new ulong[]{0x0000100000000002UL});
    public static readonly BitSet FOLLOW_53_in_mulDistance1569 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_mulDistance1571 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_mulDistance1575 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_mulDistance1577 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_mulDistance1581 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_mulDistance1583 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_54_in_zeros1598 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_zeros1602 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_zeros1604 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_zeros1608 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_zeros1612 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_index1625 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_index1629 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_index1631 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_index1635 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_55_in_size11651 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_size11653 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_size11657 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_size11659 = new BitSet(new ulong[]{0x0000000000000210UL});
    public static readonly BitSet FOLLOW_set_in_size11663 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_size11673 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_56_in_randPerm1689 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_randPerm1693 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_randPerm1697 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_assignColumn1714 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_assignColumn1716 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_assignColumn1720 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_assignColumn1722 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_assignColumn1724 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_assignColumn1726 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_assignColumn1728 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_assignColumn1733 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_assignColumn1735 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_assignColumn1739 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_assignColumn1741 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_assignColumn1743 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_assignColumn1747 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_26_in_value1765 = new BitSet(new ulong[]{0x0000000004000010UL});
    public static readonly BitSet FOLLOW_ID_in_value1769 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_function_in_value1773 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_INT_in_value1777 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_constant_in_value1781 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_26_in_value1786 = new BitSet(new ulong[]{0x0000000004000400UL});
    public static readonly BitSet FOLLOW_DOUBLE_in_value1790 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ifStatement_in_structuredStatement1806 = new BitSet(new ulong[]{0x0200004040027050UL});
    public static readonly BitSet FOLLOW_repetetiveStatement_in_structuredStatement1810 = new BitSet(new ulong[]{0x0200004040027050UL});
    public static readonly BitSet FOLLOW_ELSE_in_structuredStatement1814 = new BitSet(new ulong[]{0x0200004040027050UL});
    public static readonly BitSet FOLLOW_compoundStatement_in_structuredStatement1817 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_statements_in_compoundStatement1842 = new BitSet(new ulong[]{0x0000000000000020UL});
    public static readonly BitSet FOLLOW_END_in_compoundStatement1850 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_statement_in_statements1874 = new BitSet(new ulong[]{0x0200004040027052UL});
    public static readonly BitSet FOLLOW_whileStatement_in_repetetiveStatement1892 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_forStatement_in_repetetiveStatement1900 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_assign1917 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_assign1921 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_assign1924 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_power_in_assign1926 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_assign1931 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_57_in_assign1935 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assign3dtomatrix_in_statement3d1955 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_assignmatrixto3d_in_statement3d1959 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_init3d_in_statement3d1963 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_matrix3d_in_assign3dtomatrix1975 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_assign3dtomatrix1977 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_assign3dtomatrix1979 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_assign3dtomatrix1981 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID_in_assignmatrixto3d1998 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_assignmatrixto3d2000 = new BitSet(new ulong[]{0x0000000000001000UL});
    public static readonly BitSet FOLLOW_matrix3d_in_assignmatrixto3d2002 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_assignmatrixto3d2004 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID3D_in_init3d2020 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_init3d2022 = new BitSet(new ulong[]{0x0040200000000000UL});
    public static readonly BitSet FOLLOW_function3d_in_init3d2024 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_init3d2026 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_zeros3d_in_function3d2042 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ones3d_in_function3d2046 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_WHILE_in_whileStatement2065 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_condExpression_in_whileStatement2068 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_whileStatement2072 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_FOR_in_forStatement2094 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_forStatement2099 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_forStatement2103 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_forStatement2107 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_forStatement2111 = new BitSet(new ulong[]{0x0000000000000610UL});
    public static readonly BitSet FOLLOW_atom_in_forStatement2113 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_sizeEval2129 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_sizeEval2134 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_sizeEval2137 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_sizeEval2142 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_sizeEval2145 = new BitSet(new ulong[]{0x0000000000200000UL});
    public static readonly BitSet FOLLOW_21_in_sizeEval2148 = new BitSet(new ulong[]{0x0080000000000000UL});
    public static readonly BitSet FOLLOW_55_in_sizeEval2153 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_sizeEval2156 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_sizeEval2161 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_sizeEval2166 = new BitSet(new ulong[]{0x0000000000040000UL});
    public static readonly BitSet FOLLOW_SEMI_in_sizeEval2169 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_ID3D_in_matrix3d2185 = new BitSet(new ulong[]{0x0000000000400000UL});
    public static readonly BitSet FOLLOW_22_in_matrix3d2189 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_matrix3d2191 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_matrix3d2193 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_matrix3d2195 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_matrix3d2199 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_expression_in_matrix3d2201 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_matrix3d2205 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_54_in_zeros3d2221 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_zeros3d2223 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_zeros3d2227 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_zeros3d2229 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_zeros3d2233 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_zeros3d2235 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_zeros3d2237 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_45_in_ones3d2255 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_ones3d2257 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_ones3d2261 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_ones3d2263 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_ones3d2267 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_ones3d2269 = new BitSet(new ulong[]{0x0000000000800000UL});
    public static readonly BitSet FOLLOW_23_in_ones3d2271 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_addColumn2293 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_addColumn2297 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_addColumn2299 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_interval_in_addColumn2302 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_ID_in_addColumn2306 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_addColumn2311 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_value_in_interval2329 = new BitSet(new ulong[]{0x0000000400000000UL});
    public static readonly BitSet FOLLOW_34_in_interval2333 = new BitSet(new ulong[]{0x0DFFEF8844000610UL});
    public static readonly BitSet FOLLOW_value_in_interval2337 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_refValues2_in_refValues2352 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_refValues3_in_refValues2356 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_refValues32367 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_refValues32371 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_refValues32373 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_refValues32377 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_refValues32379 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_refValues32383 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_refValues32385 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_30_in_refValues22396 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_refValues22400 = new BitSet(new ulong[]{0x0000000001000000UL});
    public static readonly BitSet FOLLOW_24_in_refValues22402 = new BitSet(new ulong[]{0x0000000000000010UL});
    public static readonly BitSet FOLLOW_ID_in_refValues22406 = new BitSet(new ulong[]{0x0000000080000000UL});
    public static readonly BitSet FOLLOW_31_in_refValues22408 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_58_in_constant2425 = new BitSet(new ulong[]{0x0000000000000002UL});
    public static readonly BitSet FOLLOW_59_in_constant2430 = new BitSet(new ulong[]{0x0000000000000002UL});

}
