﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace Sample
{
    using System;
    using System.Diagnostics;
    using Nwc.XmlRpc;
    using System.Collections;
    /// <remarks>
    /// This is a very trivial server that does nothing more then fire up the embeded
    /// server on a hardcoded port serving only itself and the system object.
    /// </remarks>
    class SampleServer
    {
        const int PORT = 8080;

        /// <summary><c>LoggerDelegate</c> compliant method that does logging to Console.
        /// This method filters out the <c>LogLevel.Information</c> chatter.</summary>
        static public void WriteEntry(String msg, LogLevel level)
        {
            if (level > LogLevel.Information) // ignore debug msgs
                Console.WriteLine("{0}: {1}", level, msg);
        }

        /// <summary>The application starts here.</summary>
        /// <remarks>This method instantiates an <c>XmlRpcServer</c> as an embedded XML-RPC server,
        /// then add this object to the server as an XML-RPC handler, and finally starts the server.</remarks>
        //public static void Main()
        //{
        //    // Use the console logger above.
        //    Logger.Delegate = new Logger.LoggerDelegate(WriteEntry);

        //    XmlRpcServer server = new XmlRpcServer(PORT);
        //    server.Add("sample", new SampleServer());
        //    Console.WriteLine("Web Server Running on port {0} ... Press ^C to Stop...", PORT);
        //    server.Start();
        //}

        /// <summary>A method that returns the current time.</summary>
        /// <return>The current <c>DateTime</c> of the server is returned.</return>
        public ArrayList Ping()
        {
            ArrayList arrins = new ArrayList(6);
            arrins[2] = 4.3;
            ArrayList[] arrd = new ArrayList[5];
            arrd[3] = arrins;
            ArrayList arr = new ArrayList(arrd);
            
            return arr;
        }

        /// <summary>A method that echos back it's arguement.</summary>
        /// <param name="arg">A <c>String</c> to echo back to the caller.</param>
        /// <return>Return, as a <c>String</c>, the <paramref>arg</paramref> that was passed in.</return>
        public String Echo(String arg)
        {
            return arg;
        }
    }
}
