﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Reflection;
using MathNet.Numerics.LinearAlgebra;
using System.Diagnostics;

namespace Service
{
    public class Service
    {
        public static string Implement(string methodName, ArrayList data, ArrayList paramsAdd)
        {
            parameters = new Object[paramsAdd.Count + 1];

            Load(methodName);
            MethodInfo method = assembly.GetType(methodName + "Class").GetMethod(methodName);

            parameters[0] = ToMatrix(data);
            for (int i = 0; i < paramsAdd.Count; i++)
            {
                //parameters[i + 1] = (Matrix)((double)paramsAdd[i]);
                parameters[i + 1] = (Matrix)(Double.Parse((string)paramsAdd[i]));
            }

            Matrix result;
            Stopwatch sw = new Stopwatch();
            sw.Start();
            try
            {
                result = (Matrix)method.Invoke(null, parameters);
            }
            catch (Exception e)
            {
                throw new Exception("Some error occurs during method invocation.\n Exception:" + e.ToString());
            }
            sw.Stop();

            ArrayList arrayResult = ToList(result);
            arrayResult.Add(sw.Elapsed.TotalSeconds);
            return ConvertToString(ToStringList(arrayResult));
        }

        //search for an assembly and if success - load it in appdomain
        private static void Load(string libraryName)
        {
            if (hardcoded.Contains(libraryName))
            {
                if (File.Exists(@"..\..\Libraries\Algorythms.dll"))
                {
                    assembly = Assembly.LoadFrom(@"..\..\Libraries\Algorythms.dll");
                }
            }
            else if (File.Exists(@"..\..\Libraries\" + libraryName + ".dll"))
            {
                assembly = Assembly.LoadFrom(@"..\..\Libraries\" + libraryName + ".dll");
            }
        }

        //list[0] contains number of rows
        //private static Matrix ToMatrix(ArrayList list)
        //{
        //    double rowCount = Double.Parse((string)list[0]);
        //    ArrayList listToConvert = list.GetRange(1, list.Count - 1);
        //    double[] array = new double[listToConvert.Count];
        //    for (int i = 0; i < listToConvert.Count; i++)
        //    {
        //        array[i] = Double.Parse((string)listToConvert[i]); 
        //    }
        //    //array = (double[])listToConvert.ToArray(typeof(double));
        //    return new Matrix(array, (int)rowCount);
        //}

        private static Matrix ToMatrix(ArrayList list)
        {
            double rowCount = Double.Parse(list[0].ToString());
            ArrayList listToConvert = list.GetRange(1, list.Count - 1);
            double[] array = new double[listToConvert.Count];
            for (int i = 0; i < listToConvert.Count; i++)
            {
                array[i] = (double)listToConvert[i];
            }
            //array = (double[])listToConvert.ToArray(typeof(double));
            return new Matrix(array, (int)rowCount);
        }

        public static string ConvertToString(string[] array)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < array.Length; i++)
            {
                result.Append(array[i] + ";");
            }
            return result.ToString();
        }

        public static ArrayList ToList(Matrix m)
        {
            ArrayList list = new ArrayList();
            list.Add((double)m.RowCount);
            for (int i = 0; i < m.ColumnCount; i++)
            {
                for (int j = 0; j < m.RowCount; j++)
                {
                    list.Add(m[j, i]);
                }
            }
            return list;
        }

        public static string[] ToStringList(ArrayList inputList)
        {
            string[] outputList = new string[inputList.Count];
            for (int i = 0; i < inputList.Count; i++)
            {
                outputList[i] = inputList[i].ToString();
            }
            return outputList;
        }

        public static ArrayList FindAllMethods()
        {
            ArrayList results = new ArrayList();
            //Directory.SetCurrentDirectory(ROOT);
            StringBuilder sb = new StringBuilder(hardcoded.ToString());
            string[] libs = Directory.GetFiles(@"..\..\Libraries\");
            foreach (string lib in libs)
            {
                StringBuilder methodName = new StringBuilder(lib.Remove(0, 16));
                methodName.Replace(".dll", "");
                Assembly assembly = Assembly.LoadFrom(lib);
                MethodInfo mi;
                Type[] types = null;
                if (methodName.ToString() != "Algorythms")
                {
                    mi = assembly.GetType(methodName.ToString() + "Class").GetMethod(methodName.ToString());
                    results.Add(mi.ToString());
                }
                else
                    types = assembly.GetTypes();

                MethodInfo info;
                if (types != null)
                {
                    foreach (Type type in types)
                    {
                        info = type.GetMethod(type.Name.Replace("Class", ""));
                        results.Add(info.ToString());
                    }
                }
            }
            return results;
        }

        static Object[] parameters;
        static Assembly assembly;
        static List<string> hardcoded = new List<string>() { "kMeansCluster", "FCM", "EMGM", "Dunn", "Hartigan"};
    }
}
