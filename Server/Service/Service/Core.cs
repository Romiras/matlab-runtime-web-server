/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Text;
using Path = System.IO.Path;
using Antlr.Runtime;
using Microsoft.CSharp;
using System.IO;
using System.CodeDom.Compiler;
using System.Globalization;


namespace Service
{
    public class Core
    {
        static string matlabName;
        static string csName;

        public static void Compile(string inputFileName, string inputFile)
        {
            matlabName = @"..\..\AlgorythmsM\" + inputFileName + ".m";
            csName = @"..\..\AlgorythmsCS\" + inputFileName + ".cs";
            
            FindAndDelete(matlabName);
            FindAndDelete(csName);

            WriteToFile(matlabName, inputFile);
            
            ITokenStream tokens;
            MatlabParser parser;
            //initialize parser and lexer
            Init(matlabName, out tokens, out parser);
            //insert all necessary data inside the output stream
            StringBuilder finalOutput = InsertIdentifiers(tokens, parser, inputFileName);

            //write to cs file
            WriteToFile(csName, finalOutput.ToString());
            //compile to dll file
            CompileLibrary(csName);
        }

        private static StringBuilder InsertIdentifiers(ITokenStream tokens, MatlabParser parser, string methodName)
        {
            string storedTypes = String.Empty;
            foreach (String storedType in parser.identifiers)
            {
                if (!parser.parameters.Contains(storedType))
                {
                    storedTypes += "static Matrix " + storedType + ";\n";
                }
            }
            foreach (String storedType in parser.identifiers3d)
            {
                if (!parser.parameters.Contains(storedType))
                {
                    storedTypes += "Matrix3d " + storedType + " = null;\n";
                }
            }

            StringBuilder finalOutput = new StringBuilder(tokens.ToString());
            finalOutput.Insert(119, "\n" + storedTypes + "\n");
            finalOutput.Replace("Wrapper", methodName + "Class");
            return finalOutput;
        }

        private static void Init(string inputFileName, out ITokenStream tokens, out MatlabParser parser)
        {
            ICharStream input = new ANTLRFileStream(inputFileName);
            MatlabLexer lex = new MatlabLexer(input);
            tokens = new TokenRewriteStream(lex);
            parser = new MatlabParser(tokens);
            parser.program();
        }

        public static void WriteToFile(string fileName, string output)
        {
            FindAndDelete(fileName);

            using (StreamWriter sw = File.CreateText(fileName))
            {
                sw.Write(output.ToString());
                sw.Close();
            }
        }

        private static void FindAndDelete(string fileName)
        {
            if (File.Exists(fileName))
                File.Delete(fileName);
        }

        public static bool CompileLibrary(String sourceName)
        {
            FileInfo sourceFile = new FileInfo(sourceName);
            CodeDomProvider provider = null;
            bool compileOk = false;

            // Select the code provider based on the input file extension.
            if (sourceFile.Extension.ToUpper(CultureInfo.InvariantCulture) == ".CS")
                provider = new Microsoft.CSharp.CSharpCodeProvider();
            else
                Console.WriteLine("Source file must have a .cs extension");

            if (provider != null)
            {
                // Format the executable file name.
                // Build the output assembly path using the current directory
                // and <source>_cs.exe or <source>_vb.exe.

                String dllName = String.Format(@"{0}\{1}" + ".dll",
                    @"..\..\Libraries\",
                    sourceFile.Name.Replace(".cs", ""));

                CompilerParameters cp = new CompilerParameters();

                // Generate an executable instead of 
                // a class library.
                cp.GenerateExecutable = false;
                
                // Specify the assembly file name to generate.
                cp.OutputAssembly = dllName;

                // Save the assembly as a physical file.
                cp.GenerateInMemory = false;

                // Set whether to treat all warnings as errors.
                cp.TreatWarningsAsErrors = false;

                cp.ReferencedAssemblies.Add("MathNet.Iridium.dll");
                cp.ReferencedAssemblies.Add("SupportMathFunctions.dll");

                // Invoke compilation of the source file.
                CompilerResults cr = provider.CompileAssemblyFromFile(cp,
                    sourceName);

                if (cr.Errors.Count > 0)
                {
                    // Display compilation errors.
                    Console.WriteLine("Errors building {0} into {1}",
                        sourceName, cr.PathToAssembly);
                    foreach (CompilerError ce in cr.Errors)
                    {
                        Console.WriteLine("  {0}", ce.ToString());
                        Console.WriteLine();
                    }
                }
                else
                {
                    // Display a successful compilation message.
                    Console.WriteLine("Source {0} built into {1} successfully.",
                        sourceName, cr.PathToAssembly);
                }

                // Return the results of the compilation.
                if (cr.Errors.Count > 0)
                {
                    compileOk = false;
                }
                else
                {
                    compileOk = true;
                }
            }
            return compileOk;
        }


    }
}
