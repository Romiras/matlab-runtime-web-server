/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/** Convert the simple input to be java code; wrap in a class,
 *  convert method with "public void", add decls.
 */
grammar Matlab;

options {
	language=CSharp2;
	//backtrack = true;
	//output=AST;
}

@header {

}

@members {
public TokenRewriteStream tokens;
public System.Collections.Generic.List<String> identifiers = new System.Collections.Generic.List<string>();
public System.Collections.Generic.List<String> identifiers3d = new System.Collections.Generic.List<string>();
public System.Collections.Generic.List<String> parameters = new System.Collections.Generic.List<string>();
public System.Collections.Generic.List<String> refParameters = new System.Collections.Generic.List<string>();
public int haven;
public string returnValue;
}

program
@init {
    tokens = (TokenRewriteStream)input; 
    IToken start = input.LT(1);
}
    :   method+
        {
        tokens.InsertBefore(start,"using System;\nusing System.IO;\nusing MathNet.Numerics.LinearAlgebra;\nusing SupportMathFunctions;\npublic class Wrapper{\n");      
        // note the reference to the last token matched for method:
        tokens.InsertAfter($method.stop, "\n}\n");
//        haven = (IToken)(method.stop).TokenIndex;
        }
    ;

method
    :   m='function' (rv=ID | refValues) s='=' ID br1='(' vars lbl=')' st=statements fin=END
        {
        	
        	tokens.Replace($m, $s, "public static Matrix ");
        	tokens.Replace($lbl, ")\n{\n");
        	tokens.Replace($fin, "}");
        	if($rv != null)
        	{
        		returnValue=$rv.Text;
	        	tokens.InsertBefore($fin, "return " + returnValue + ";\n");
        	}
        	System.Text.StringBuilder sb = new System.Text.StringBuilder();
        	for(int j = 0; j < refParameters.Count; j++)
        	{
			sb.Append("ref Matrix " + refParameters[j] + ", ");
        	}
        	tokens.Replace($br1, "(" + sb.ToString());
        }
    ;
    
ifStatement 
    : IF^ condExpression com=','
    {
    	tokens.Replace($com, "");
    	tokens.InsertBefore($condExpression.start, "(");
    	tokens.InsertAfter($condExpression.stop, ")");
    }
    ;


expression:   mul (('+'|'-') mul)* ;

power	:	id1=ID pow='^' id2=ID
	{
		tokens.Replace($id1, "Math.Pow(" + $id1.Text + ", " + $id2.Text);
	}
	;

condExpression :  '(' value CONDITION value ')' (ANDOR '('value CONDITION value ')')*
    ;
    

mul :   value (('*'|'/') value)*
    ;

atom:   ID
    |   INT
    |   DOUBLE
    ;
    
vars 
    :	(param ',')* param
    ;

param	: id1=ID
	{
		tokens.InsertBefore($id1, "Matrix ");
		parameters.Add($id1.Text);
	}
	;

statement
    : assignmentStatement | structuredStatement
    ;
	
simpleStatement
    : assignmentStatement
    //| emptyStatement
    ;

assignmentStatement
    : assign | vectorAssign  | returnKeyword | complexFunction | statement3d  
    ;

scalarAssign
	:  mx1=ID curl1='(' value ',' value curl2=')' seq='=' expression last=';'
	{
		tokens.Replace($curl1, "[");
		tokens.Replace($curl2, "]");
		tokens.Replace($seq, "= (int)");
	}
	;

complexFunction
	: minimumIndexVector | sizeEval | stepfcm | validsumsqures | innerComplexFunction
	;

validsumsqures
	: first='[' st=ID ',' sw=ID ',' sb=ID ',' cintra=ID ',' cinter=ID ']' prelast='=' func='validsumsqures' cur1='(' data=ID ',' labels=ID ',' k=ID ')' last=';'
	{
		tokens.Delete($first, $prelast);
		tokens.Replace($func, "MatrixSupport.ValidSumSqures");
		tokens.InsertAfter($cur1, "ref " + $st.Text + ",ref " + $sw.Text + ", " + "ref " + $sb.Text + ", "
		+ "ref " + $cintra.Text + ",ref " + $cinter.Text + ", ");
		if(!identifiers.Contains($st.Text))
		{
			identifiers.Add($st.Text);
		}
		if(!identifiers.Contains($sw.Text))
		{
			identifiers.Add($sw.Text);
		}
		if(!identifiers.Contains($sb.Text))
		{
			identifiers.Add($sb.Text);
		}
		if(!identifiers.Contains($cintra.Text))
		{
			identifiers.Add($cintra.Text);
		}
		if(!identifiers.Contains($cinter.Text))
		{
			identifiers.Add($cinter.Text);
		}
	}
	;

stepfcm	: first='[' u=ID ',' center=ID ',' objfcn=ID ']' prelast='=' func='stepfcm' cur1='(' mx=ID ',' ID ',' id1=ID ',' id2=ID ')' last=';'
	{
		tokens.Delete($first, $prelast);
		tokens.Replace($func, "FCM.StepFCM");
		tokens.InsertAfter($cur1, "ref " + $u.Text + ",ref " + $center.Text + ", " + "ref " + $objfcn.Text + ", ");
		tokens.InsertBefore($id1, "(int)");
		tokens.InsertBefore($id2, "(double)");
		if(!identifiers.Contains($center.Text))
		{
			identifiers.Add($center.Text);
		}
		
	}
	;

vectorAssign
	: mx1=ID curl1='(' (column=':' ',')*  id1=value (semi=',' ':')* (',' secondArg=ID)* curl2=')' seq='=' expression last=';'
	{
		if($semi != null)
		{
			tokens.Delete($mx1, $curl2);
			tokens.InsertBefore($seq, "Matrix tempVector"+$mx1.Text);
			if(!identifiers.Contains($mx1.Text))
			{
				identifiers.Add($mx1.Text);
			}
			tokens.InsertAfter($last, "\nMatrixSupport.CreateAndSetRow(ref " + $mx1.Text +", (Vector)" + "tempVector"+$mx1.Text 
				+ ", " 
				+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
				+ ");");
		}
		else if($secondArg != null)
		{
			tokens.Replace($curl1, "[");
			tokens.Replace($curl2, "]");
			tokens.Replace($seq, "= (double)");
		}
		else if($column != null)
		{
			
			tokens.Delete($mx1, $curl2);
			tokens.InsertBefore($seq, "Matrix tempVector"+$mx1.Text);
			if(!identifiers.Contains($mx1.Text))
			{
				identifiers.Add($mx1.Text);
			}
			tokens.InsertAfter($last, "\nMatrixSupport.CreateAndSetColumn(ref " + $mx1.Text +", (Vector)" + "tempVector"+$mx1.Text 
				+ ", " 
				+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
				+ ");");
		}
		else
		{
			tokens.Replace($curl1, "[");
			tokens.Replace($curl2, ", 0]");
			tokens.InsertAfter($seq, "(double)");
		}
	}
	;

innerComplexFunction 
	:  first='[' id1=ID ',' id2=ID ',' id3=ID ']' eq='=' ID br1='(' (ID ',')* ID ')' ';'
	{
		tokens.Delete($first, $eq);
		tokens.Replace($br1, "(" + "ref " + $id1.Text + ", "
		+ "ref " + $id2.Text + ", "
		+ "ref " + $id3.Text + ", ");
	}	
	;

minimumIndexVector
	:  first='[' ID ',' vector=ID ']' '=' 'min(' mx=ID ',' '[]' ',' dim=INT ')' last=';'
	{
		tokens.Delete($first, $last);
		if(!identifiers.Contains($vector.Text))
		{
			identifiers.Add($vector.Text);
		}
		tokens.InsertAfter($last, $vector.Text + "=" + "MatrixSupport.RetriveMinimumIndexesVector(" + $mx.Text + "," + $dim.Text + ");");
	}
	;

error
	:  'error' '('  NAME* ')' ';'
	;

returnKeyword	: 'return' value ';'
	;

funcAssign
	:  num=ID eq='=' function semi=';'
	{
		if (!identifiers.Contains($num.Text))
		{
			identifiers.Add($num.Text);
			//tokens.InsertAfter(haven, "Matrix "+ $num.Text + ";");
			//tokens.InsertBefore($num, "Matrix ");
			tokens.InsertAfter($eq, "(Matrix)");
			//tokens.InsertBefore($semi, ")");
		}
		
	}
	;

function
	: addColumn 
	| randPerm 
	| size1 
	| index
	| minor
	| zeros 
	| mulDistance 
	| find 
	| max
	| minimum
	| mean 
	| vectorAsFunction1
	| vectorAsFunction2
	| initfcm
	| kmeans
	| abs
	| trace
	| ones
	| sum
	| sqrt
	| det
	| inv
	| transposed
	| exp
	| cell
	| innerFunction
	
	;

trace	: tr='trace(' ID last=')'
	{
		tokens.Delete($tr);
		tokens.Replace($last, ".Trace()");
	}
	;

minimum	: func='min(' ID ')'
	{
		tokens.Replace($func, "MatrixSupport.Min(");
	}
	;

minor	: ID br='(' row=ID ',' col1=ID ':' col2=ID br4=')'
	{
		tokens.Replace($br, $br4, ".GetMatrix(" + $row.Text + ", " + $row.Text + ", " + $col1.Text + ", " + $col2.Text + ")");
	}
	;

max	: func='max(' ID ')'
	{
		tokens.Replace($func, "MatrixSupport.Max(");
	}
	;


kmeans	: func='kmeans(' ID ',' ID br=')'
	{
		tokens.Replace($func, "kMeans.kMeansCluster(ref c, ");
		tokens.Replace($br, ", (Matrix)false)");
		identifiers.Add("c");
	}
	;


cell	: start=ID br1='(' ID ',' ID br2=')'
	{
		tokens.Replace($br1, "[");
		tokens.Replace($br2, "]");
	}
	;

sum	: func='sum(' ID ')'
	{
		tokens.Replace($func, "MatrixSupport.Sum((Vector)");
	}
	;

exp	: func='exp(' expression ')'
	{
		tokens.Replace($func, "Math.Exp(");
	}
	;

transposed
	: var=ID trans='\''
	{
		tokens.Replace($trans, ".Transpose()");
	}
	;

ones	: func='ones(' value ',' value br=')'
	{
		tokens.Replace($func, "new Matrix(");
		tokens.Replace($br, ", 1.0)");
	}
	;

inv	: func='inv(' value br=')'
	{
		tokens.Delete($func);
		tokens.Replace($br, ".Inverse()");
	}
	;

sqrt	: func='sqrt(' value ')'
	{
		tokens.Replace($func, "Math.Sqrt(");
	}
	;

det	: func='det(' value br=')'
	{
		tokens.Delete($func);
		tokens.Replace($br, ".Determinant()");
	}
	;

innerFunction
	:	start=ID br1='(' (ID ',')* ID ')'
	{
		tokens.Replace($start, $start.Text);
	}
	;

abs	: start='abs' '(' expression ')'
	{
		tokens.Replace($start, "(Matrix)Math.Abs");
	}
	;

initfcm	: first='initfcm' '(' id1=ID ',' id2=ID last=')'
	{
		tokens.Replace($first, $last, "FCM.InitFCM((int)" + id1.Text + ",(int) " + id2.Text + ")");
	}
	;

mean	: first='mean' curl1='(' id1=value com=',' fil=INT last=')'
	{
		tokens.Replace($first, $curl1, "MatrixSupport.MeanMatrix(");
	}
	;

find	: first='find' '(' mx=ID CONDITION filter=ID last=')'
	{
		tokens.Replace($first, $last, "MatrixSupport.Find(" + $mx.Text + ", " + $filter.Text + ")");
	}
	;

vectorAsFunction1
	: mx1=ID '(' id1=ID ',' ':' curl=')' trans='\''*
	{
		
		if(trans != null)
		{
			tokens.Delete($trans);
			tokens.Replace($mx1, $curl, "Matrix.Transpose(MatrixSupport.GetMatrixByRow((Vector)" + $id1.Text + ", " + $mx1.Text + "))");
		}
		else
		{
			tokens.Replace($mx1, $curl, "MatrixSupport.GetMatrixByRow((Vector)" + $id1.Text + ", " + $mx1.Text + ")");
		}
	}
	;
	
	
vectorAsFunction2
	: mx1=ID '(' ':' ',' id1=ID curl=')' trans='\''*
	{
		
		if(trans != null)
		{
			tokens.Delete($trans);
			tokens.Replace($mx1, $curl, "Matrix.Transpose(MatrixSupport.GetMatrixByColumn((Vector)" + $id1.Text + ", " + $mx1.Text + "))");
		}
		else
		{
			tokens.Replace($mx1, $curl, "MatrixSupport.GetMatrixByColumn((Vector)" + $id1.Text + ", " + $mx1.Text + ")");
		}
	}
	;

mulDistance
	: func='DistMatrix' '(' id1=ID ',' id2=ID ')'
	{
		tokens.Replace($func, "MatrixSupport.GetDistanceMatrix");
	}
	;

zeros	: first='zeros(' id1=value ',' id2=expression last=')'
	{	
		tokens.Replace($first, $last, "new Matrix((int)"
		+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
		+ ", (int)"
		+ tokens.ToString( ((IToken)id2.Start).TokenIndex, ((IToken)id2.Stop).TokenIndex )
		+ " )");
	}
	;

index	: ID first='(' expression last=')'	
	{
		tokens.Replace($first, "[");
		tokens.Replace($last, ", 0]");
	}
	;

size1	: first='size' '(' id1=ID ',' id2=(ID | INT) last=')'
	{
		tokens.Replace($first, $last, "MatrixSupport.Size(" + $id1.Text + ", " + $id2.Text + ")");
	}
	;

randPerm
	: st='randperm(' arg=value fn=')'
	{
		//tokens.Replace($st, "");
		
		//tokens.Replace($fn, "");
		tokens.Replace($st, "RandomSupport.RandomPermutation((int)");
	}
	;
//obsolete
assignColumn 
	:	mx1=ID '(' id1=value ',' ':' ')' '='  mx2=ID '(' id2=value ',' ':' last=')'	 
	{
		tokens.Delete($mx1, $last);
		tokens.InsertAfter($last, "Matrix" + $mx1.Text + ".SetColumnVector(" + $mx2.Text + ".GetColumnVector((int)" 
		+ tokens.ToString( ((IToken)id2.Start).TokenIndex, ((IToken)id2.Stop).TokenIndex )
		+ "), "
		+ tokens.ToString( ((IToken)id1.Start).TokenIndex, ((IToken)id1.Stop).TokenIndex )
		+ ");");
		
	}
	;

value	:  (('-')* ID | function | INT | constant | ('-')* DOUBLE)
	;

structuredStatement
    : (ifStatement | repetetiveStatement | 'else') compoundStatement 
    ;

compoundStatement
    : 
    st=statements
    cur2=END!
    {
    	tokens.InsertBefore(((IToken)st.Start).TokenIndex, "{\n");
      	tokens.Replace($cur2, '}');
    }
    ;

statements
    : statement+
    ;

repetetiveStatement
    : whileStatement
    | forStatement
    ;

assign:   (num=ID eq='=' (expression|power) semi=';' | 'break;')
	{
		if (num != null)
		{
			if (!identifiers.Contains($num.Text))
			{
				identifiers.Add($num.Text);
			}
			tokens.InsertAfter($eq, " (Matrix)(");
			tokens.Replace($semi, ");");
		}
		
	}
    ;

statement3d
	: 
	assign3dtomatrix|
	assignmatrixto3d|
	init3d	
	;

assign3dtomatrix
	: matrix3d '=' ID ';'
	{
	
	}	
	;

assignmatrixto3d
	: var=ID '=' matrix3d ';'
	{
		identifiers.Add($var.Text);
	}
	;

init3d
	: id=ID3D '=' function3d ';'
		{
			if (!identifiers3d.Contains($id.Text))
			{
				identifiers3d.Add($id.Text);
			}
		}	
	;

function3d
	: zeros3d | ones3d
	;

    
whileStatement
    : WHILE^ condExpression com=',' 
    {
    		tokens.Replace($com, "");
    		tokens.InsertBefore($condExpression.start, "(");
    		tokens.InsertAfter($condExpression.stop, ")");
    }
    ;

forStatement	: start=FOR^ it=ID eq='=' min=value dd=':' atom
	{
		tokens.Delete($it, $dd);
		tokens.Replace($atom.start, $atom.stop, "(" + "int "+ $it.Text + " = " 
		+ tokens.ToString( ((IToken)min.Start).TokenIndex, ((IToken)min.Stop).TokenIndex )
		+ "- 1; " + $it.Text + " < " + 
		tokens.ToString( ((IToken)$atom.start).TokenIndex, ((IToken)$atom.stop).TokenIndex ) 
		+ "; " + $it.Text + "++)");
	}
;

sizeEval 
	:	first='['! id1=ID! ','! id2=ID! ']'! '='! op='size'! '('! mx=ID! last=')'! ';'
	{
		tokens.Delete($first, $last);
		tokens.InsertAfter($last, "int " + $id1.Text + " = " + $mx.Text + ".RowCount;\n" + "int " + $id2.Text + " = " + $mx.Text + ".ColumnCount");
	}
	;

matrix3d
	:	first=ID3D br1='(' ':' ',' ':' secondcomma=',' expression last=')'
		{
			tokens.Replace($br1, $secondcomma, "[");
			tokens.Replace($last, "]");
		}
	;

zeros3d :	func='zeros(' value first=',' value second=',' value ')'
		{
			tokens.Replace($func, "new Matrix3d((int)");
			tokens.Replace($first, ",(int) ");
			tokens.Replace($second, ",(int) ");
		}	
	;

ones3d 	:	func='ones(' value first=',' value second=',' value ')'
		{
			tokens.Replace($func, "Matrix3d.Ones3d(");
			tokens.Replace($first, ",(int) ");
			tokens.Replace($second, ",(int) ");
		}	
	;

addColumn 
	:	
		first='[' mx=ID ',' (interval|vec=ID) last=']'
		{
			tokens.Replace($first, "MatrixSupport.AddColumn((Matrix)");
			if($vec != null)
			{
				tokens.InsertBefore($vec, "(Vector)");
				tokens.Replace($last, ")");
			}
			else
			{
				tokens.Replace($last, "))");
			}
		}
	;
	
interval
	:	from=value dd=':' to=value
	{
		tokens.InsertBefore(((IToken)from.Start).TokenIndex, "MatrixSupport.CreateInterval((int)");
		tokens.Replace($dd, ", ");
		tokens.InsertBefore(((IToken)to.Start).TokenIndex, "(int)");
		//tokens.InsertAfter(((IToken)to.Start).TokenIndex, ")");
	}
	;
	
refValues
	: refValues2 | refValues3
	;

refValues3
	: '[' id1=ID ',' id2=ID ',' id3=ID ']'
	;

refValues2
	: '[' id1=ID ',' id2=ID ']'
	{
		refParameters.Add($id1.Text);
		refParameters.Add($id2.Text);	
	}
	;
	
constant	: (valpi='pi'| valeps='eps')
	{
		if($valpi != null)
		{
			tokens.Replace($valpi, "Math.PI");
		}
		if($valeps != null)
		{
			tokens.Replace($valeps, "Math.Pow(2, -50)");
		}
	}	
	;
//iterator=ID eq='=' min=INT dd=':' max=INT

TYPE	: 'string' | 'bool' | 'int' | 'char' | 'double' | 'Matrix';

BEGIN 	: 'begin' ;

IF 	:	'if' ;

WHILE 	:	'while' ;

ELSE 	:	 'else' ;

FOR 	:	 'for' ;

END 	:	'end' ;

SEMI 	:	';' ;

ID  :   ('a'..'z'|'A'..'Z'|'.')+ ;

ID3D 	: '_' ('a'..'z'|'A'..'Z')+;

INT :   ('0'..'9')+ ;

CONDITION 
	:	 ('>' | '>=' | '<=' | '<' | '==' | '~=')
	;

NAME 
    : ('a'..'z'|'A'..'Z')('a'..'z'|'A'..'Z'|'0'..'9')+
    ;

WS  :   (' '|'\t'|'\r'|'\n')+ {$channel=HIDDEN;}
    ;

ANDOR	: '&' | '|'
	;

DOUBLE	: INT '.' INT+
	;





 
