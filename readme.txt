A final project of Cyrill Skrygan at Mathematics and Mechanics Department,
	St. Petersburg State University, Russia.

	MATLAB Runtime Web Server is a toolchain for running of MATLAB-compatible language algorithms online.
	This implementation is using ANTLR Parser Generator for translation into C#, SOAP as a message protocol
	between server and client, and it used for Clustering Algorithms Meta Applier (CAMA).

See full description in 'docs' (currently, in russian only).

Third-party components:
* Math.NET Iridium
