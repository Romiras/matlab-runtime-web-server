﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;

namespace SupportMathFunctions
{
    public class kMeans
    {
        public static Matrix kMeansCluster(ref Matrix c, Matrix m, Matrix k, Matrix isRand)
        {
            Matrix y = null;
            Matrix tempSize = null;
            Matrix p = null;
            Matrix temp = null;
            Matrix d = null;
            Matrix g = null;
            Matrix f = null;
            Matrix tempf = null;


            if (k < 3)
            {
                isRand = (Matrix)(false);
            }

            int maxRow = m.RowCount;
            int maxCol = m.ColumnCount;


            if (maxRow <= k)
            {
                y = (Matrix)(MatrixSupport.AddColumn((Matrix)m, MatrixSupport.CreateInterval((int)0, (int)(maxRow-1))));
            }
            else
            {
                if (isRand == 1)
                {
                    tempSize = (Matrix)(MatrixSupport.Size(m, 1));
                    p = (Matrix)(RandomSupport.RandomPermutation((int)tempSize));
                    for (int i = 0; i < k; i++)
                    {
                        temp = (Matrix)(p[i, 0]);
                        Matrix tempVectorc = MatrixSupport.GetMatrixByRow((Vector)temp, m);
                        MatrixSupport.CreateAndSetRow(ref c, (Vector)tempVectorc, i);
                    }
                }
                else
                {
                    for (int i = 0; i < k; i++)
                    {
                        Matrix tempVectorc = MatrixSupport.GetMatrixByRow((Vector)i, m);
                        MatrixSupport.CreateAndSetRow(ref c, (Vector)tempVectorc, i);
                    }
                }

                temp = (Matrix)(new Matrix((int)maxRow, 1));
                while (1 == 1)
                {
                    d = (Matrix)(MatrixSupport.GetDistanceMatrix(m, c));
                    g = MatrixSupport.RetriveMinimumIndexesVector(d, 2);
                    if (g == temp)
                    {
                        break;
                    }
                    else
                    {
                        temp = (Matrix)(g);
                    }
                    for (int i = 0; i < k; i++)
                    {
                        f = (Matrix)(MatrixSupport.Find(g, i));
                        if (f.RowCount != 0)
                        {
                            tempf = (Matrix)(MatrixSupport.GetMatrixByRow((Vector)f, m));
                            Matrix tempVectorc = MatrixSupport.MeanMatrix(tempf, 1);
                            MatrixSupport.CreateAndSetRow(ref c, (Vector)tempVectorc, i);
                        }
                    }
                }
            }
            y = (Matrix)(MatrixSupport.AddColumn((Matrix)m, (Vector)g));
            return y;
        }
    }
}
