﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;

namespace SupportMathFunctions
{
    public class Covariance
    {
        /// <summary>
        /// Get average
        /// </summary>
        public static double GetAverage(Vector data)
        {
            int len = data.Length;
            if (len == 0)
                throw new Exception("No data");

            double sum = 0;
            for (int i = 0; i < data.Length; i++)
                sum += data[i];
            return sum / len;
        }

        /// <summary>
        /// Get variance
        /// </summary>
        public static double GetVariance(Vector data)
        {
            int len = data.Length;
            // Get average
            double avg = GetAverage(data);

            double sum = 0;
            for (int i = 0; i < data.Length; i++)
                sum += Math.Pow((data[i] - avg), 2);
            return sum / (len-1);
        }

        /// <summary>
        /// Get standard deviation
        /// </summary>
        public static double GetStdev(Vector data)
        {
            return Math.Sqrt(GetVariance(data));
        }

        /// <summary>
        /// Get correlation
        /// </summary>
        public static double GetCovariance(Vector x, Vector y)
        {
            if (x.Length != y.Length)
                throw new Exception("Length of sources is different");
            double avgX = GetAverage(x);
            double stdevX = GetStdev(x);
            double avgY = GetAverage(y);
            double stdevY = GetStdev(y);
            int len = x.Length;
            double covXY = 0;
            for (int i = 0; i < len; i++)
                covXY += (x[i] - avgX) * (y[i] - avgY);
            covXY /= len-1;
            return covXY;
        }

        public static Matrix GetCovarianceMatrix(Matrix m)
        {
            Matrix covarinceMatrix = new Matrix(m.ColumnCount, m.ColumnCount);
            for (int i = 0; i < m.ColumnCount; i++)
            {
                for (int j = 0; j < m.ColumnCount; j++)
                {
                    covarinceMatrix[i, j] = GetCovariance(m.GetColumnVector(i), m.GetColumnVector(j));
                }
            }
            return covarinceMatrix;
        }
    }
}
