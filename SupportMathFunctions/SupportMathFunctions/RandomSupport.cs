﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;

namespace SupportMathFunctions
{
    public class RandomSupport
    {
        // Methods
        public static Matrix RandomPermutation(int n)
        {
            Vector v = new Vector(n);
            for (int i = 0; i < v.Length; i++)
            {
                v[i] = i + 1;
            }

            Combinatorics.RandomShuffle<double>(v);
            Matrix result = new Matrix(n, 1);
            result.SetColumnVector(v, 0);
            return result;
        }
    }


}
