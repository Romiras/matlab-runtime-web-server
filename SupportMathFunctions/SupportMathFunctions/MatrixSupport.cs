﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;

namespace SupportMathFunctions
{
    public class MatrixSupport
    {
        //public static void Main()
        //{
        //    Vector x = new Vector(new double[] { 5, 6, 7, 1 });
        //    Vector z = new Vector(new double[] { 8, 7, 8, 19 });
        //    Matrix m = new Matrix(4, 2);
        //    m[0, 0] = 1;
        //    m[0, 1] = 1;
        //    m[1, 0] = 2;
        //    m[1, 1] = 1;
        //    m[2, 0] = 4;
        //    m[2, 1] = 3;
        //    m[3, 0] = 5;
        //    m[3, 1] = 4;
        //    Matrix res = Covariance.GetCovarianceMatrix(m);
        //    Console.ReadKey();
        //}



        public static Matrix AddColumn(Matrix m, Vector v)
        {
            Matrix result = new Matrix(m.RowCount, m.ColumnCount + 1);
            result.SetMatrix(0, m.RowCount-1, 0, m.ColumnCount-1, m);
            result.SetColumnVector(v, m.ColumnCount);
            return result;
        }

        public static Matrix MeanMatrix(Matrix m, int dim)
        {
            Matrix result;
            if (dim == 2)
            {
                result = new Matrix(m.RowCount, 1);
                for (int i = 0; i < m.RowCount; i++)
                {
                    result[i, 0] = MeanVector(m.GetRowVector(i));
                }
            }
            else if (dim == 1)
            {
                result = new Matrix(1, m.ColumnCount);
                for (int i = 0; i < m.ColumnCount; i++)
                {
                    result[0, i] = MeanVector(m.GetColumnVector(i));
                }
            }
            else
            {
                throw new ArgumentException("Currently, only 1 and 2 are the supported values of dimension");
            }
            return result;
        }
        //matlab interface, i.e. ones(3, 4) will produce matrix(4, 5) with actual inner matrix (3, 4).
        public static
        Matrix
        Ones(
            int m, int n
            )
        {
            Matrix result = new Matrix(m, n, 1.0);
            //result.SetColumnVector(new Vector(m + 1), 0);
            //result.SetRowVector(new Vector(n + 1), 0);
            return result;
        }

        private static double MeanVector(Vector vector)
        {
            double sum = 0;
            for (int i = 0; i < vector.Length; i++)
            {
                sum += vector[i];
            }
            return (sum / (vector.Length));
        }

        public static int Size1(Matrix m, int d)
        {
            return m.ColumnCount;
        }

        public static Vector Sum(Matrix m)
        {
            Vector result = new Vector(m.ColumnCount);
            for (int i = 0; i < m.ColumnCount; i++)
            {
                result[i] = Sum(m.GetColumnVector(i));
            }
            return result;
        }

        public static double Sum(Vector v)
        {
            double result = 0;
            for (int i = 0; i < v.Length; i++)
            {
                result += v[i];
            }
            return result;
        }

        public static int Size(Matrix m, int dim)
        {
            if (dim == 1)
            {
                return m.RowCount;
            }
            else if (dim == 2)
            {
                return m.ColumnCount;
            }
            else
            {
                throw new ArgumentException("dimension value must be either 1 or 2 value");
            }
        }

        //obsolete
        public static Matrix MultipleMatrixDistance(Matrix m, Matrix n)
        {
            if (m.ColumnCount != n.RowCount)
            {
                throw new ArgumentException("number of rows and columns should be the same.");
            }
            else
            {
                return m.Multiply(n);
            }
        }

        public static Matrix GetDistanceMatrix(Matrix m, Matrix n)
        {
            if (m.ColumnCount != n.ColumnCount)
            {
                throw new ArgumentException("number of columns should be the same");
            }
            else
            {
                Matrix result = new Matrix(m.RowCount, n.RowCount);
                for (int i = 0; i < m.RowCount; i++)
                {
                    for (int j = 0; j < n.RowCount; j++)
                    {
                        result[i, j] = GetEuclidDistance(m.GetRowVector(i), n.GetRowVector(j));
                    }
                }
                return result;
            }
        }

        public static double GetEuclidDistance(Vector v1, Vector v2)
        {
            if (v1.Length != v2.Length)
            {
                throw new ArgumentException("dimensions should be the same");
            }
            else
            {
                double sum = 0;
                for (int i = 0; i < v1.Length; i++)
                {
                    sum += Math.Pow(v1[i] - v2[i], 2);
                }
                return Math.Sqrt(sum);
            }
        }


        public static Matrix Find(Matrix m, int n)
        {
            Matrix resultTemp = new Matrix(m.RowCount, 1);
            int t = 0;
            for (int i = 0; i < m.RowCount; i++)
            {
                if (m[i, 0] == n)
                {
                    resultTemp[t, 0] = i;
                    t++;
                }
            }
            Matrix resultFinal = new Matrix(t, 1);
            for (int i = 0; i < t; i++)
            {
                resultFinal[i, 0] = resultTemp[i, 0];
            }
            return resultFinal;
        }

        public static Matrix GetMatrixByRow(Vector v, Matrix m)
        {
            Matrix result = new Matrix(v.Length, m.ColumnCount);
            for (int i = 0; i < v.Length; i++)
            {
                result.SetRowVector(m.GetRowVector((int)v[i]), i);
            }
            return result;
        }

        public static Matrix GetMatrixByColumn(Vector v, Matrix m)
        {
            Matrix result = new Matrix(m.RowCount, v.Length);
            for (int i = 0; i < v.Length; i++)
            {
                result.SetColumnVector(m.GetColumnVector((int)v[i]), i);
            }
            return result;
        }

        public static Matrix RetriveMinimumIndexesVector(Matrix m, int dimension)
        {
            Vector resultAsVector;
            if (dimension == 2)
            {
                resultAsVector = new Vector(m.RowCount);
                for (int i = 0; i < m.RowCount; i++)
                {
                    resultAsVector[i] = MinimumCordIndex(m.GetRowVector(i));
                }
            }
            else if (dimension == 1)
            {
                resultAsVector = new Vector(m.ColumnCount-1);
                for (int i = 0; i < m.ColumnCount; i++)
                {
                    resultAsVector[i] = MinimumCordIndex(m.GetColumnVector(i));
                }
            }
            else
            {
                throw new ArgumentException("Currently, only 1 and 2 are the supported values of dimension");
            }
            Matrix resultAsMatrix = new Matrix(resultAsVector.Length, 1);
            resultAsMatrix.SetColumnVector(resultAsVector, 0);
            return resultAsMatrix;
        }

        public static void CreateAndSetRow(ref Matrix m, Vector v, int index)
        {
            if (m == null)
            {
                m = new Matrix(index+1, v.Length);
                m.SetRowVector(v, index);
                return;
            }
            if (m.RowCount > index)
            {
                m.SetRowVector(v, index);
                return;
            }
            if (m.RowCount <= index)
            {
                Matrix mNew = new Matrix(index+1, v.Length);
                mNew.SetMatrix(0, index - 1, 0, m.ColumnCount - 1, m);
                mNew.SetRowVector(v, index);
                m = new Matrix(mNew.RowCount, mNew.ColumnCount);
                m.SetMatrix(0, mNew.RowCount - 1, 0, mNew.ColumnCount - 1, mNew);
                return;
            }
        }

        public static void CreateAndSetColumn(ref Matrix m, Vector v, int index)
        {
            if (m == null)
            {
                m = new Matrix(v.Length, index);
                m.SetColumnVector(v, index - 1);
                return;
            }
            if (m.ColumnCount >= index)
            {
                m.SetColumnVector(v, index);
                return;
            }
            if (m.ColumnCount < index)
            {
                Matrix mNew = new Matrix(v.Length, index);
                mNew.SetMatrix(0, m.RowCount - 1, 0, index - 2, m);
                mNew.SetColumnVector(v, index - 1);
                m = new Matrix(mNew.RowCount, mNew.ColumnCount);
                m.SetMatrix(0, mNew.RowCount - 1, 0, mNew.ColumnCount - 1, mNew);
                return;
            }
        }

        public static Vector CreateInterval(int from, int to)
        {
            int distance = to - from;
            if (distance < 0)
            {
                throw new ArgumentException("from value must be smaller then to value");
            }

            Vector result = new Vector(distance+1);
            int t = from;
            for (int i = 0; i <= distance; i++)
            {
                result[i] = t;
                t++;
            }
            return result;

        }

        public static void ReplaceRows(Matrix m, int i1, int i2)
        {
            Vector row1 = m.GetRowVector(i1);
            Vector row2 = m.GetRowVector(i2);
            m.SetRowVector(row2, i1);
            m.SetRowVector(row1, i2);
        }

        public static Vector RetrieveMinimumVector(Matrix m, int dimension)
        {
            Vector result;
            if (dimension == 1)
            {
                result = new Vector(m.RowCount);
                for (int i = 0; i < m.RowCount; i++)
                {
                    result[i] = MinimumCord(m.GetRowVector(i));
                }
            }
            else if (dimension == 2)
            {
                result = new Vector(m.ColumnCount);
                for (int i = 0; i < m.ColumnCount; i++)
                {
                    result[i] = MinimumCord(m.GetColumnVector(i));
                }
            }
            else
            {
                throw new ArgumentException("Currently, only 1 and 2 are the supported dimension values");
            }
            return result;
        }

        public static int MinimumCordIndex(Vector vector)
        {
            int result = 0;
            for (int i = 0; i < vector.Length; i++)
            {
                if (vector[i] < vector[result])
                {
                    result = i;
                }
            }
            return result;
        }

        private static double MinimumCord(Vector vector)
        {
            double result = vector[0];
            for (int i = 0; i < vector.Length; i++)
            {
                if (vector[i] < result)
                {
                    result = vector[i];
                }
            }
            return result;
        }

        public static Vector RetrieveMinimumColumnVector(Matrix m)
        {
            Vector resultVector = new Vector(m.ColumnCount);
            for (int i = 0; i < m.ColumnCount; i++)
            {
                double min = m[0, i];
                for (int j = 0; j < m.RowCount; j++)
                {
                    if (m[j, i] < min)
                        min = m[j, i];
                }
                resultVector[i] = min;
            }

            return resultVector;
        }

        public static Matrix Max(Matrix m)
        {
            double result = m[0, 0];
            for (int i = 0; i < m.RowCount; i++)
            {
                for (int j = 0; j < m.ColumnCount; j++)
                {
                    if (m[i, j] > result)
                    {
                        result = m[i, j];
                    }
                }
            }
            return (Matrix)result;
        }

        public static Matrix Min(Matrix m)
        {
            double result = m[0, 0];
            for (int i = 0; i < m.RowCount; i++)
            {
                for (int j = 0; j < m.ColumnCount; j++)
                {
                    if (m[i, j] < result)
                    {
                        result = m[i, j];
                    }
                }
            }
            return (Matrix)result;
        }

        public static Vector FilterElements(Vector v, int criteria)
        {
            Vector result = new Vector(v.Length);
            for (int i = 0; i < v.Length; i++)
            {
                if (v[i] == criteria)
                {
                    result[i] = 0;
                }
                else
                    result[i] = 1;
            }
            return result;
        }

        public static void ValidSumSqures(ref Matrix T, ref Matrix W, ref Matrix B, ref Matrix Sintra, ref Matrix Sinter, Matrix data, Matrix labels, int k)
        {
            if (Size(labels, 1) == 1)
            {
               labels.Transpose();
            }

            int ncase = data.RowCount;
            int m = data.ColumnCount;

            Matrix Dm = MeanMatrix(data, 1);
            Matrix temp = new Matrix(ncase, m);
            for (int i = 0; i < ncase; i++)
            {
                temp.SetRowVector(Dm.GetRowVector(0), i);
            }
            Dm = temp;
            Dm = data - Dm;
            T = Matrix.Transpose(Dm) * Dm;

            W = new Matrix(T.RowCount, T.ColumnCount);
            Dm = new Matrix(k, m);
            Sintra = new Matrix(1, k);

            Vector Clust_idx;
            for (int i = 0; i < k; i++)
            {
                if (k > 1)
                {
                    Clust_idx = (Vector)Find(labels, i);
                }
                else
                {
                    Clust_idx = CreateInterval(1, ncase); 
                }
                int nk = Clust_idx.Length;
                if (nk > 1)
                {
                    Matrix dataC = data.GetMatrix(0, nk - 1, 0, m-1);
                    Matrix mn = MeanMatrix(dataC, 1);
                    Dm.SetRowVector(mn.GetRowVector(0), i);

                    Matrix temp1 = new Matrix(nk, mn.ColumnCount);
                    for (int h = 0; h < nk; h++)
			        {
                        temp1.SetRowVector(mn.GetRowVector(0), h);
			        }
                    dataC = dataC - temp1;
                    W = W + Matrix.Transpose(dataC)*dataC;
                    dataC.ArrayPower(2);
                    dataC = Sum2(dataC);
                    dataC.ArrayPower(0.5);
                    Sintra[0, i] = MeanVector((Vector)dataC);
                }
            }

            B = T - W;

            Sinter = new Matrix(k, k);
            if (k > 1)
            {
                for (int i = 0; i < k; i++)
                {
                    for (int j = i + 1; j < k; j++)
                    {
                        Matrix mn2 = Abs((Matrix)(Dm.GetRowVector(i) - Dm.GetRowVector(j)));
                        mn2.ArrayPower(2);
                        Sinter[i,j] = Math.Pow(Sum2(mn2)[0, 0], 2);
                        Sinter[j,i] = Sinter[i,j];
                    }
                }
            }

        }

        public static Matrix Abs(Matrix m)
        {
            Matrix result = new Matrix(m.RowCount, m.ColumnCount);
            for (int i = 0; i < m.RowCount; i++)
            {
                for (int j = 0; j < m.ColumnCount; j++)
                {
                    result[i, j] = Math.Abs(m[i, j]);
                }
            }
            return result;
        }

        private static Matrix Sum2(Matrix m)
        {
            Matrix result = new Matrix(m.RowCount, 1);
            for (int i = 0; i < m.RowCount; i++)
            {
                result[i, 0] = Sum(m.GetRowVector(i));
            }
            return result;
        }
    }
}
