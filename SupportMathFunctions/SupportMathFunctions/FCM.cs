﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;

namespace SupportMathFunctions
{
    public class FCM
    {
        public static Matrix InitFCM(int clusterNumber, int dataNumber)
        {
            double[] sum = new double[dataNumber];
            Matrix result = new Matrix(clusterNumber, dataNumber);
            Random r = new Random();
            for (int i = 0; i < result.RowCount; i++)
            {
                for (int j = 0; j < result.ColumnCount; j++)
                {
                    result[i, j] = r.NextDouble();
                }
            }
            for (int i = 0; i < result.ColumnCount; i++)
            {
                for (int j = 0; j < result.RowCount; j++)
			    {
                    sum[i] += result[j, i];
			    }
                result.SetColumnVector(result.GetColumnVector(i) / sum[i], i);
            }
            //result.SetRowVector(new Vector(result.ColumnCount), 0);
            //result.SetColumnVector(new Vector(result.RowCount), 0);
            return result;
        }

        public static Matrix FCMMain(Matrix data, Matrix clusterN)
        {
            Matrix dataN = null;
            Matrix inN = null;
            Matrix expo = null;
            Matrix maxIter = null;
            Matrix minImpro = null;
            Matrix display = null;
            Matrix objFcn = null;
            Matrix u = null;
            Matrix temp = null;
            Matrix center = null;


            dataN = (Matrix)(MatrixSupport.Size(data, 1));
            inN = (Matrix)(MatrixSupport.Size(data, 2));

            expo = (Matrix)(2);
            maxIter = (Matrix)(100);
            minImpro = (Matrix)(0.00001);
            display = (Matrix)(1);

            objFcn = (Matrix)(new Matrix((int)maxIter, 1));

            u = (Matrix)(SupportMathFunctions.FCM.InitFCM((int)clusterN, (int)dataN));

            for (int i = 0; i < maxIter; i++)
            {
                temp = (Matrix)(objFcn[i, 0]);
                SupportMathFunctions.FCM.StepFCM(ref u, ref center, ref temp, data, u, (int)clusterN, (double)expo);
                objFcn[i, 0] = (double)temp;

                if (i > 1)
                {
                    if ((Matrix)Math.Abs(objFcn[i, 0] - objFcn[i - 1, 0]) < minImpro)
                    {
                        break;
                    }
                }
            }

            return center;
        }

        public static void StepFCM(ref Matrix uNew, ref Matrix center, ref Matrix objFcn, Matrix m, Matrix u, int k, double exp)
        {
            Matrix mf = u.Clone();
            mf.ArrayPower(exp);
            Matrix up = mf * m;
            Matrix mf_t = mf.Clone();
            mf_t.Transpose();
            Matrix down = MatrixSupport.Ones(MatrixSupport.Size(m, 2), 1) * (Matrix)MatrixSupport.Sum(mf_t);
            down.Transpose();
            center = Matrix.ArrayDivide(up, down);
            Matrix distance = MatrixSupport.GetDistanceMatrix(center, m);
            Matrix distSqr = distance.Clone();
            distSqr.ArrayPower(2.0);
            objFcn = (Matrix)MatrixSupport.Sum(MatrixSupport.Sum(Matrix.ArrayMultiply(distSqr, mf)));
            Matrix temp = Matrix.ArrayPower(distance, (-2.0/(exp-1)));
            uNew = Matrix.ArrayDivide(temp, MatrixSupport.Ones(k, 1)*(Matrix)MatrixSupport.Sum(temp));
        }
    }
}
