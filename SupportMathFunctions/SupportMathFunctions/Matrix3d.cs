﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;

namespace SupportMathFunctions
{
    public class Matrix3d
    {
        public Matrix3d(int x, int y, int z)
        {
            this.data = new Matrix[z];
            for (int i = 0; i < z; i++)
            {
                this.data[i] = new Matrix(x, y);
            }   
        }

        public static Matrix3d Ones3d(int x, int y, int z)
        {
            Matrix3d result = new Matrix3d(x, y, z);
            for (int i = 0; i < z; i++)
            {
                result.data[z] = new Matrix(x, y, 1.0);
            }
            return result;
        }

        public Matrix[] data;

        public Matrix this[int i]
        {
            get
            {
                return data[i];
            }

            set
            {
                data[i] = value;
            }
        }
    }
}
