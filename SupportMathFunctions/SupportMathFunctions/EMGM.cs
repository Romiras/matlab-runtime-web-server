﻿/**
 * Copyright (C) 2009 Cyrill Skrygan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MathNet.Numerics.LinearAlgebra;

namespace SupportMathFunctions
{
    public class EMGM
    {
        public class ClusterData
        {
            public Matrix m;
            public int k;
        }

        public static void InitEMGM(ref Matrix W, ref Matrix M, ref Matrix3d V, Matrix X, int k)
        {
            W = new Matrix(1, k);
            //M = FCM.FCMMain(X, (Matrix)4);
            Matrix kMeansResult = kMeans.kMeansCluster(ref M, X, (Matrix)k, (Matrix)false);
            M.Transpose();
            List<ClusterData> clusters = new List<ClusterData>();
            for (int i = 0; i < k; i++)
            {
                clusters.Add(new ClusterData());
                clusters[i].m = new Matrix(X.RowCount, X.ColumnCount);
            }

            for (int i = 0; i < X.RowCount; i++)
            {
                int num = (int)kMeansResult[i, kMeansResult.ColumnCount - 1];
                clusters[num].m.SetRowVector(X.GetRowVector(i), clusters[num].k);
                clusters[num].k++;
            }

            for (int i = 0; i < clusters.Count; i++)
            {
                V.data[i] = Covariance.GetCovarianceMatrix(clusters[i].m.GetMatrix(0, clusters[i].k-1, 0, X.ColumnCount-1));
                W[0, i] = (double)clusters[i].k / X.RowCount;
            }

        }

    }
}
